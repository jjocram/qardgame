//
// Created by Marco Ferrati on 2019-04-26.
//

#ifndef QARDGAME_CONTAINER_H
#define QARDGAME_CONTAINER_H

#include "ProjectException/ProjException.h"
#include "DeepPtr.h"

#include <iostream>
using namespace std;

template <class Type>
class Container{
private:
    Type* vector;
    unsigned int size;
    unsigned int numberOfElements;

    //Ritorna una copia pronfonda di vector
    Type* copyVector() const;

    //Moltiplica per value la size del container
    void expandSize(unsigned short int value = 2);

    //Incrementa di incrementValue la size del container
    void incrementSize(unsigned int incrementValue);

public:
    Container();
    Container(const Container& container);
    Container& operator=(const Container& container);
    ~Container();

    //Aggiunge in coda al container un elemento
    Container& append(const Type& el);

    //Aggiunge in coda al container un altro container
    Container& append(const Container& container);

    //Aggiunge in posizione index un nuovo elemento
    Container& insertAt(unsigned int index, const Type& el);

    //Aggiunge dalla posizione index un container
    Container& insertAt(unsigned int index, const Container& container);

    //Rimuove l'elemento in poszione x e sposta gli elementi successivi in dietro di 1
    Type removeAt(unsigned int index);

    //Cerca l'elemento elToRemove nel container e se lo trova lo elimina con removeAt
    Type remove(const Type &elToRemove);

    //Rimpiazza l'elemento in posizione index con elToReplace
    Type replaceAt(unsigned int index, const Type &elToReplace);

    bool operator==(const Container& containerToCompare) const;
    bool operator<(const Container& containerToCompare) const;
    bool operator<=(const Container& containerToCompare) const;
    bool operator>(const Container& containerToCompare) const;
    bool operator>=(const Container& containerToCompare) const;

    //Cerca elToFind nel container e ne restituisce l'indice se non lo trova
    unsigned int indexOf(const Type& elToFind) const;

    //Restituisce il numero di elementi (numberoOfElements) all'interno del container
    unsigned int count() const;

    //Restituisce true se il container è vuoto, false altrimenti
    bool isEmpty() const;

    //Ripulisce completamente l'array
    void clear();

    //Overload dell'operatore [] per l'accesso agli elementi del container
    Type& operator[](unsigned int index) const;

    //Funzione che restituisce un container con gli elementi filtrati
    template <typename Functor>
    Container<Type> filter(Functor test) const;

    //Classe iteratore
    class Iterator{
        friend class Container;

    private:
        unsigned int index;
        const Container* ptrToContainer; //Per accedere al container che sta usando questo iteratore
        Iterator(unsigned int index_init, const Container* ptrToContainer_init);

    public:
        Iterator();
        Iterator(const Iterator& it);

        Iterator& operator++();
        Iterator operator++(int);
        Iterator& operator--();
        Iterator operator--(int);
        Iterator& operator+=(int);
        Iterator &operator-=(int);
        Iterator operator+(int) const;
        Iterator operator-(int) const;
        Iterator& operator+=(const Iterator&);
        Iterator& operator-=(const Iterator&);
        Iterator operator+(const Iterator&) const;
        Iterator operator-(const Iterator&) const;


        bool operator==(const Iterator& it) const;
        bool operator!=(const Iterator& it) const;
        bool operator<(const Iterator& it) const;
        bool operator<=(const Iterator& it) const;
        bool operator>(const Iterator& it) const;
        bool operator>=(const Iterator& it) const;

        Type& operator*();
        Type* operator->();
        Type& operator[](unsigned int i) const;

        unsigned int getIndex() const;
    };

    //Classe iteratore costante
    class Const_Iterator{
        friend class Container;

    private:
        unsigned int index;
        const Container* ptrToContainer; //Per accedere al container che sta usando questo iteratore
        Const_Iterator(unsigned int index_init, const Container* ptrToContainer_init);

    public:
        Const_Iterator();
        Const_Iterator(const Const_Iterator& cit);

        Const_Iterator& operator++();
        Const_Iterator operator++(int);
        Const_Iterator& operator--();
        Const_Iterator operator--(int);
        Const_Iterator& operator+=(int);
        Const_Iterator &operator-=(int);
        Const_Iterator operator+(int) const;
        Const_Iterator operator-(int) const;
        Const_Iterator& operator+=(const Const_Iterator&);
        Const_Iterator& operator-=(const Const_Iterator&);
        Const_Iterator operator+(const Const_Iterator&) const;
        Const_Iterator operator-(const Const_Iterator&) const;


        bool operator==(const Const_Iterator& cit) const;
        bool operator!=(const Const_Iterator& cit) const;
        bool operator<(const Const_Iterator& it) const;
        bool operator<=(const Const_Iterator& it) const;

        bool operator>(const Const_Iterator& it) const;
        bool operator>=(const Const_Iterator& it) const;

        const Type& operator*();
        const Type* operator->();
        const Type & operator[](unsigned int i) const;

        unsigned int getIndex() const;
    };

    Iterator begin() const;
    Iterator end() const;
    Const_Iterator const_begin() const;
    Const_Iterator const_end() const;
};

/*************
 * Container *
 *************/

template<class Type>
Type *Container<Type>::copyVector() const {
    Type* vectorToReturn = size == 0 ? nullptr : new Type[size];

    for (unsigned int i = 0; i < numberOfElements; ++i)
        vectorToReturn[i] = vector[i];

    return vectorToReturn;
}

template<class Type>
void Container<Type>::expandSize(unsigned short value) {
    size = size == 0 ? 1 : size*value;
    Type* newVector = new Type[size];

    for (unsigned int i = 0; i < numberOfElements; ++i)
        newVector[i] = vector[i];

    delete[] vector;
    vector = newVector;
}

template<class Type>
void Container<Type>::incrementSize(unsigned int incrementValue) {
    size = size == 0 ? incrementValue : size+incrementValue;
    Type* newVector = new Type[size];

    for (unsigned int i = 0; i < numberOfElements; ++i)
        newVector[i] = vector[i];

    delete[] vector;
    vector = newVector;
}

template<class Type>
Container<Type>::Container(): vector(nullptr), size(0), numberOfElements(0) {}

template<class Type>
Container<Type>::Container(const Container &container): vector(container.copyVector()), size(container.size), numberOfElements(container.numberOfElements) {}

template<class Type>
Container<Type> &Container<Type>::operator=(const Container &container) {
    if (this != &container) {
        delete[] vector;

        size = container.size;
        numberOfElements = container.numberOfElements;
        vector = container.copyVector();
    }

    return *this;
}

template<class Type>
Container<Type>::~Container() {
    if (vector) delete[] vector;
}

template<class Type>
Container<Type> &Container<Type>::append(const Type &el) {
    if (numberOfElements == size)
        expandSize();

    vector[numberOfElements] = el;
    numberOfElements++;

    return *this;
}

template<class Type>
Container<Type> &Container<Type>::append(const Container &container) {
    if (size < (numberOfElements+container.numberOfElements))
        incrementSize(container.numberOfElements);

    for (unsigned int i = 0; i < container.numberOfElements; ++i)
        vector[numberOfElements+i] = container.vector[i];

    numberOfElements = numberOfElements + container.numberOfElements;

    return *this;
}

template<class Type>
Container<Type> &Container<Type>::insertAt(unsigned int index, const Type &el) {
    if (index < 0 || index > numberOfElements-1) throw(ProjException("Index out of range"));
    if (numberOfElements == size) expandSize();

    for (unsigned int i = numberOfElements-1; i >= index ; --i)
        vector[i+1] = vector[i];
    vector[index] = el;

    numberOfElements++;

    return *this;
}

template<class Type>
Container<Type> &Container<Type>::insertAt(unsigned int index, const Container &container) {
    if (index < 0 || index > numberOfElements-1) throw(ProjException("Index out of range"));
    if ((numberOfElements + container.numberOfElements) >=size) incrementSize(container.numberOfElements);

    unsigned int i = index+container.numberOfElements;
    unsigned int oldI = index;

    while (i < numberOfElements+container.numberOfElements){
        vector[i] = vector[oldI];
        ++i;
        ++oldI;
    }

    for (unsigned int j = 0; j < container.numberOfElements; ++j) {
        vector[index] = container[j];
        ++index;
    }

    numberOfElements = numberOfElements + container.numberOfElements;

    return *this;
}

template<class Type>
Type Container<Type>::removeAt(unsigned int index) {
    if (index < 0 || index > numberOfElements-1) throw(ProjException("Index out of range"));
    Type elRemoved = vector[index];
    Type* firstPartVector = new Type[index];
    for (unsigned int i = 0; i<index; i++)
        firstPartVector[i] = vector[i];

    Type* secondPartVector = new Type[numberOfElements-index];
    for (unsigned int i = index+1; i < numberOfElements; ++i)
        secondPartVector[i] = vector[i];


    //delete [] vector;
    for (unsigned int i = 0; i < index; ++i)
        vector[i] = firstPartVector[i];
    for (unsigned int i = index; i < numberOfElements-1; ++i)
        vector[i] = secondPartVector[i-index];

    numberOfElements--;
    return elRemoved;
}

template<class Type>
Type Container<Type>::remove(const Type &elToRemove) {
    return removeAt(indexOf(elToRemove));
}

template<class Type>
Type Container<Type>::replaceAt(unsigned int index, const Type &elToReplace) {
    if (index < 0 || index > numberOfElements-1) throw(ProjException("Index out of range"));

    Type elReplaced = vector[index];
    vector[index] = elToReplace;

    return Type();
}

template<class Type>
bool Container<Type>::operator==(const Container &containerToCompare) const {
    if (numberOfElements != containerToCompare.numberOfElements)
        return false;
    for (unsigned int i = 0; i < numberOfElements; ++i) {
        if (vector[i] != containerToCompare.vector[i])
            return false;
    }

    return true;
}

template<class Type>
unsigned int Container<Type>::indexOf(const Type &elToFind) const {
    for (unsigned int i = 0; i < numberOfElements; ++i) {
        if (vector[i] == elToFind)
            return i;
    }
    throw(ProjException("Element not found"));
}

template<class Type>
unsigned int Container<Type>::count() const {
    return numberOfElements;
}

template<class Type>
bool Container<Type>::isEmpty() const {
    return numberOfElements == 0;
}

template<class Type>
void Container<Type>::clear() {
    numberOfElements = 0;
    size = 0;
    delete[] vector;
    vector  = nullptr;
}

template<class Type>
Type &Container<Type>::operator[](unsigned int index) const{
    return vector[index];
}

template<class Type>
template<typename Functor>
Container<Type> Container<Type>::filter(Functor test) const {
    Container<Type> containerToReturn;

    for (auto it = begin(); it != end(); ++it)
        if(test(*it)) containerToReturn.append(*it);

    return containerToReturn;
}

template<class Type>
typename Container<Type>::Iterator Container<Type>::begin() const {
    return Container::Iterator(0, this);
}

template<class Type>
typename Container<Type>::Iterator Container<Type>::end() const {
    return Container::Iterator(numberOfElements, this);
}

template<class Type>
typename Container<Type>::Const_Iterator Container<Type>::const_begin() const {
    return Container::Const_Iterator(0, this);
}

template<class Type>
typename Container<Type>::Const_Iterator Container<Type>::const_end() const {
    return Container::Const_Iterator(numberOfElements, this);
}

template<class Type>
bool Container<Type>::operator<(const Container &containerToCompare) const {
    unsigned int min = numberOfElements < containerToCompare.numberOfElements ? numberOfElements
                                                                              : containerToCompare.numberOfElements;
    for (unsigned i = 0; i < min; ++i) {
        if (vector[i] != containerToCompare.vector[i])
            return vector[i] < containerToCompare.vector[i];
    }

    return numberOfElements < containerToCompare.numberOfElements;
}

template<class Type>
bool Container<Type>::operator<=(const Container &containerToCompare) const {
    return *this < containerToCompare || *this == containerToCompare;
}

template<class Type>
bool Container<Type>::operator>(const Container &containerToCompare) const {
    unsigned int min = numberOfElements < containerToCompare.numberOfElements ? numberOfElements
                                                                              : containerToCompare.numberOfElements;
    for (unsigned i = 0; i < min; ++i) {
        if (vector[i] != containerToCompare.vector[i])
            return vector[i] > containerToCompare.vector[i];
    }

    return numberOfElements>containerToCompare.numberOfElements ? true : false;
}

template<class Type>
bool Container<Type>::operator>=(const Container &containerToCompare) const {
    return *this > containerToCompare || *this == containerToCompare;
}

/************
 * Iterator *
 ************/

template<class Type>
Container<Type>::Iterator::Iterator(unsigned int index_init, const Container *ptrToContainer_init): index(index_init), ptrToContainer(ptrToContainer_init) {}

template<class Type>
Container<Type>::Iterator::Iterator(): index(0), ptrToContainer(nullptr) {}

template<class Type>
Container<Type>::Iterator::Iterator(const Container::Iterator &it): index(it.index), ptrToContainer(it.ptrToContainer) {}

template<class Type>
typename Container<Type>::Iterator &Container<Type>::Iterator::operator++() {
    index++;
    return *this;
}

template<class Type>
typename Container<Type>::Iterator Container<Type>::Iterator::operator++(int) {
    Iterator copy(*this);
    index++;
    return copy;
}

template<class Type>
typename Container<Type>::Iterator &Container<Type>::Iterator::operator--() {
    index--;
    return *this;
}

template<class Type>
typename Container<Type>::Iterator Container<Type>::Iterator::operator--(int) {
    Iterator copy(*this);
    index--;
    return copy;
}

template<class Type>
bool Container<Type>::Iterator::operator==(const Container::Iterator &it) const {
    return ptrToContainer == it.ptrToContainer && index == it.index;
}

template<class Type>
bool Container<Type>::Iterator::operator!=(const Container::Iterator &it) const {
    return !(*this == it);
}

template<class Type>
Type &Container<Type>::Iterator::operator*() {
    return ptrToContainer->vector[index];
}

template<class Type>
Type *Container<Type>::Iterator::operator->() {
    return &ptrToContainer->vector[index];
}

template<class Type>
unsigned int Container<Type>::Iterator::getIndex() const {
    return index;
}

template<class Type>
typename Container<Type>::Iterator &Container<Type>::Iterator::operator+=(int i) {
    index += i;
    return *this;
}

template<class Type>
typename Container<Type>::Iterator &Container<Type>::Iterator::operator-=(int i) {
    index -= i;
    return *this;
}

template<class Type>
typename Container<Type>::Iterator Container<Type>::Iterator::operator+(int i) const {
    return Container::Iterator(index+i, ptrToContainer);
}

template<class Type>
typename Container<Type>::Iterator Container<Type>::Iterator::operator-(int i) const {
    return Container::Iterator(index-i, ptrToContainer);
}

template<class Type>
typename Container<Type>::Iterator &Container<Type>::Iterator::operator+=(const Container::Iterator &it) {
    index += it.index;
    return *this;
}

template<class Type>
typename Container<Type>::Iterator &Container<Type>::Iterator::operator-=(const Container::Iterator &it) {
    index -= it.index;
    return *this;
}

template<class Type>
typename Container<Type>::Iterator Container<Type>::Iterator::operator+(const Container::Iterator &it) const {
    return Container::Iterator(index+it.index, ptrToContainer);
}

template<class Type>
typename Container<Type>::Iterator Container<Type>::Iterator::operator-(const Container::Iterator &it) const {
    return Container::Iterator(index-it.index, ptrToContainer);
}

template<class Type>
Type &Container<Type>::Iterator::operator[](unsigned int i) const {
    return ptrToContainer->vector[i];
}

template<class Type>
bool Container<Type>::Iterator::operator<(const Container::Iterator &it) const {
    return index<it.index;
}

template<class Type>
bool Container<Type>::Iterator::operator<=(const Container::Iterator &it) const {
    return index<=it.index;
}

template<class Type>
bool Container<Type>::Iterator::operator>(const Container::Iterator &it) const {
    return index>it.index;
}

template<class Type>
bool Container<Type>::Iterator::operator>=(const Container::Iterator &it) const {
    return index>=it.index;
}

/******************
 * Const Iterator *
 ******************/

template<class Type>
Container<Type>::Const_Iterator::Const_Iterator(unsigned int index_init, const Container *ptrToContainer_init): index(index_init), ptrToContainer(ptrToContainer_init) {}

template<class Type>
Container<Type>::Const_Iterator::Const_Iterator(): index(0), ptrToContainer(nullptr) {}

template<class Type>
Container<Type>::Const_Iterator::Const_Iterator(const Container::Const_Iterator &cit): index(cit.index), ptrToContainer(cit.ptrToContainer) {}

template<class Type>
typename Container<Type>::Const_Iterator &Container<Type>::Const_Iterator::operator++() {
    index++;
    return *this;
}

template<class Type>
typename Container<Type>::Const_Iterator Container<Type>::Const_Iterator::operator++(int) {
    Const_Iterator copy(*this);
    index++;
    return copy;
}

template<class Type>
typename Container<Type>::Const_Iterator &Container<Type>::Const_Iterator::operator--() {
    index--;
    return *this;
}

template<class Type>
typename Container<Type>::Const_Iterator Container<Type>::Const_Iterator::operator--(int) {
    Const_Iterator copy(*this);
    index--;
    return copy;
}

template<class Type>
bool Container<Type>::Const_Iterator::operator==(const Container::Const_Iterator &cit) const {
    return ptrToContainer == cit.ptrToContainer && index == cit.index;
}

template<class Type>
bool Container<Type>::Const_Iterator::operator!=(const Container::Const_Iterator &cit) const {
    return !(*this == cit);
}

template<class Type>
const Type &Container<Type>::Const_Iterator::operator*() {
    return ptrToContainer->vector[index];
}

template<class Type>
const Type *Container<Type>::Const_Iterator::operator->() {
    return &ptrToContainer->vector[index];
}

template<class Type>
unsigned int Container<Type>::Const_Iterator::getIndex() const {
    return index;
}

template<class Type>
typename Container<Type>::Const_Iterator &Container<Type>::Const_Iterator::operator+=(int i) {
    index += i;
    return *this;
}

template<class Type>
typename Container<Type>::Const_Iterator &Container<Type>::Const_Iterator::operator-=(int i) {
    index -= i;
    return *this;
}

template<class Type>
typename Container<Type>::Const_Iterator Container<Type>::Const_Iterator::operator+(int i) const {
    return Container::Const_Iterator(index+i, ptrToContainer);
}

template<class Type>
typename Container<Type>::Const_Iterator Container<Type>::Const_Iterator::operator-(int i) const {
    return Container::Const_Iterator(index-i, ptrToContainer);
}

template<class Type>
typename Container<Type>::Const_Iterator &Container<Type>::Const_Iterator::operator+=(const Container::Const_Iterator &it) {
    index += it.index;
    return *this;
}

template<class Type>
typename Container<Type>::Const_Iterator &Container<Type>::Const_Iterator::operator-=(const Container::Const_Iterator &it) {
    index -= it.index;
    return *this;
}

template<class Type>
typename Container<Type>::Const_Iterator Container<Type>::Const_Iterator::operator+(const Container::Const_Iterator &it) const {
    return Container::Const_Iterator(index+it.index, ptrToContainer);
}

template<class Type>
typename Container<Type>::Const_Iterator Container<Type>::Const_Iterator::operator-(const Container::Const_Iterator &it) const {
    return Container::Const_Iterator(index-it.index, ptrToContainer);
}

template<class Type>
bool Container<Type>::Const_Iterator::operator<(const Container::Const_Iterator &it) const {
    return index<it.index;
}

template<class Type>
bool Container<Type>::Const_Iterator::operator<=(const Container::Const_Iterator &it) const {
    return index<=it.index;
}

template<class Type>
bool Container<Type>::Const_Iterator::operator>(const Container::Const_Iterator &it) const {
    return index>it.index;
}

template<class Type>
bool Container<Type>::Const_Iterator::operator>=(const Container::Const_Iterator &it) const {
    return index>=it.index;
}

template<class Type>
const Type & Container<Type>::Const_Iterator::operator[](unsigned int i) const {
    return ptrToContainer->vector[i];
}

#endif //QARDGAME_CONTAINER_H
