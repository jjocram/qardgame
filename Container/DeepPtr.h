//
// Created by Marco Ferrati on 2019-04-26.
//

#ifndef QARDGAME_DEEPPTR_H
#define QARDGAME_DEEPPTR_H

#include <iostream>
using namespace std;

template <class Type>
class DeepPtr{
private:
    Type* ptr;
public:
    explicit DeepPtr(Type* p = nullptr);
    DeepPtr(const DeepPtr& deepPtr);
    DeepPtr& operator=(const DeepPtr& deepPtr);
    Type* operator->() const;
    Type& operator*() const;
    ~DeepPtr();

    bool operator==(const DeepPtr& deepPtr);
    bool operator!=(const DeepPtr& deepPtr);
};

template<class Type>
DeepPtr<Type>::DeepPtr(Type *p): ptr(p) {}

template<class Type>
DeepPtr<Type>::DeepPtr(const DeepPtr &deepPtr){
    if (deepPtr.ptr == nullptr){
        ptr = nullptr;
    }
    else{
        ptr = deepPtr.ptr->clone();
    }
}

template<class Type>
DeepPtr<Type>& DeepPtr<Type>::operator=(const DeepPtr &deepPtr) {
    if (this != &deepPtr){
        if (ptr) delete ptr;
        if (deepPtr.ptr == nullptr)
            ptr = nullptr;
        else{
            ptr = deepPtr.ptr->clone();
        }
    }
    return *this;
}

template<class Type>
Type *DeepPtr<Type>::operator->() const {
    return ptr;
}

template<class Type>
Type &DeepPtr<Type>::operator*() const {
    return *ptr;
}

template<class Type>
DeepPtr<Type>::~DeepPtr() {
    if (ptr) delete ptr;
}

template<class Type>
bool DeepPtr<Type>::operator==(const DeepPtr &deepPtr) {
    return ptr == deepPtr.ptr;
}

template<class Type>
bool DeepPtr<Type>::operator!=(const DeepPtr &deepPtr) {
    return ptr != deepPtr.ptr;
}

#endif //QARDGAME_DEEPPTR_H
