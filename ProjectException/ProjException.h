//
// Created by Marco Ferrati on 2019-04-26.
//

#ifndef QARDGAME_PROJEXCEPTION_H
#define QARDGAME_PROJEXCEPTION_H

#include <exception>
#include <string>

class ProjException: public std::exception {
private:
    std::string whatHappened;
public:
    ProjException(std::string whatHappened_init);
    const char *what() const noexcept override;
};


#endif //QARDGAME_PROJEXCEPTION_H
