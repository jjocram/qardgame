//
// Created by Marco Ferrati on 2019-04-26.
//

#include "ProjException.h"

ProjException::ProjException(std::string whatHappened_init): whatHappened(whatHappened_init) {}

const char *ProjException::what() const noexcept {
    return whatHappened.c_str();
}
