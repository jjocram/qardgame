//
// Created by Marco Ferrati on 2019-05-01.
//

#include "MainWidget.h"

#include "CardsWidgets/CreateNewCardWidget.h"
#include "CardsWidgets/EditCardsWidget.h"
#include "DecksWidgets/DeckCreatorWidget.h"
#include "DecksWidgets/ModifyDecksWidget.h"
#include "PlayWidgets/StartPlayWidget.h"

#include <iostream>
using namespace std;

MainWidget::MainWidget(QWidget *parent): QWidget(parent) {
    //Pulsanti
    QSize buttonSize = QSize(150, 25);

    play_button = new QPushButton("Gioca", this);
    play_button->setFixedSize(QSize(300, 25));

    createCard_button = new QPushButton("Crea carta", this);
    createCard_button->setFixedSize(buttonSize);

    createDeck_button = new QPushButton("Crea deck", this);
    createDeck_button->setFixedSize(buttonSize);

    editCards_button = new QPushButton("Modifica carte", this);
    editCards_button->setFixedSize(buttonSize);

    editDecks_button = new QPushButton("Modifica deck", this);
    editDecks_button->setFixedSize(buttonSize);

    //Layout
    main_layout = new QVBoxLayout(this);

    cardButtons_layout = new QHBoxLayout();
    cardButtons_layout->addWidget(createCard_button);
    cardButtons_layout->addWidget(editCards_button);

    deckButtons_layout = new QHBoxLayout();
    deckButtons_layout->addWidget(createDeck_button);
    deckButtons_layout->addWidget(editDecks_button);

    main_layout->addWidget(play_button);
    main_layout->addLayout(cardButtons_layout);
    main_layout->addLayout(deckButtons_layout);

    //Connessioni
    connect(play_button, SIGNAL(clicked()), this, SLOT(openPlayWidget()));
    connect(createCard_button, SIGNAL(clicked()), this, SLOT(openCreateCardWidget()));
    connect(editCards_button, SIGNAL(clicked()), this, SLOT(openEditCardsWidget()));
    connect(createDeck_button, SIGNAL(clicked()), this, SLOT(openCreateDeckWidget()));
    connect(editDecks_button, SIGNAL(clicked()), this, SLOT(openEditDecksWidget()));
}

void MainWidget::openPlayWidget() {
    StartPlayWidget *widget = new StartPlayWidget();
    widget->setWindowModality(Qt::WindowModal);
    widget->show();
}

void MainWidget::openCreateCardWidget() {
    CreateNewCardWidget *widget = new CreateNewCardWidget();
    widget->setWindowModality(Qt::WindowModal);
    widget->show();
}

void MainWidget::openEditCardsWidget() {
    EditCardsWidget *widget = new EditCardsWidget();
    widget->setWindowModality(Qt::WindowModal);
    widget->show();
}

void MainWidget::openCreateDeckWidget() {
    DeckCreatorWidget *widget = new DeckCreatorWidget();
    widget->setWindowModality(Qt::WindowModal);
    widget->show();
}

void MainWidget::openEditDecksWidget() {
    ModifyDecksWidget *widget = new ModifyDecksWidget();
    widget->setWindowModality(Qt::WindowModal);
    widget->show();
}
