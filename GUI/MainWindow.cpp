//
// Created by Marco Ferrati on 2019-05-01.
//

#include "MainWindow.h"

#include "QUtils.h"

#include <QString>
#include <QFileDialog>
#include <QFile>

#include "../Model/Deck.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent) {
    //Setting di base
    setWindowTitle("QardGame");
    setFixedSize(310, 100);
    setWindowIconText(":/Images/fight.png");

    //Main widget
    main_widget = new MainWidget(this);
    main_widget->setFixedSize(300, 100);
}

MainWindow::~MainWindow() {

}
