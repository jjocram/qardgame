//
// Created by Marco Ferrati on 2019-05-16.
//

#ifndef QARDGAME_CARDEDITORWIDGET_H
#define QARDGAME_CARDEDITORWIDGET_H

#include <QWidget>
#include <QLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>

#include "EditCardsWidget.h"

#include "../../Model/Hierachy/Card.h"
#include "../../Model/Hierachy/Effects/Effect.h"


class CardEditorWidget: public QWidget {
    friend EditCardsWidget; //Per poter accedere ai vari campi coinvolti nella modifica
    Q_OBJECT

private:
    DeepPtr<Card> selectedCard;
    QVBoxLayout *main_layout;
    QVBoxLayout *changable_layout;

    //Elementi per il layout vuoto
    QLabel *emptyCardTitle_label;
    QLabel *emptyCardDescritpion_label;
    QVBoxLayout *emptyCard_layout;

    //Elementi base e sempre presenti
    QComboBox *cardType_input;
    QLabel *cardType_label;
    QHBoxLayout *cardType_layout;

    QLineEdit *cardTitle_input;
    QLabel *cardTitle_label;
    QHBoxLayout *cardTitle_layout;

    QLineEdit *cardDescription_input;
    QLabel *cardDescription_label;
    QHBoxLayout *cardDescription_layout;

    QLineEdit *cardManaCost_input;
    QLabel *cardManaCost_label;
    QHBoxLayout *cardManaCost_layout;

    //Elementi per le carte creatura
    QLineEdit *cardCreatureLife_input;
    QLabel *cardCreatureLife_label;
    QLineEdit *cardCreatureAtk_input;
    QLabel *cardCreatureAtk_label;
    QLineEdit *cardCreatureDef_input;
    QLabel *cardCreatureDef_label;
    QHBoxLayout *cardCreature_layout;

    //Elementi per le carte generatori di mana
    QLineEdit *cardManageneratorIncrementValue_input;
    QLabel *cardManaGeneratorIncrementValue_label;
    QHBoxLayout *cardManaGenerator_layout;

    //Elementi per le carte magia
    QComboBox *cardMagicEffect_input;
    QLabel *cardMagicEffect_label;
    QHBoxLayout *cardMagicEffectType_layout;

    //Elementi per gli effetti
    QLineEdit *effectDamage_input;
    QLabel *effectDamage_label;
    QHBoxLayout *effectDamage_layout;
    QComboBox *effectContinuousObjective_input;
    QLabel *effectContinuousObjective_label;
    QComboBox *effectContinuousEffect_input;
    QLabel *effectContinuousEffect_label;
    QLineEdit *effectContinuousValue_input;
    QLabel *effectContinuosValue_label;
    QHBoxLayout *effectContinuous_layout;

    //Metodi per la creazione dei differenti layout
    void buildEmptyLayout();
    void buildBasicLayout();
    void buildCreatureLayout();
    void buildManaGeneratorLayout();
    void buildMagicLayout();
    void buildMagicDamageEffectLayout();
    void buildMagicContinuousEffectLayout();

    //Metodi per il controllo sui dati inseriti
    bool failedCheckBasicData() const;
    bool failedCheckCreatureData() const;
    bool failedCheckManaGeneratorData() const;
    bool failedCheckMagicData() const;

    Effect* getEffect() const;

public:
    explicit CardEditorWidget(Card *card = nullptr, QWidget *parent = nullptr);
    void updateUIWith(Card *card);

public slots:
    void updateAfterDeletion();
};

#endif //QARDGAME_CARDEDITORWIDGET_H
