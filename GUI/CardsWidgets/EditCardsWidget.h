//
// Created by Marco Ferrati on 2019-05-09.
//

#ifndef QARDGAME_EDITCARDSWIDGET_H
#define QARDGAME_EDITCARDSWIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>

#include "../DecksWidgets/MainDeckListWidget.h"

#include "../../Model/Hierachy/Card.h"

class CardEditorWidget; //Dichiarazione incompleta per poter dichiarare un puntatore

class EditCardsWidget: public QWidget {
    Q_OBJECT
private:
    MainDeckListWidget *mainDeckListWidget;
    CardEditorWidget *cardEditorWidget;
    QHBoxLayout *main_layout;

    QPushButton *modify_button;
    QVBoxLayout *modifySection_layout;
public:
    explicit EditCardsWidget(QWidget *parent = nullptr);

public slots:
    void handleModifyButton();
    void changedDetailCard(Card *);
};


#endif //QARDGAME_EDITCARDSWIDGET_H
