//
// Created by Marco Ferrati on 2019-05-16.
//

#include "CardEditorWidget.h"

#include "../QUtils.h"

#include "../../Model/Hierachy/Creature.h"
#include "../../Model/Hierachy/ManaGenerator.h"
#include "../../Model/Hierachy/Magic.h"
#include "../../Model/Hierachy/Effects/Effect.h"
#include "../../Model/Hierachy/Effects/DamageEffect.h"
#include "../../Model/Hierachy/Effects/ContinuousEffect.h"
#include "../../Model/Hierachy/SpecialManaGenerator.h"
#include "../../Model/Hierachy/SpecialCreature.h"

CardEditorWidget::CardEditorWidget(Card *card, QWidget *parent): QWidget(parent), selectedCard(card) {
    main_layout = new QVBoxLayout(this);
    changable_layout = new QVBoxLayout();

    buildEmptyLayout();

    main_layout->addLayout(changable_layout);
}

void CardEditorWidget::buildEmptyLayout() {
    emptyCardTitle_label = new QLabel("<font size=8>Seleziona una carta</font>");
    emptyCardDescritpion_label = new QLabel(
            "Se la lista è vuota usare il pulsante + per creare una nuova carta ");
    emptyCard_layout = new QVBoxLayout();

    emptyCard_layout->addWidget(emptyCardTitle_label, 0, Qt::AlignTop);
    emptyCard_layout->addWidget(emptyCardDescritpion_label, 0, Qt::AlignTop);

    changable_layout->addLayout(emptyCard_layout);
}


void CardEditorWidget::buildBasicLayout() {
    //Nome della carta
    cardTitle_label = new QLabel("Nome della carta");

    cardTitle_input = new QLineEdit();
    cardTitle_input->setText(selectedCard->getTitle().c_str());

    cardTitle_layout = new QHBoxLayout();
    cardTitle_layout->addWidget(cardTitle_label);
    cardTitle_layout->addWidget(cardTitle_input);

    //Descrizione della carta
    cardDescription_label = new QLabel("Descrizione della carta");

    cardDescription_input = new QLineEdit();
    cardDescription_input->setText(selectedCard->getDescription().c_str());

    cardDescription_layout = new QHBoxLayout();
    cardDescription_layout->addWidget(cardDescription_label);
    cardDescription_layout->addWidget(cardDescription_input);

    //Costo in mana della carta
    cardManaCost_label = new QLabel("Costo in mana (da 0 a 99)");

    cardManaCost_input = new QLineEdit();
    cardManaCost_input->setText(std::to_string(selectedCard->getManaCost()).c_str());
    cardManaCost_input->setValidator(new QIntValidator(0, 99, cardManaCost_input));

    cardManaCost_layout = new QHBoxLayout();
    cardManaCost_layout->addWidget(cardManaCost_label);
    cardManaCost_layout->addWidget(cardManaCost_input);

    //Tipo della carta
    cardType_label = new QLabel("Tipo della carta");
    cardType_input = new QComboBox();

    cardType_layout = new QHBoxLayout();
    cardType_layout->addWidget(cardType_label);
    cardType_layout->addWidget(cardType_input);
}

void CardEditorWidget::buildCreatureLayout() {
    Creature *creature = dynamic_cast<Creature *>(selectedCard.operator->());
    if (!creature){
        QUtils::showAlert("Errore durante il caricamento della carta")->exec();
        QUtils::clearLayout(main_layout);
        buildEmptyLayout();
        main_layout->addLayout(changable_layout);
        return;
    }
    cardCreatureLife_label = new QLabel("Vita (da 1 a 99)");

    cardCreatureLife_input = new QLineEdit();
    cardCreatureLife_input->setValidator(new QIntValidator(1,99, cardCreatureLife_input));
    cardCreatureLife_input->setText(std::to_string(creature->getLife()).c_str());

    cardCreatureAtk_label = new QLabel("Attacco (da 0 a 99)");

    cardCreatureAtk_input = new QLineEdit();
    cardCreatureAtk_input->setValidator(new QIntValidator(0,99, cardCreatureAtk_input));
    cardCreatureAtk_input->setText(std::to_string(creature->getAtk()).c_str());

    cardCreatureDef_label = new QLabel("Difesa (da 0 a 99)");

    cardCreatureDef_input = new QLineEdit();
    cardCreatureDef_input->setValidator(new QIntValidator(0,99, cardCreatureDef_input));
    cardCreatureDef_input->setText(std::to_string(creature->getDef()).c_str());

    cardCreature_layout = new QHBoxLayout();
    cardCreature_layout->addWidget(cardCreatureLife_label);
    cardCreature_layout->addWidget(cardCreatureLife_input);
    cardCreature_layout->addWidget(cardCreatureAtk_label);
    cardCreature_layout->addWidget(cardCreatureAtk_input);
    cardCreature_layout->addWidget(cardCreatureDef_label);
    cardCreature_layout->addWidget(cardCreatureDef_input);

    changable_layout->addLayout(cardCreature_layout);
}

void CardEditorWidget::buildManaGeneratorLayout() {
    ManaGenerator *manaGenerator = dynamic_cast<ManaGenerator *>(selectedCard.operator->());
    if (!manaGenerator){
        QUtils::showAlert("Errore durante il caricamento della carta")->exec();
        QUtils::clearLayout(main_layout);
        buildEmptyLayout();
        main_layout->addLayout(changable_layout);
        return;
    }
    cardManaGeneratorIncrementValue_label = new QLabel("Valore di incremento (da 0 a 99)");

    cardManageneratorIncrementValue_input = new QLineEdit();
    cardManageneratorIncrementValue_input->setValidator(new QIntValidator(0,99));
    cardManageneratorIncrementValue_input->setText(std::to_string(manaGenerator->getIncrementValue()).c_str());

    cardManaGenerator_layout = new QHBoxLayout();
    cardManaGenerator_layout->addWidget(cardManaGeneratorIncrementValue_label);
    cardManaGenerator_layout->addWidget(cardManageneratorIncrementValue_input);

    changable_layout->addLayout(cardManaGenerator_layout);
}

void CardEditorWidget::buildMagicLayout() {
    Magic *magic = dynamic_cast<Magic *>(selectedCard.operator->());
    if (!magic){
        QUtils::showAlert("Errore durante il caricamento della carta")->exec();
        QUtils::clearLayout(main_layout);
        buildEmptyLayout();
        main_layout->addLayout(changable_layout);
        return;
    }
    cardMagicEffect_label = new QLabel("Tipo di magia");
    cardMagicEffect_input = new QComboBox();

    cardMagicEffectType_layout = new QHBoxLayout();
    cardMagicEffectType_layout->addWidget(cardMagicEffect_label);
    cardMagicEffectType_layout->addWidget(cardMagicEffect_input);

    changable_layout->addLayout(cardMagicEffectType_layout);

    switch (magic->getEffect()->getType()){
        case DamageType: {
            cardMagicEffect_input->addItem("Danno");
            cardMagicEffect_input->setDisabled(true);
            buildMagicDamageEffectLayout();
            break;
        }
        case ContinuousType: {
            cardMagicEffect_input->addItem("Continuo");
            cardMagicEffect_input->setDisabled(true);
            buildMagicContinuousEffectLayout();
            break;
        }
    }
}

void CardEditorWidget::buildMagicDamageEffectLayout() {
    Magic* magic= dynamic_cast<Magic*>(selectedCard.operator->());
    DamageEffect *effect = dynamic_cast<DamageEffect *>(magic->getEffect());
    if (!effect){
        QUtils::showAlert("Errore durante il caricamento della carta")->exec();
        QUtils::clearLayout(main_layout);
        buildEmptyLayout();
        main_layout->addLayout(changable_layout);
        return;
    }
    effectDamage_label = new QLabel("Danno (da 0 a 99");

    effectDamage_input = new QLineEdit();
    effectDamage_input->setValidator(new QIntValidator(0, 99, effectDamage_input));
    effectDamage_input->setText(std::to_string(effect->getDamage()).c_str());

    effectDamage_layout = new QHBoxLayout();
    effectDamage_layout->addWidget(effectDamage_label);
    effectDamage_layout->addWidget(effectDamage_input);

    changable_layout->addLayout(effectDamage_layout);
}

void CardEditorWidget::buildMagicContinuousEffectLayout() {
    Magic* magic= dynamic_cast<Magic*>(selectedCard.operator->());
    ContinuousEffect *effect = dynamic_cast<ContinuousEffect *>(magic->getEffect());
    if (!effect){
        QUtils::showAlert("Errore durante il caricamento della carta")->exec();
        QUtils::clearLayout(main_layout);
        buildEmptyLayout();
        main_layout->addLayout(changable_layout);
        return;
    }
    effectContinuousObjective_label = new QLabel("Caratteristica modificata");

    effectContinuousObjective_input = new QComboBox();
    effectContinuousObjective_input->addItem("Attacco");
    effectContinuousObjective_input->addItem("Difesa");
    effectContinuousObjective_input->setCurrentIndex(effect->getTypeModified());

    effectContinuousEffect_label = new QLabel("Tipo di modifica");

    effectContinuousEffect_input = new QComboBox();
    effectContinuousEffect_input->addItem("Incrementare");
    effectContinuousEffect_input->addItem("Decrementare");
    effectContinuousEffect_input->setCurrentIndex(effect->getValue() >= 0 ? 0 : 1);

    effectContinuosValue_label = new QLabel("Di quanto (da 0 a 99)");

    effectContinuousValue_input = new QLineEdit();
    effectContinuousValue_input->setValidator(new QIntValidator(0, 99, effectContinuousValue_input));
    effectContinuousValue_input->setText(effect->getValue() >= 0 ? std::to_string(effect->getValue()).c_str() : std::to_string(effect->getValue()*-1).c_str());

    effectContinuous_layout = new QHBoxLayout();
    effectContinuous_layout->addWidget(effectContinuousObjective_label);
    effectContinuous_layout->addWidget(effectContinuousObjective_input);
    effectContinuous_layout->addWidget(effectContinuousEffect_label);
    effectContinuous_layout->addWidget(effectContinuousEffect_input);
    effectContinuous_layout->addWidget(effectContinuosValue_label);
    effectContinuous_layout->addWidget(effectContinuousValue_input);

    changable_layout->addLayout(effectContinuous_layout);
}

bool CardEditorWidget::failedCheckBasicData() const {
    return cardTitle_input->text().isEmpty() || cardDescription_input->text().isEmpty() || cardManaCost_input->text().isEmpty();
}

bool CardEditorWidget::failedCheckCreatureData() const {
    return cardCreatureLife_input->text().isEmpty() || cardCreatureAtk_input->text().isEmpty() || cardCreatureDef_input->text().isEmpty();
}

bool CardEditorWidget::failedCheckManaGeneratorData() const {
    return cardManageneratorIncrementValue_input->text().isEmpty();
}

bool CardEditorWidget::failedCheckMagicData() const {
    Magic* magic = dynamic_cast<Magic*>(selectedCard.operator->());
    if (!magic)
        return true;

    switch (magic->getEffect()->getType()) {
        case DamageType:{ //Damage
            return effectDamage_input->text().isEmpty();
        }
        case ContinuousType:{ //Continuous
            return effectContinuousValue_input->text().isEmpty();
        }
    }
    return true;
}

Effect *CardEditorWidget::getEffect() const {
    Magic* magic = dynamic_cast<Magic*>(selectedCard.operator->());
    Effect* effectOfMagicPart;
    switch (magic->getEffect()->getType()) {
        case DamageType:{
            effectOfMagicPart = new DamageEffect(DamageType, effectDamage_input->text().toInt());
            break;
        }
        case ContinuousType:{
            int value = effectContinuousEffect_input->currentIndex() == 0 ? effectContinuousValue_input->text().toInt() : effectContinuousValue_input->text().toInt() * (-1);
            TypeModified type;
            switch (effectContinuousObjective_input->currentIndex()) {
                case 0:{ //attacco
                    type = atk;
                    break;
                }
                case 1:{ //difesa
                    type = def;
                    break;
                }
            }

            effectOfMagicPart = new ContinuousEffect(ContinuousType, value, type);
            break;
        }
    }

    return effectOfMagicPart;
}

void CardEditorWidget::updateUIWith(Card *card) {
    QUtils::clearLayout(main_layout);

    if (card == nullptr){
        buildEmptyLayout();
        main_layout->addLayout(changable_layout);
        return;
    }
    else{
        selectedCard = DeepPtr<Card>(card);
        changable_layout = new QVBoxLayout();
        buildBasicLayout();

        switch (selectedCard->getType()){
            case Creature_Type:{
                cardType_input->addItem("Creatura");
                cardType_input->setDisabled(true);
                buildCreatureLayout();
                break;
            }
            case ManaGenerator_Type:{
                cardType_input->addItem("Generatore di mana");
                cardType_input->setDisabled(true);
                buildManaGeneratorLayout();
                break;
            }
            case Magic_Type:{
                cardType_input->addItem("Magia");
                cardType_input->setDisabled(true);
                buildMagicLayout();
                break;
            }
            case SpecialCreature_Type:{
                cardType_input->addItem("Creatura con effetto");
                cardType_input->setDisabled(true);
                buildCreatureLayout();
                buildMagicLayout();
                break;
            }
            case SpecialManaGenerator_Type:{
                cardType_input->addItem("Generatore di mana con effetto");
                cardType_input->setDisabled(true);
                buildManaGeneratorLayout();
                buildMagicLayout();
                break;
            }
        default:
            std::cout << "Carta non riconosciuta" << std::endl;
        }

        //Aggiorno il main layout
        main_layout->addLayout(cardType_layout);
        main_layout->addLayout(cardTitle_layout);
        main_layout->addLayout(cardDescription_layout);
        main_layout->addLayout(cardManaCost_layout);
        main_layout->addLayout(changable_layout);
    }
}

void CardEditorWidget::updateAfterDeletion(){
    QUtils::clearLayout(main_layout);
    //buildEmptyLayout();
    //main_layout->addLayout(changable_layout);
}
