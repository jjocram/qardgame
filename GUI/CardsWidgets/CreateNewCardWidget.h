//
// Created by Marco Ferrati on 2019-05-07.
//

#ifndef QARDGAME_CREATENEWCARDWIDGET_H
#define QARDGAME_CREATENEWCARDWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>

#include "../../Model/Deck.h"
#include "../../Model/Hierachy/Effects/Effect.h"


class CreateNewCardWidget: public QWidget{
    Q_OBJECT
private:
    Deck mainDeck;

    //Layout
    QVBoxLayout *main_layout;

    QVBoxLayout *changable_layout;
    QHBoxLayout *magicEffect_layout;


    //Bottoni
    QPushButton *createButton;
    QPushButton *cancelButton;
    QHBoxLayout *buttons_layout;


    //Input e label per il tipo base (Card)
    QComboBox *cardType_input;
    QLabel *cardType_label;
    QHBoxLayout *cardType_layout;

    QLineEdit *cardTitle_input;
    QLabel *cardTitle_label;
    QHBoxLayout *cardTitle_layout;

    QLineEdit *cardDescription_input;
    QLabel *cardDescription_label;
    QHBoxLayout *cardDescription_layout;

    QLineEdit *cardManaCost_input;
    QLabel *cardManaCost_label;
    QHBoxLayout *cardManaCost_layout;


    //Input e label per il tipo derivato (Creature)
    QLineEdit *cardCreatureLife_input;
    QLabel *cardCreatureLife_label;

    QLineEdit *cardCreatureAtk_input;
    QLabel *cardCreatureAtk_label;

    QLineEdit *cardCreatureDef_input;
    QLabel *cardCreatureDef_label;

    QHBoxLayout *cardCreature_layout;

    //Input e label per il tipo derivato (ManaGenerator)
    QLineEdit *cardManaGeneratorIncrementValue_input;
    QLabel *cardManaGeneratorIncrementValue_label;

    QHBoxLayout *cardManaGenerator_layout;


    //Input e label per il tipo derivato (Magic)
    QComboBox *cardMagicEffect_input;
    QLabel *cardMagicEffect_label;

    QHBoxLayout *cardMagicEffect_layout;

    //Input e label per il dato effect in Magic
    QLineEdit *effectDamage_input;
    QLabel *effectDamage_label;

    QComboBox *effectContinuousTypeModified_input;
    QLabel *effectContinuousTypeModified_label;

    QComboBox *effectContinuousIncrementOrDecrememt_input;
    QLabel *effectContinuousIncrementOrDecrement_label;

    QLineEdit *effectContinuousValue_input;
    QLabel *effectContinuousValue_label;


    void buildCreatureLayout();
    void buildManaGeneratorLayout();
    void buildMagicLayout();
    void buildDamageEffectLayout();
    void buildContinuousEffectLayout();

    Effect *getEffectFromForm();

    bool failedCheckCreatureData() const;
    bool failedCheckManaGeneratorData() const;
    bool failedCheckMagicData() const;
public:
    explicit CreateNewCardWidget(QWidget *parent = nullptr);

signals:
    void createdNewCard();

private slots:
    void handleCreateButton();
    void handleCancelButton();
    void handleChangedCardType(int);
    void handleChangedMagicEffectType(int);
};


#endif //QARDGAME_CREATENEWCARDWIDGET_H
