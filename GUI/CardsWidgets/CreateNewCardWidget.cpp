//
// Created by Marco Ferrati on 2019-05-07.
//

#include "CreateNewCardWidget.h"

#include "ProjectException/ProjException.h"
#include "../QUtils.h"

#include "../../Model/Hierachy/Card.h"
#include "../../Model/Hierachy/Creature.h"
#include "../../Model/Hierachy/ManaGenerator.h"
#include "../../Model/Hierachy/Magic.h"
#include "../../Model/Hierachy/SpecialManaGenerator.h"
#include "../../Model/Hierachy/SpecialCreature.h"
#include "../../Model/Hierachy/Effects/Effect.h"
#include "../../Model/Hierachy/Effects/DamageEffect.h"
#include "../../Model/Hierachy/Effects/ContinuousEffect.h"

#include <QValidator>

#include <iostream>
using namespace std;

CreateNewCardWidget::CreateNewCardWidget(QWidget *parent): QWidget(parent) {
    setWindowTitle("Crea nuova carta");

    main_layout = new QVBoxLayout(this);
    changable_layout = new QVBoxLayout();
    magicEffect_layout = new QHBoxLayout();

    try {
        mainDeck = Deck::load(":decks/maindeck.xml");
    } catch (ProjException e){
        std::cerr << "[Warning]: " << e.what() << std::endl;
    }

    createButton = new QPushButton("Crea nuova carta", this);
    connect(createButton, SIGNAL(clicked()), this, SLOT(handleCreateButton()));

    cancelButton = new QPushButton("Cancella", this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(handleCancelButton()));

    buttons_layout = new QHBoxLayout();
    buttons_layout->addWidget(cancelButton);
    buttons_layout->addWidget(createButton);

    //--Card type selector--//
    cardType_label = new QLabel("Seleziona il tipo di carta");

    cardType_input = new QComboBox();
    cardType_input->addItem("Creatura");
    cardType_input->addItem("Generatore di Mana");
    cardType_input->addItem("Magia");
    cardType_input->addItem("Creatura con effetto");
    cardType_input->addItem("Generatore di Mana con effetto");
    connect(cardType_input, SIGNAL(currentIndexChanged(int)), this, SLOT(handleChangedCardType(int)));

    cardType_layout = new QHBoxLayout();
    cardType_layout->addWidget(cardType_label);
    cardType_layout->addWidget(cardType_input);


    //--Card name--//
    cardTitle_label = new QLabel("Nome della carta");
    cardTitle_input = new QLineEdit();
    cardTitle_layout = new QHBoxLayout();
    cardTitle_layout->addWidget(cardTitle_label);
    cardTitle_layout->addWidget(cardTitle_input);

    //--Card description--//
    cardDescription_label = new QLabel("Descrizione della carta");
    cardDescription_input = new QLineEdit();
    cardDescription_layout = new QHBoxLayout();
    cardDescription_layout->addWidget(cardDescription_label);
    cardDescription_layout->addWidget(cardDescription_input);

    //--Card Mana cost--//
    cardManaCost_label = new QLabel("Costo in mana (da 0 a 99)");
    cardManaCost_input = new QLineEdit();
    cardManaCost_input->setValidator(new QIntValidator(0, 99, cardManaCost_input));
    cardManaCost_layout = new QHBoxLayout();
    cardManaCost_layout->addWidget(cardManaCost_label);
    cardManaCost_layout->addWidget(cardManaCost_input);


    //Costruzione del creature layout in quanto primo elemento del type chooser
    buildCreatureLayout();

    main_layout->addLayout(cardType_layout);
    main_layout->addLayout(cardTitle_layout);
    main_layout->addLayout(cardDescription_layout);
    main_layout->addLayout(cardManaCost_layout);

    main_layout->addLayout(changable_layout);

    main_layout->addLayout(buttons_layout);
}

void CreateNewCardWidget::buildCreatureLayout() {
    cardCreatureLife_label = new QLabel("Vita (da 1 a 99)");
    cardCreatureLife_input = new QLineEdit();
    cardCreatureLife_input->setValidator(new QIntValidator(0, 99, cardCreatureLife_input));

    cardCreatureAtk_label = new QLabel("Attacco (da 1 a 99)");
    cardCreatureAtk_input = new QLineEdit();
    cardCreatureAtk_input->setValidator(new QIntValidator(0, 99, cardCreatureAtk_input));

    cardCreatureDef_label = new QLabel("Difesa (da 1 a 99)");
    cardCreatureDef_input = new QLineEdit();
    cardCreatureDef_input->setValidator(new QIntValidator(0, 99, cardCreatureDef_input));

    cardCreature_layout = new QHBoxLayout();
    cardCreature_layout->addWidget(cardCreatureLife_label);
    cardCreature_layout->addWidget(cardCreatureLife_input);
    cardCreature_layout->addWidget(cardCreatureAtk_label);
    cardCreature_layout->addWidget(cardCreatureAtk_input);
    cardCreature_layout->addWidget(cardCreatureDef_label);
    cardCreature_layout->addWidget(cardCreatureDef_input);

    changable_layout->addLayout(cardCreature_layout);
}

void CreateNewCardWidget::buildManaGeneratorLayout() {
    cardManaGeneratorIncrementValue_label = new QLabel("Valore di incremento (da 1 a 99)");
    cardManaGeneratorIncrementValue_input = new QLineEdit();
    cardManaGeneratorIncrementValue_input->setValidator(new QIntValidator(0, 99, cardManaGeneratorIncrementValue_input));

    cardManaGenerator_layout = new QHBoxLayout();
    cardManaGenerator_layout->addWidget(cardManaGeneratorIncrementValue_label);
    cardManaGenerator_layout->addWidget(cardManaGeneratorIncrementValue_input);

    changable_layout->addLayout(cardManaGenerator_layout);
}

void CreateNewCardWidget::buildMagicLayout() {
    cardMagicEffect_label = new QLabel("Tipo di magia");

    cardMagicEffect_input = new QComboBox();
    cardMagicEffect_input->addItem("Danno");
    cardMagicEffect_input->addItem("Continuo");
    connect(cardMagicEffect_input, SIGNAL(currentIndexChanged(int)), this, SLOT(handleChangedMagicEffectType(int)));

    cardMagicEffect_layout = new QHBoxLayout();

    buildDamageEffectLayout();

    changable_layout->addLayout(cardMagicEffect_layout);
    changable_layout->addLayout(magicEffect_layout);
}

void CreateNewCardWidget::buildDamageEffectLayout() {
    effectDamage_label = new QLabel("Danno (da 0 a 99)");
    effectDamage_input = new QLineEdit();
    effectDamage_input->setValidator(new QIntValidator(0, 99, effectDamage_input));

    magicEffect_layout->addWidget(effectDamage_label);
    magicEffect_layout->addWidget(effectDamage_input);
}

void CreateNewCardWidget::buildContinuousEffectLayout() {
    effectContinuousTypeModified_label = new QLabel("Caratteristica modificata");

    effectContinuousTypeModified_input = new QComboBox();
    effectContinuousTypeModified_input->addItem("Attacco");
    effectContinuousTypeModified_input->addItem("Difesa");

    effectContinuousIncrementOrDecrement_label = new QLabel("Come verrà modificata");

    effectContinuousIncrementOrDecrememt_input = new QComboBox();
    effectContinuousIncrementOrDecrememt_input->addItem("Aumentare");
    effectContinuousIncrementOrDecrememt_input->addItem("Decrementare");

    effectContinuousValue_label = new QLabel("Di quanto (da 0 a 99)");

    effectContinuousValue_input = new QLineEdit();
    effectContinuousValue_input->setValidator(new QIntValidator(0, 99, effectContinuousValue_input));

    magicEffect_layout->addWidget(effectContinuousTypeModified_label);
    magicEffect_layout->addWidget(effectContinuousTypeModified_input);
    magicEffect_layout->addWidget(effectContinuousIncrementOrDecrement_label);
    magicEffect_layout->addWidget(effectContinuousIncrementOrDecrememt_input);
    magicEffect_layout->addWidget(effectContinuousValue_label);
    magicEffect_layout->addWidget(effectContinuousValue_input);
}

Effect *CreateNewCardWidget::getEffectFromForm() {
    Effect *effect = nullptr;
    switch (cardMagicEffect_input->currentIndex()){
        case 0:{ //Damage
            if (effectDamage_input->text().isEmpty()){
                QUtils::showAlert("Dati mancanti", "Riempi tutti i dati per poter creare la carta")->exec();
                return nullptr;
            }
            effect = new DamageEffect(DamageType, effectDamage_input->text().toInt());
            break;
        }
        case 1:{ //Continuous
            if (effectContinuousValue_input->text().isEmpty()){
                QUtils::showAlert("Dati mancanti", "Riempi tutti i dati per poter creare la carta")->exec();
                return nullptr;
            }
            int value = effectContinuousIncrementOrDecrememt_input->currentIndex() == 0
                        ? effectContinuousValue_input->text().toInt() :
                        effectContinuousValue_input->text().toInt() * (-1);
            TypeModified typeModified = effectContinuousTypeModified_input->currentIndex() == 0 ? atk : def;
            effect = new ContinuousEffect(ContinuousType, value, typeModified);
            break;
        }
    }
    return effect;
}

bool CreateNewCardWidget::failedCheckCreatureData() const {
    return cardCreatureLife_input->text().isEmpty() || cardCreatureAtk_input->text().isEmpty() || cardCreatureDef_input->text().isEmpty();
}

bool CreateNewCardWidget::failedCheckManaGeneratorData() const {
    return cardManaGeneratorIncrementValue_input->text().isEmpty();
}

bool CreateNewCardWidget::failedCheckMagicData() const {
    switch (cardMagicEffect_input->currentIndex()){
        case 0:
            return effectDamage_input->text().isEmpty();
        case 1:
            return effectContinuousValue_input->text().isEmpty();
        default:
            return true;
    }
}

void CreateNewCardWidget::handleCreateButton() {
    if (cardTitle_input->text().isEmpty() || cardDescription_input->text().isEmpty() || cardManaCost_input->text().isEmpty()){
        QUtils::showAlert("Dati mancanti", "Riempi tutti i dati per poter creare la carta")->exec();
        return;
    }
    else{
        switch (cardType_input->currentIndex()){
            case 0:{ //Creature
                if (failedCheckCreatureData()){
                    QUtils::showAlert("Dati mancanti", "Riempi tutti i dati per poter creare la carta")->exec();
                    return;
                }
                mainDeck.addCard(new Creature(cardTitle_input->text().toStdString(),
                                              cardDescription_input->text().toStdString(),
                                              static_cast<unsigned int>(cardManaCost_input->text().toInt()), Creature_Type,
                                              cardCreatureAtk_input->text().toInt(), cardCreatureDef_input->text().toInt(),
                                              cardCreatureLife_input->text().toInt()));
                break;
            }
            case 1:{ //ManaGenerator
                if (failedCheckManaGeneratorData()){
                    QUtils::showAlert("Dati mancanti", "Riempi tutti i dati per poter creare la carta")->exec();
                    return;
                }
                mainDeck.addCard(new ManaGenerator(cardTitle_input->text().toStdString(),
                                                   cardDescription_input->text().toStdString(),
                                                   static_cast<unsigned int>(cardManaCost_input->text().toInt()),
                                                   ManaGenerator_Type,
                                                   static_cast<unsigned int>(cardManaGeneratorIncrementValue_input->text().toInt())));
                break;
            }
            case 2:{ //Magic
                if (failedCheckMagicData()){
                    QUtils::showAlert("Dati mancanti", "Riempi tutti i dati per poter creare la carta")->exec();
                    return;
                }

                Effect *effect = getEffectFromForm();
                if (effect == nullptr)
                    return;

                mainDeck.addCard(new Magic(cardTitle_input->text().toStdString(),
                                           cardDescription_input->text().toStdString(),
                                           static_cast<unsigned int>(cardManaCost_input->text().toInt()), Magic_Type, effect));
                break;
            }
            case 3:{ //Special Creature
                if (failedCheckCreatureData() || failedCheckMagicData()){
                    QUtils::showAlert("Dati mancanti", "Riempi tutti i dati per poter creare la carta")->exec();
                    return;
                }

                Effect *effect = getEffectFromForm();
                if (effect == nullptr)
                    return;

                mainDeck.addCard(new SpecialCreature(cardTitle_input->text().toStdString(),
                                                     cardDescription_input->text().toStdString(),
                                                     static_cast<unsigned int>(cardManaCost_input->text().toInt()),
                                                     SpecialCreature_Type,
                                                     cardCreatureAtk_input->text().toInt(),
                                                     cardCreatureDef_input->text().toInt(),
                                                     cardCreatureLife_input->text().toInt(), effect));
                break;
            }
            case 4:{ //Special ManaGenerator
                if (failedCheckManaGeneratorData() || failedCheckMagicData()){
                    QUtils::showAlert("Dati mancanti", "Riempi tutti i dati per poter creare la carta")->exec();
                    return;
                }

                Effect *effect = getEffectFromForm();
                if (effect == nullptr)
                    return;

                mainDeck.addCard(new SpecialManaGenerator(cardTitle_input->text().toStdString(),
                                                          cardDescription_input->text().toStdString(),
                                                          static_cast<unsigned int>(cardManaCost_input->text().toInt()),
                                                          SpecialManaGenerator_Type,
                                                          static_cast<unsigned int>(cardManaGeneratorIncrementValue_input->text().toInt()),
                                                          effect));

                break;
            }
        }

        //Salvataggio del deck con la nuova carta
        try {
            mainDeck.save("decks/maindeck.xml");
            emit(createdNewCard());
            close();
        }
        catch (ProjException e){
            QUtils::showAlert("Problema durante il salvataggio", e.what())->exec();
        }

    }
}

void CreateNewCardWidget::handleCancelButton() {
    close();
}

void CreateNewCardWidget::handleChangedCardType(int index) {
    QUtils::clearLayout(changable_layout);

    switch (index){
        case 0: {
            buildCreatureLayout();
            break;
        }
        case 1: {
            buildManaGeneratorLayout();
            break;
        }
        case 2: {
            buildMagicLayout();
            break;
        }
        case 3: {
            buildCreatureLayout();
            buildMagicLayout();
            break;
        }
        case 4: {
            buildManaGeneratorLayout();
            buildMagicLayout();
            break;
        }
        default:
            std::cerr << "Tipo non riconosciuto" << std::endl;
            break;
    }
}

void CreateNewCardWidget::handleChangedMagicEffectType(int index) {
    QUtils::clearLayout(magicEffect_layout);

    switch (index){
        case 0:{
            buildDamageEffectLayout();
            break;
        }
        case 1:{
            buildContinuousEffectLayout();
            break;
        }
        default:
            std::cerr << "Tipo di magia non riconosciuto" << std::endl;
            break;
    }
}
