//
// Created by Marco Ferrati on 2019-05-09.
//

#include "EditCardsWidget.h"

#include "CardEditorWidget.h"

#include "ProjectException/ProjException.h"
#include "../QUtils.h"

#include "../../Model/Hierachy/ManaGenerator.h"

#include <iostream>
#include <Model/Hierachy/Magic.h>
#include <Model/Hierachy/SpecialCreature.h>
#include <Model/Hierachy/SpecialManaGenerator.h>

using namespace std;

EditCardsWidget::EditCardsWidget(QWidget *parent) : QWidget(parent) {
    setWindowTitle("Modifica le carte");

    main_layout = new QHBoxLayout(this);

    mainDeckListWidget = new MainDeckListWidget();
    connect(mainDeckListWidget, SIGNAL(changedCard(Card * )), this, SLOT(changedDetailCard(Card * )));


    cardEditorWidget = new CardEditorWidget();
    modify_button = new QPushButton("Applica modifiche");
    modify_button->setMaximumWidth(250);
    connect(modify_button, SIGNAL(clicked()), this, SLOT(handleModifyButton()));
    connect(modify_button, SIGNAL(clicked()), mainDeckListWidget, SLOT(handleCardChanged()));
    connect(mainDeckListWidget, SIGNAL(deletedCard()), cardEditorWidget, SLOT(updateAfterDeletion()));

    modifySection_layout = new QVBoxLayout();
    modifySection_layout->addWidget(cardEditorWidget);
    modifySection_layout->addWidget(modify_button);

    main_layout->addWidget(mainDeckListWidget);
    main_layout->addLayout(modifySection_layout);
}

void EditCardsWidget::handleModifyButton() {
    if (mainDeckListWidget->mainDeck_list->count() <= 0)
        return;

    //Aggiornamento della carta selelzionata la carta selezionata
    if (cardEditorWidget->failedCheckBasicData()) {
        QUtils::showAlert("Dati mancanti")->exec();
        return;
    }

    mainDeckListWidget->mainDeck_list->currentItem()->setText(
            cardEditorWidget->cardTitle_input->text());//Aggiornamento del nome nella lista

    switch (cardEditorWidget->selectedCard->getType()) {
        case Creature_Type: {
            if (cardEditorWidget->failedCheckCreatureData()) {
                QUtils::showAlert("Dati mancanti")->exec();
                return;
            }
            mainDeckListWidget->mainDeck.replace(mainDeckListWidget->selectedItemRow(),
                                                 DeepPtr<Card>(new Creature(cardEditorWidget->cardTitle_input->text().toStdString(),
                                                              cardEditorWidget->cardDescription_input->text().toStdString(),
                                                              static_cast<unsigned int>(cardEditorWidget->cardManaCost_input->text().toInt()),
                                                              Creature_Type,
                                                              cardEditorWidget->cardCreatureAtk_input->text().toInt(),
                                                              cardEditorWidget->cardCreatureDef_input->text().toInt(),
                                                              cardEditorWidget->cardCreatureLife_input->text().toInt()))
            );
            break;
        }
        case ManaGenerator_Type: {
            if (cardEditorWidget->failedCheckManaGeneratorData()) {
                QUtils::showAlert("Dati mancanti")->exec();
                return;
            }
            mainDeckListWidget->mainDeck.replace(mainDeckListWidget->selectedItemRow(), DeepPtr<Card>(new ManaGenerator(
                    cardEditorWidget->cardTitle_input->text().toStdString(),
                    cardEditorWidget->cardDescription_input->text().toStdString(),
                    static_cast<unsigned int>(cardEditorWidget->cardManaCost_input->text().toInt()),
                    ManaGenerator_Type,
                    static_cast<unsigned int>(cardEditorWidget->cardManageneratorIncrementValue_input->text().toInt())))
            );
            break;
        }
        case Magic_Type: {
            if (cardEditorWidget->failedCheckMagicData()) {
                QUtils::showAlert("Dati mancanti")->exec();
                return;
            }
            mainDeckListWidget->mainDeck.replace(mainDeckListWidget->selectedItemRow(), DeepPtr<Card>(new Magic(cardEditorWidget->cardTitle_input->text().toStdString(),
                                                                                           cardEditorWidget->cardDescription_input->text().toStdString(),
                                                                                           static_cast<unsigned int>(cardEditorWidget->cardManaCost_input->text().toInt()),
                                                                                           Magic_Type,
                                                                                           cardEditorWidget->getEffect())));
            break;
        }
        case SpecialCreature_Type: {
            if (cardEditorWidget->failedCheckCreatureData() || cardEditorWidget->failedCheckMagicData()) {
                QUtils::showAlert("Dati mancanti")->exec();
                return;
            }
            mainDeckListWidget->mainDeck.replace(mainDeckListWidget->selectedItemRow(), DeepPtr<Card>(new SpecialCreature(
                    cardEditorWidget->cardTitle_input->text().toStdString(),
                    cardEditorWidget->cardDescription_input->text().toStdString(),
                    static_cast<unsigned int>(cardEditorWidget->cardManaCost_input->text().toInt()),
                    SpecialCreature_Type,
                    cardEditorWidget->cardCreatureAtk_input->text().toInt(),
                    cardEditorWidget->cardCreatureDef_input->text().toInt(),
                    cardEditorWidget->cardCreatureLife_input->text().toInt(),
                    cardEditorWidget->getEffect()))
            );
            break;
        }
        case SpecialManaGenerator_Type: {
            if (cardEditorWidget->failedCheckManaGeneratorData() || cardEditorWidget->failedCheckMagicData()){
                QUtils::showAlert("Dati mancanti")->exec();
                return;
            }
            mainDeckListWidget->mainDeck.replace(mainDeckListWidget->selectedItemRow(), DeepPtr<Card>(new SpecialManaGenerator(cardEditorWidget->cardTitle_input->text().toStdString(),
                                                                                                          cardEditorWidget->cardDescription_input->text().toStdString(),
                                                                                                          static_cast<unsigned int>(cardEditorWidget->cardManaCost_input->text().toInt()),
                                                                                                          SpecialManaGenerator_Type,
                                                                                                          static_cast<unsigned int>(cardEditorWidget->cardManageneratorIncrementValue_input->text().toInt()),
                                                                                                          cardEditorWidget->getEffect()))
            );
            break;
        }
    }

    try {
        mainDeckListWidget->mainDeck.save("decks/maindeck.xml");
    } catch (ProjException e) {
        QUtils::showAlert("Impossibile salvare il deck", e.what())->exec();
    }
}

void EditCardsWidget::changedDetailCard(Card *card) {
    cardEditorWidget->updateUIWith(card->clone());
}
