#include "DeckCreatorWidget.h"

#include "../QUtils.h"
#include "QDir"
#include "ProjectException/ProjException.h"

DeckCreatorWidget::DeckCreatorWidget(const Deck& deckToModify, QWidget *parent) : QWidget(parent), newDeck(deckToModify) {
    main_layout = new QHBoxLayout(this);

    mainDeck_list = new MainDeckListWidget();
    connect(mainDeck_list->mainDeck_list, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(handleDoubleClickOnMainDeckList(QListWidgetItem *)));

    moveToNewDeck_button = new QPushButton(">>");
    removeFromNewDeck_button = new QPushButton("<<");

    buttonsForNewDeck_layout = new QVBoxLayout();
    buttonsForNewDeck_layout->addWidget(moveToNewDeck_button);
    buttonsForNewDeck_layout->addWidget(removeFromNewDeck_button);

    deckName_input = new QLineEdit();
    newDeckCards_list = new QListWidget();
    connect(newDeckCards_list, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(handleDoubleClickOnNewDeckList(QListWidgetItem *)));
    createNewDeck_button = new QPushButton("Crea");

    newDeck_layout = new QVBoxLayout();
    newDeck_layout->addWidget(deckName_input);
    newDeck_layout->addWidget(newDeckCards_list);
    newDeck_layout->addWidget(createNewDeck_button);

    main_layout->addWidget(mainDeck_list);
    main_layout->addLayout(buttonsForNewDeck_layout);
    main_layout->addLayout(newDeck_layout);

    if (newDeck.getNumberOfCards() == 0){
        //Creazione di un nuovo deck
        setWindowTitle("Crea nuovo deck");
        connect(createNewDeck_button, SIGNAL(clicked()), this, SLOT(handleCreateNewDeck()));
    }
    else {
        //Modifica di un deck esistente
        setWindowTitle("Modifca di un deck");
        for (unsigned int i = 0; i < deckToModify.getNumberOfCards(); ++i) {
                    newDeckCards_list->addItem(newDeck.getCard(i)->getTitle().c_str());
                }
                deckName_input->setText(newDeck.getName().c_str());
                createNewDeck_button->setText("Modifica");
                connect(createNewDeck_button, SIGNAL(clicked()), this, SLOT(handleModifyDeck()));
    }
    connect(moveToNewDeck_button, SIGNAL(clicked()), this, SLOT(handleMoveToNewDeck()));
    connect(removeFromNewDeck_button, SIGNAL(clicked()), this, SLOT(handleRemoveFromNewDeck()));
}

QString DeckCreatorWidget::getNewDeckName() const{
    return deckName_input->text();
}

bool DeckCreatorWidget::failedToCheckData() const {
    if (deckName_input->text().isEmpty()){
        QUtils::showAlert("<font size=8>Nome mancante</font>", "Inserire un nome per il nuovo deck nell'apposita casella di input")->exec();
        return true;
    }
    if (deckName_input->text() == "maindeck"){
        QUtils::showAlert("<font size=8>Nome non valido</font>", "Questo nome è già in uso per la gestione delle carte da parte dle programma, si è pregati di sceglierne un altro")->exec();
        return true;
    }
    return false;
}

void DeckCreatorWidget::handleMoveToNewDeck() {
    QString title = mainDeck_list->inSearchMode ? mainDeck_list->searchedDeck.getCard(static_cast<unsigned int>(mainDeck_list->selectedItemRow()))->getTitle().c_str() : mainDeck_list->mainDeck.getCard(static_cast<unsigned int>(mainDeck_list->selectedItemRow()))->getTitle().c_str();
    newDeckCards_list->addItem(title);
    newDeck.addCard(mainDeck_list->inSearchMode ? mainDeck_list->searchedDeck.getCard(static_cast<unsigned int>(mainDeck_list->selectedItemRow())) : mainDeck_list->mainDeck.getCard(static_cast<unsigned int>(mainDeck_list->selectedItemRow())));
}

void DeckCreatorWidget::handleRemoveFromNewDeck() {
    int index = newDeckCards_list->currentRow();
    newDeckCards_list->takeItem(index);
    newDeck.removeCard(static_cast<unsigned int>(index));
}

void DeckCreatorWidget::handleCreateNewDeck() {
    if (failedToCheckData())
        return;

    newDeck.setName(deckName_input->text().toStdString());
    if (!QDir("decks").exists())
         QDir().mkdir("decks");

        try {
            newDeck.save("decks/" + deckName_input->text().toStdString() + ".xml");
            emit(newDeckCreated(newDeck));
            close();
        } catch (ProjException e) {
            QUtils::showAlert("Impossibile salvare il nuovo deck", e.what());
        }
}

void DeckCreatorWidget::handleModifyDeck() {
    if (failedToCheckData())
        return;

    if (QDir("decks").remove(QString::fromStdString(newDeck.getName() + ".xml"))){
        newDeck.setName(deckName_input->text().toStdString());

        try {
            newDeck.save("decks/" + deckName_input->text().toStdString() + ".xml");
            emit(deckWasModified(newDeck));
        } catch (ProjException e) {
            QUtils::showAlert("Impossibile modificare deck", e.what());
        }
    }
}

void DeckCreatorWidget::handleDoubleClickOnMainDeckList(QListWidgetItem *item) {
    unsigned int index = mainDeck_list->mainDeck_list->row(item);
    QString title = mainDeck_list->inSearchMode ? mainDeck_list->searchedDeck.getCard(index)->getTitle().c_str() : mainDeck_list->mainDeck.getCard(index)->getTitle().c_str();
    newDeckCards_list->addItem(title);
    newDeck.addCard(mainDeck_list->inSearchMode ? mainDeck_list->searchedDeck.getCard(index) : mainDeck_list->mainDeck.getCard(index));
}

void DeckCreatorWidget::handleDoubleClickOnNewDeckList(QListWidgetItem *) {
    int index = newDeckCards_list->currentRow();
    newDeckCards_list->takeItem(index);
    newDeck.removeCard(static_cast<unsigned int>(index));
}
