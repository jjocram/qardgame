//
// Created by Marco Ferrati on 2019-05-09.
//

#ifndef QARDGAME_MAINDECKLISTWIDGET_H
#define QARDGAME_MAINDECKLISTWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QListWidget>
#include <QListWidgetItem>

#include <QKeyEvent>

#include "../../Model/Deck.h"

class EditCardsWidget;
class DeckCreatorWidget;

class MainDeckListWidget: public QWidget {
    Q_OBJECT
    friend EditCardsWidget;
    friend DeckCreatorWidget;
private:
    Deck mainDeck;
    Deck searchedDeck;

    QPushButton *resetReasearch_button;
    bool inSearchMode;

    QVBoxLayout *main_layout;

    QVBoxLayout *list_layout;
    QHBoxLayout *listOptions_layout;

    QListWidget *mainDeck_list;

    QHBoxLayout *search_layout;
    QLineEdit *searchBar_input;
    QPushButton *showFilters_button;
    QPushButton *add_button;
    QPushButton *remove_button;

    void keyReleaseEvent(QKeyEvent *event);
public:
    explicit MainDeckListWidget(QWidget *parent = nullptr);
    Deck& getSelectedDeck();
    int selectedItemRow() const;
signals:
    void changedCard(Card*);
    void deletedCard();
public slots:
    void handleSwitchItem(QListWidgetItem *oldItem, QListWidgetItem *newItem);
    void handleAddButton();
    void handleRemoveButton();
    void handleShowFiltersButton();
    void handleSearchFilters(const Deck& deck);
    void handleReserchResearchButton();
    void handleMainDeckChanged();
    void handleCardChanged();
};


#endif //QARDGAME_MAINDECKLISTWIDGET_H
