//
// Created by Marco Ferrati on 2019-05-14.
//

#ifndef QARDGAME_SEARCHFILTERWIDGET_H
#define QARDGAME_SEARCHFILTERWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLineEdit>
#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>

#include "../../Model/Deck.h"
#include "../../Model/Hierachy/Creature.h"
#include "../../Model/Hierachy/ManaGenerator.h"
#include "../../Model/Hierachy/Magic.h"

class SearchFilterWidget:public QWidget {
    Q_OBJECT
private:
    Deck searchDeck;
    Deck findedCards;

    //Different Layout
    QVBoxLayout *main_layout;
    QHBoxLayout *creature_layout;
    QHBoxLayout *manaGenerator_layout;
    QVBoxLayout *magic_layout;
    QHBoxLayout *effect_layout;

    QPushButton *search_button;

    //Basic card info
    QLineEdit *cardName_input;
    QLabel *cardName_label;
    QHBoxLayout *cardName_layout;
    QLineEdit *cardDescription_input;
    QLabel *cardDescription_label;
    QHBoxLayout *cardDescription_layout;
    QLineEdit *cardManaCost_input;
    QLabel *cardManaCost_label;
    QHBoxLayout *cardManaCost_layout;

    //Type of card
    QCheckBox *creature_checkBox;
    QCheckBox *manaGenerator_checkBox;
    QCheckBox *magic_checkBox;
    QCheckBox *specialCreature_checkBox;
    QCheckBox *specialManaGenerator_checkBox;
    QHBoxLayout *firstCheckBox_layout;
    QHBoxLayout *secondCheckBox_layout;
    QGridLayout *checkBoxes_layout;

    //Creature info
    QLabel *cardCreatureLife_label;
    QLabel *cardCreatureAtk_label;
    QLabel *cardCreatureDef_label;
    QCheckBox *allCreature_checkBox;
    QLineEdit *cardCreatureLife_input;
    QComboBox *cardCreatureLife_operator;
    QComboBox *cardCreatureAtk_operator;
    QLineEdit *cardCreatureAtk_input;
    QLineEdit *cardCreatureDef_input;
    QComboBox *cardCreatureDef_operator;

    //ManaGenerator info
    QCheckBox *allManaGenerator_checkBox;
    QLabel *cardManaGeneratorIncrementValue_label;
    QLineEdit *cardManaGeneratorIncrementValue_input;
    QComboBox *cardManaGeneratorIncrementValue_opearator;

    //Magic and effect info
    QCheckBox *allTypeOfMagic_checkBox;
    QComboBox *typeOfMagic;
    QHBoxLayout *selectTypeOfMagic_layout;
    QLabel *damageEffectValue_label;
    QLineEdit *damageEffectValue_input;
    QComboBox *damageEffectValue_operator;
    QCheckBox *allObjectivesOfContinuousEffect;
    QComboBox *objectiveOfContinuousEffect;
    QCheckBox *allTypeOfContinuousEffect;
    QComboBox *typeOfContinuousEffect;
    QLabel* continuousEffectValue_label;
    QLineEdit *continuousEffectValue_input;
    QComboBox *continuousEffectValue_operator;

    //Metodi per la costruzione dei differenti layout
    void buildCreatureSearchLayout();
    void buildManaGeneratorSearchLayout();
    void buildMagicSearchLayout();
    void buildDamageEffectLayout();
    void buildContinuousEffectLayout();

    bool findCreature(bool searchResult, Creature* creature) const;
    bool findManaGenerator(bool searchResult, ManaGenerator* manaGenerator) const;
    bool findMagic(bool searchResult, Magic* magic) const;

public:
    SearchFilterWidget(const Deck& deck, QWidget *parent = nullptr);

signals:
    void clickedSearchButton(const Deck &);

public slots:
    void handleCreatureCheckBox(int);
    void handleManaGeneratorCheckBox(int);
    void handleMagicCheckBox(int);
    void handleSpecialCreatureCheckBox(int);
    void handleSpecilaManaGeneratorCheckBox(int);
    void handleAllTypeOfMagicCheckBox(int);
    void handleTypeOfMagicChanged(int);
    void handleObjectiveOfEffectCheckBox(int);
    void handleTypeOfEffectCheckBox(int);
    void handleAllCreatureCheckBox(int);
    void handleAllManaGeneratorCheckBox(int);
    void handleSearchButton();

};


#endif //QARDGAME_SEARCHFILTERWIDGET_H
