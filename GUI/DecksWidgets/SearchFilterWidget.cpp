//
// Created by Marco Ferrati on 2019-05-14.
//

#include "SearchFilterWidget.h"

#include "../QUtils.h"

#include "../../Model/Hierachy/SpecialCreature.h"
#include "../../Model/Hierachy/SpecialManaGenerator.h"
#include "../../Model/Hierachy/Effects/DamageEffect.h"
#include "../../Model/Hierachy/Effects/ContinuousEffect.h"

SearchFilterWidget::SearchFilterWidget(const Deck &deck, QWidget *parent): QWidget(parent), searchDeck(deck) {
    setWindowTitle("Cerca carte");
    main_layout = new QVBoxLayout(this);

    creature_layout = nullptr;
    manaGenerator_layout = nullptr;
    magic_layout = nullptr;
    effect_layout = nullptr;

    //Nome
    cardName_label = new QLabel("Nome");
    cardName_input = new QLineEdit();
    cardName_layout = new QHBoxLayout();
    cardName_layout->addWidget(cardName_label);
    cardName_layout->addWidget(cardName_input);

    //Descrizione
    cardDescription_label = new QLabel("Descrizione");
    cardDescription_input = new QLineEdit();
    cardDescription_layout = new QHBoxLayout();
    cardDescription_layout->addWidget(cardDescription_label);
    cardDescription_layout->addWidget(cardDescription_input);

    //ManaCost
    cardManaCost_label = new QLabel("Costo in mana");
    cardManaCost_input = new QLineEdit();
    cardManaCost_layout = new QHBoxLayout();
    cardManaCost_layout->addWidget(cardManaCost_label);
    cardManaCost_layout->addWidget(cardManaCost_input);

    //CheckBox per il tipo
    creature_checkBox = new QCheckBox("Creatura");
    connect(creature_checkBox, SIGNAL(stateChanged(int)), this, SLOT(handleCreatureCheckBox(int)));

    manaGenerator_checkBox = new QCheckBox("Generatore di mana");
    connect(manaGenerator_checkBox, SIGNAL(stateChanged(int)), this, SLOT(handleManaGeneratorCheckBox(int)));

    magic_checkBox = new QCheckBox("Magia");
    connect(magic_checkBox, SIGNAL(stateChanged(int)), this, SLOT(handleMagicCheckBox(int)));

    specialCreature_checkBox = new QCheckBox("Creatura con effetto");
    connect(specialCreature_checkBox, SIGNAL(stateChanged(int)), this, SLOT(handleSpecialCreatureCheckBox(int)));

    specialManaGenerator_checkBox = new QCheckBox("Generatore di mana con effetto");
    connect(specialManaGenerator_checkBox, SIGNAL(stateChanged(int)), this, SLOT(handleSpecilaManaGeneratorCheckBox(int)));

    //Layout in due colonne per le checkbox
    firstCheckBox_layout = new QHBoxLayout();
    firstCheckBox_layout->addWidget(creature_checkBox);
    firstCheckBox_layout->addWidget(manaGenerator_checkBox);
    firstCheckBox_layout->addWidget(magic_checkBox);
    secondCheckBox_layout = new QHBoxLayout();
    secondCheckBox_layout->addWidget(specialCreature_checkBox);
    secondCheckBox_layout->addWidget(specialManaGenerator_checkBox);
    //Layout finale per le checkbox
    checkBoxes_layout = new QGridLayout();
    checkBoxes_layout->addLayout(firstCheckBox_layout,0,0);
    checkBoxes_layout->addLayout(secondCheckBox_layout,1,0);

    search_button = new QPushButton("Ricerca");
    connect(search_button, SIGNAL(clicked()), this, SLOT(handleSearchButton()));

    main_layout->addWidget(search_button);
    main_layout->addLayout(cardName_layout);
    main_layout->addLayout(cardDescription_layout);
    main_layout->addLayout(cardManaCost_layout);
    main_layout->addLayout(checkBoxes_layout);

}

void SearchFilterWidget::buildCreatureSearchLayout() {
    creature_layout = new QHBoxLayout();

    allCreature_checkBox = new QCheckBox("Tutte le creature");
    allCreature_checkBox->setCheckState(Qt::Checked);
    connect(allCreature_checkBox, SIGNAL(stateChanged(int)), this, SLOT(handleAllCreatureCheckBox(int)));

    cardCreatureLife_label = new QLabel("Vita");
    cardCreatureLife_input = new QLineEdit();
    cardCreatureLife_input->setValidator(new QIntValidator(0,99));
    cardCreatureLife_operator = new QComboBox();
    cardCreatureLife_operator->addItem("Minore di");
    cardCreatureLife_operator->addItem("Uguale a");
    cardCreatureLife_operator->addItem("Maggiore di");
    cardCreatureLife_operator->setCurrentIndex(1);

    cardCreatureAtk_label = new QLabel("Attacco");
    cardCreatureAtk_input = new QLineEdit();
    cardCreatureAtk_input->setValidator(new QIntValidator(0,99));
    cardCreatureAtk_operator = new QComboBox();
    cardCreatureAtk_operator->addItem("Minore di");
    cardCreatureAtk_operator->addItem("Uguale a");
    cardCreatureAtk_operator->addItem("Maggiore di");
    cardCreatureAtk_operator->setCurrentIndex(1);

    cardCreatureDef_label = new QLabel("Difesa");
    cardCreatureDef_input = new QLineEdit();
    cardCreatureDef_input->setValidator(new QIntValidator(0,99));
    cardCreatureDef_operator = new QComboBox();
    cardCreatureDef_operator->addItem("Minore di");
    cardCreatureDef_operator->addItem("Uguale a ");
    cardCreatureDef_operator->addItem("Maggiore di");
    cardCreatureDef_operator->setCurrentIndex(1);

    cardCreatureLife_label->setVisible(false);
    cardCreatureLife_input->setVisible(false);
    cardCreatureLife_operator->setVisible(false);
    cardCreatureAtk_label->setVisible(false);
    cardCreatureAtk_input->setVisible(false);
    cardCreatureAtk_operator->setVisible(false);
    cardCreatureDef_label->setVisible(false);
    cardCreatureDef_input->setVisible(false);
    cardCreatureDef_operator->setVisible(false);

    creature_layout->addWidget(allCreature_checkBox);
    creature_layout->addWidget(cardCreatureLife_label);
    creature_layout->addWidget(cardCreatureLife_operator);
    creature_layout->addWidget(cardCreatureLife_input);
    creature_layout->addWidget(cardCreatureAtk_label);
    creature_layout->addWidget(cardCreatureAtk_operator);
    creature_layout->addWidget(cardCreatureAtk_input);
    creature_layout->addWidget(cardCreatureDef_label);
    creature_layout->addWidget(cardCreatureDef_operator);
    creature_layout->addWidget(cardCreatureDef_input);
}

void SearchFilterWidget::buildManaGeneratorSearchLayout() {
    manaGenerator_layout = new QHBoxLayout();

    allManaGenerator_checkBox = new QCheckBox("Tutti i generatori di mana");
    allManaGenerator_checkBox->setCheckState(Qt::Checked);
    connect(allManaGenerator_checkBox, SIGNAL(stateChanged(int)), this, SLOT(handleAllManaGeneratorCheckBox(int)));

    cardManaGeneratorIncrementValue_label = new QLabel("Valore d'incremento");
    cardManaGeneratorIncrementValue_input = new QLineEdit();
    cardManaGeneratorIncrementValue_input->setValidator(new QIntValidator(0,99));
    cardManaGeneratorIncrementValue_opearator = new QComboBox();
    cardManaGeneratorIncrementValue_opearator->addItem("Minore di");
    cardManaGeneratorIncrementValue_opearator->addItem("Uguale a ");
    cardManaGeneratorIncrementValue_opearator->addItem("Maggiore di");
    cardManaGeneratorIncrementValue_opearator->setCurrentIndex(1);

    cardManaGeneratorIncrementValue_label->setVisible(false);
    cardManaGeneratorIncrementValue_input->setVisible(false);
    cardManaGeneratorIncrementValue_opearator->setVisible(false);

    manaGenerator_layout->addWidget(allManaGenerator_checkBox);
    manaGenerator_layout->addWidget(cardManaGeneratorIncrementValue_label);
    manaGenerator_layout->addWidget(cardManaGeneratorIncrementValue_opearator);
    manaGenerator_layout->addWidget(cardManaGeneratorIncrementValue_input);
}

void SearchFilterWidget::buildMagicSearchLayout() {
    magic_layout = new QVBoxLayout();
    selectTypeOfMagic_layout = new QHBoxLayout();

    allTypeOfMagic_checkBox = new QCheckBox("Tutti i tipi di magia");
    allTypeOfMagic_checkBox->setCheckState(Qt::Checked);
    connect(allTypeOfMagic_checkBox, SIGNAL(stateChanged(int)), this, SLOT(handleAllTypeOfMagicCheckBox(int)));

    typeOfMagic = new QComboBox();
    typeOfMagic->addItem("Danno");
    typeOfMagic->addItem("Continuo");
    typeOfMagic->setVisible(false);
    connect(typeOfMagic, SIGNAL(currentIndexChanged(int)), this, SLOT(handleTypeOfMagicChanged(int)));

    selectTypeOfMagic_layout->addWidget(allTypeOfMagic_checkBox);
    selectTypeOfMagic_layout->addWidget(typeOfMagic);

    magic_layout->addLayout(selectTypeOfMagic_layout);
}

void SearchFilterWidget::buildDamageEffectLayout() {
    effect_layout = new QHBoxLayout();

    damageEffectValue_label = new QLabel("Danno inflitto");
    damageEffectValue_input = new QLineEdit();
    damageEffectValue_input->setValidator(new QIntValidator(0,99));
    damageEffectValue_operator = new QComboBox();
    damageEffectValue_operator->addItem("Minore di");
    damageEffectValue_operator->addItem("Uguale a ");
    damageEffectValue_operator->addItem("Maggiore di");
    damageEffectValue_operator->setCurrentIndex(1);

    effect_layout->addWidget(damageEffectValue_label);
    effect_layout->addWidget(damageEffectValue_operator);
    effect_layout->addWidget(damageEffectValue_input);

    magic_layout->addLayout(effect_layout);
}

void SearchFilterWidget::buildContinuousEffectLayout() {
    effect_layout = new QHBoxLayout();

    allObjectivesOfContinuousEffect = new QCheckBox("Tutti gli obiettivi");
    allObjectivesOfContinuousEffect->setCheckState(Qt::Checked);
    connect(allObjectivesOfContinuousEffect, SIGNAL(stateChanged(int)), this, SLOT(handleObjectiveOfEffectCheckBox(int)));

    objectiveOfContinuousEffect = new QComboBox();
    objectiveOfContinuousEffect->addItem("Attacco");
    objectiveOfContinuousEffect->addItem("Difesa");
    objectiveOfContinuousEffect->setVisible(false);

    allTypeOfContinuousEffect = new QCheckBox("Tutti i tipi");
    allTypeOfContinuousEffect->setCheckState(Qt::Checked);
    connect(allTypeOfContinuousEffect, SIGNAL(stateChanged(int)), this, SLOT(handleTypeOfEffectCheckBox(int)));

    typeOfContinuousEffect = new QComboBox();
    typeOfContinuousEffect->addItem("Incrementa");
    typeOfContinuousEffect->addItem("Decrementa");
    typeOfContinuousEffect->setVisible(false);

    continuousEffectValue_label = new QLabel("Valore");
    continuousEffectValue_input = new QLineEdit();
    continuousEffectValue_input->setValidator(new QIntValidator(0,99));
    continuousEffectValue_operator = new QComboBox();
    continuousEffectValue_operator->addItem("Minore di");
    continuousEffectValue_operator->addItem("Uguale a ");
    continuousEffectValue_operator->addItem("Maggiore di");
    continuousEffectValue_operator->setCurrentIndex(1);


    effect_layout->addWidget(allObjectivesOfContinuousEffect);
    effect_layout->addWidget(objectiveOfContinuousEffect);
    effect_layout->addWidget(allTypeOfContinuousEffect);
    effect_layout->addWidget(typeOfContinuousEffect);
    effect_layout->addWidget(continuousEffectValue_label);
    effect_layout->addWidget(continuousEffectValue_operator);
    effect_layout->addWidget(continuousEffectValue_input);

    magic_layout->addLayout(effect_layout);
}

bool SearchFilterWidget::findCreature(bool searchResult, Creature *creature) const {
    if (!allCreature_checkBox->checkState()){
        if (!cardCreatureLife_input->text().isEmpty()){
            switch (cardCreatureLife_operator->currentIndex()){
                case 0:{ //vita Minore di
                    searchResult = searchResult && (creature->getLife() < cardCreatureLife_input->text().toInt());
                    break;
                }
                case 1:{ //vita Uguale a
                    searchResult = searchResult && (creature->getLife() == cardCreatureLife_input->text().toInt());
                    break;
                }
                case 2:{ //vita maggiore di
                    searchResult = searchResult && (creature->getLife() > cardCreatureLife_input->text().toInt());
                    break;
                }
            }
        }

        if (!cardCreatureAtk_input->text().isEmpty()){
            switch (cardCreatureAtk_operator->currentIndex()){
                case 0:{ //atk Minore di
                    searchResult = searchResult && (creature->getAtk() < cardCreatureAtk_input->text().toInt());
                    break;
                }
                case 1:{ //atk Uguale a
                    searchResult = searchResult && (creature->getAtk() == cardCreatureAtk_input->text().toInt());
                    break;
                }
                case 2:{ //atk maggiore di
                    searchResult = searchResult && (creature->getAtk() > cardCreatureAtk_input->text().toInt());
                    break;
                }
            }
        }

        if (!cardCreatureDef_input->text().isEmpty()){
            switch (cardCreatureAtk_operator->currentIndex()){
                case 0:{ //def Minore di
                    searchResult = searchResult && (creature->getDef() < cardCreatureDef_input->text().toInt());
                    break;
                }
                case 1:{ //defUguale a
                    searchResult = searchResult && (creature->getDef() == cardCreatureDef_input->text().toInt());
                    break;
                }
                case 2:{ //def maggiore di
                    searchResult = searchResult && (creature->getDef() > cardCreatureDef_input->text().toInt());
                    break;
                }
            }
        }
    }
    return searchResult;
}

bool SearchFilterWidget::findManaGenerator(bool searchResult, ManaGenerator *manaGenerator) const {
    if (!allManaGenerator_checkBox->checkState()){
        if (!cardManaGeneratorIncrementValue_input->text().isEmpty()){
            switch (cardManaGeneratorIncrementValue_opearator->currentIndex()){
                case 0:{
                    searchResult = searchResult && (manaGenerator->getIncrementValue() < static_cast<unsigned int>(cardManaGeneratorIncrementValue_input->text().toInt()));
                    break;
                }
                case 1:{
                    searchResult = searchResult && (manaGenerator->getIncrementValue() == static_cast<unsigned int>(cardManaGeneratorIncrementValue_input->text().toInt()));
                    break;
                }
                case 2:{
                    searchResult = searchResult && (manaGenerator->getIncrementValue() > static_cast<unsigned int>(cardManaGeneratorIncrementValue_input->text().toInt()));
                    break;
                }
            }
        }
    }
    return searchResult;
}

bool SearchFilterWidget::findMagic(bool searchResult, Magic *magic) const{
    if (!allTypeOfMagic_checkBox->checkState()){
        switch (typeOfMagic->currentIndex()) {
            case 0:{ //Damage
                DamageEffect *damageEffect = dynamic_cast<DamageEffect *>(magic->getEffect());
                searchResult = searchResult && (damageEffect != nullptr);
                if (!damageEffectValue_input->text().isEmpty()){
                    switch (damageEffectValue_operator->currentIndex()){
                        case 0:{
                            searchResult = searchResult && (damageEffect->getDamage() < damageEffectValue_input->text().toInt());
                            break;
                        }
                        case 1:{
                            searchResult = searchResult && (damageEffect->getDamage() == damageEffectValue_input->text().toInt());
                            break;
                        }
                        case 2:{
                            searchResult = searchResult && (damageEffect->getDamage() > damageEffectValue_input->text().toInt());
                            break;
                        }
                    }
                }
                break;
            }
            case 1:{ //Continuous
                ContinuousEffect* continuousEffect = dynamic_cast<ContinuousEffect*>(magic->getEffect());
                searchResult = searchResult && (continuousEffect != nullptr);
                if (!allObjectivesOfContinuousEffect->checkState()){
                    switch (objectiveOfContinuousEffect->currentIndex()) {
                        case 0:{ //Attacco
                            searchResult = searchResult && (continuousEffect->getTypeModified() == atk);
                            break;
                        }
                        case 1:{ //Difesa
                            searchResult = searchResult && (continuousEffect->getTypeModified() == def);
                            break;
                        }
                    }
                }

                if (!allTypeOfContinuousEffect->checkState()){
                    switch (typeOfContinuousEffect->currentIndex()){
                        case 0:{ //Incremento
                            searchResult = searchResult && (continuousEffect->getValue() >= 0);
                            break;
                        }
                        case 1:{ //Decremento
                            searchResult = searchResult && (continuousEffect->getValue() < 0);
                            break;
                        }
                    }
                }

                if (!continuousEffectValue_input->text().isEmpty()){
                    switch (continuousEffectValue_operator->currentIndex()){
                        case 0:{
                            searchResult = searchResult && (continuousEffect->getValue() < continuousEffectValue_input->text().toInt());
                            break;
                        }
                        case 1:{
                            searchResult = searchResult && (continuousEffect->getValue() == continuousEffectValue_input->text().toInt());
                            break;
                        }
                        case 2:{
                            searchResult = searchResult && (continuousEffect->getValue() > continuousEffectValue_input->text().toInt());
                            break;
                        }
                    }
                }
                break;
            }
        }
    }
    return searchResult;
}

void SearchFilterWidget::handleCreatureCheckBox(int state) {
    if (specialCreature_checkBox->checkState())
        return;

    if (state == 0){ //Il selettore è stato spento
        QUtils::clearLayout(creature_layout);
        delete creature_layout;
        creature_layout = nullptr;
    }
    else{ //Il selettore è stato acceso
        buildCreatureSearchLayout();
        main_layout->addLayout(creature_layout);
    }
}

void SearchFilterWidget::handleManaGeneratorCheckBox(int state) {
    if (specialManaGenerator_checkBox->checkState()) //Se specialManagenerator è gia selezionato non serve creare il layout
        return;

    if (state == 0){ //Il selettore è stato spento
        QUtils::clearLayout(manaGenerator_layout);
        delete manaGenerator_layout;
        manaGenerator_layout = nullptr;
    }
    else{ //Il selettore è stato acceso
        buildManaGeneratorSearchLayout();
        main_layout->addLayout(manaGenerator_layout);
    }

}

void SearchFilterWidget::handleMagicCheckBox(int state) {
    if (specialCreature_checkBox->checkState() || specialManaGenerator_checkBox->checkState())
        return;

    if (state == 0){ //Il selettore è stato spento
        QUtils::clearLayout(magic_layout);
        delete main_layout;
        magic_layout = nullptr;
    }
    else{ //Il selettore è stato acceso
        buildMagicSearchLayout();
        main_layout->addLayout(magic_layout);
    }

}

void SearchFilterWidget::handleSpecialCreatureCheckBox(int state) {
    if (creature_checkBox->checkState() || magic_checkBox->checkState()){
        if (creature_checkBox->checkState() && !magic_checkBox->checkState()){ //Solo creature è selezionato
            if (state == 0){
                //Disabilito solo magic
                QUtils::clearLayout(magic_layout);
                delete magic_layout;
                magic_layout = nullptr;
            }
            else {
                //aggiungo solo magic
                buildMagicSearchLayout();
                main_layout->insertLayout(6, magic_layout);
            }
        }
        else if(!creature_checkBox->checkState() && magic_checkBox->checkState()){ //Solo magia è selezionato
            if (state == 0){
                QUtils::clearLayout(creature_layout);
                delete creature_layout;
                creature_layout = nullptr;
            }
            else {
                buildCreatureSearchLayout();
                main_layout->addLayout(creature_layout);
            }
        }
        else if(creature_checkBox->checkState() && magic_checkBox->checkState()) //Entrambe sono già state create
            return;
    }
    else{
        //Non è selezionato nulla
        if (state == 0){
            QUtils::clearLayout(creature_layout);
            delete creature_layout;
            creature_layout = nullptr;
            QUtils::clearLayout(magic_layout);
            delete magic_layout;
            magic_layout = nullptr;
        }
        else {
            buildCreatureSearchLayout();
            main_layout->addLayout(creature_layout);
            buildMagicSearchLayout();
            main_layout->addLayout(magic_layout);
        }
    }

}

void SearchFilterWidget::handleSpecilaManaGeneratorCheckBox(int state) {
    if (manaGenerator_checkBox->checkState() || magic_checkBox->checkState()){
        if (manaGenerator_checkBox->checkState() && !magic_checkBox->checkState()){ //Solo manaGenerator è selezionato
            if (state == 0){
                //Disabilito solo magic
                QUtils::clearLayout(magic_layout);
                delete magic_layout;
                magic_layout = nullptr;
            }
            else {
                //aggiungo solo magic
                buildMagicSearchLayout();
                main_layout->insertLayout(6, magic_layout);
            }
        }
        else if(!manaGenerator_checkBox->checkState() && magic_checkBox->checkState()){ //Solo magia è selezionato
            if (state == 0){
                QUtils::clearLayout(manaGenerator_layout);
                delete manaGenerator_layout;
                manaGenerator_layout = nullptr;
            }
            else {
                buildManaGeneratorSearchLayout();
                main_layout->addLayout(manaGenerator_layout);
            }
        }
        else if(manaGenerator_checkBox->checkState() && magic_checkBox->checkState()) //Entrambe sono già state create
            return;
    }
    else{
        //Non è selezionato nulla
        if (state == 0){
            QUtils::clearLayout(manaGenerator_layout);
            delete manaGenerator_layout;
            manaGenerator_layout = nullptr;
            QUtils::clearLayout(magic_layout);
            delete magic_layout;
            magic_layout = nullptr;
        }
        else {
            buildManaGeneratorSearchLayout();
            main_layout->addLayout(manaGenerator_layout);
            buildMagicSearchLayout();
            main_layout->addLayout(magic_layout);
        }
    }
}

void SearchFilterWidget::handleAllTypeOfMagicCheckBox(int state) {
    QUtils::clearLayout(effect_layout);
    delete effect_layout;
    effect_layout = nullptr;
    if (state == 0){
        typeOfMagic->setCurrentIndex(0);
        typeOfMagic->setVisible(true);
        buildDamageEffectLayout();
    }
    else{
        typeOfMagic->setVisible(false);
    }
}

void SearchFilterWidget::handleTypeOfMagicChanged(int index) {
    QUtils::clearLayout(effect_layout);
    delete effect_layout;
    effect_layout = nullptr;
    switch (index) {
        case 0:{ //Damage
            buildDamageEffectLayout();
            break;
        }
        case 1:{ //Continuous
            buildContinuousEffectLayout();
            break;
        }
    }
}

void SearchFilterWidget::handleObjectiveOfEffectCheckBox(int state) {
    if (state == 0){
        objectiveOfContinuousEffect->setVisible(true);
    }
    else {
        objectiveOfContinuousEffect->setVisible(false);
    }
}

void SearchFilterWidget::handleTypeOfEffectCheckBox(int state) {
    if (state == 0){
        typeOfContinuousEffect->setVisible(true);
    }
    else {
        typeOfContinuousEffect->setVisible(false);
    }
}

void SearchFilterWidget::handleAllCreatureCheckBox(int state) {
    if (state == 0){
        cardCreatureLife_label->setVisible(true);
        cardCreatureLife_input->setVisible(true);
        cardCreatureLife_operator->setVisible(true);
        cardCreatureAtk_label->setVisible(true);
        cardCreatureAtk_input->setVisible(true);
        cardCreatureAtk_operator->setVisible(true);
        cardCreatureDef_label->setVisible(true);
        cardCreatureDef_input->setVisible(true);
        cardCreatureDef_operator->setVisible(true);
    }
    else {
        cardCreatureLife_label->setVisible(false);
        cardCreatureLife_input->setVisible(false);
        cardCreatureLife_operator->setVisible(false);
        cardCreatureAtk_label->setVisible(false);
        cardCreatureAtk_input->setVisible(false);
        cardCreatureAtk_operator->setVisible(false);
        cardCreatureDef_label->setVisible(false);
        cardCreatureDef_input->setVisible(false);
        cardCreatureDef_operator->setVisible(false);
    }
}

void SearchFilterWidget::handleAllManaGeneratorCheckBox(int state) {
    if (state == 0){
        cardManaGeneratorIncrementValue_label->setVisible(true);
        cardManaGeneratorIncrementValue_input->setVisible(true);
        cardManaGeneratorIncrementValue_opearator->setVisible(true);
    }
    else {
        cardManaGeneratorIncrementValue_label->setVisible(false);
        cardManaGeneratorIncrementValue_input->setVisible(false);
        cardManaGeneratorIncrementValue_opearator->setVisible(false);
    }
}

void SearchFilterWidget::handleSearchButton() {
    findedCards = searchDeck.find([this](DeepPtr<Card> card){
        bool searchResult;
        searchResult = !cardName_input->text().isEmpty() && QString(card->getTitle().c_str()).contains(cardName_input->text(), Qt::CaseInsensitive);
        searchResult = searchResult || (!cardDescription_input->text().isEmpty() && QString(card->getDescription().c_str()).contains(cardDescription_input->text(), Qt::CaseInsensitive));
        searchResult = searchResult || (!cardManaCost_input->text().isEmpty() && card->getManaCost() == static_cast<unsigned int>(cardManaCost_input->text().toInt()));

        if (creature_checkBox->checkState()){
            Creature* creature = dynamic_cast<Creature*>(card.operator->());
            searchResult = searchResult || (creature != nullptr);
            searchResult = searchResult && findCreature(searchResult, creature);
        }

        if (manaGenerator_checkBox->checkState()){
            ManaGenerator* manaGenerator = dynamic_cast<ManaGenerator*>(card.operator->());
            searchResult = searchResult || (manaGenerator != nullptr);
            searchResult = searchResult && findManaGenerator(searchResult, manaGenerator);
        }

        if (magic_checkBox->checkState()){
            Magic* magic = dynamic_cast<Magic*>(card.operator->());
            searchResult = searchResult || (magic != nullptr);
            searchResult = searchResult && findMagic(searchResult, magic);
        }

        if (specialCreature_checkBox->checkState()){
            SpecialCreature* specialCreature = dynamic_cast<SpecialCreature*>(card.operator->());
            searchResult = searchResult || (specialCreature != nullptr);
            searchResult = searchResult && findCreature(searchResult, specialCreature) && findMagic(searchResult, specialCreature);
        }

        if (specialManaGenerator_checkBox->checkState()){
            SpecialManaGenerator* specialManaGenerator = dynamic_cast<SpecialManaGenerator*>(card.operator->());
            searchResult = searchResult || (specialManaGenerator != nullptr);
            searchResult = searchResult && findManaGenerator(searchResult, specialManaGenerator) && findMagic(searchResult, specialManaGenerator);
        }

        return searchResult;
    });

    emit clickedSearchButton(findedCards);
    close();
}
