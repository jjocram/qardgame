//
// Created by Marco Ferrati on 2019-06-22.
//

#ifndef QARDGAME_MODIFYDECKSWIDGET_H
#define QARDGAME_MODIFYDECKSWIDGET_H

#include <QWidget>

#include <QListWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QKeyEvent>
#include <QLabel>
#include <QListWidgetItem>

#include "../../Model/Deck.h"
#include "../../Container/Container.h"

class ModifyDecksWidget: public QWidget {
    Q_OBJECT
private:
    QVBoxLayout *main_layout;

    QLabel *title_label;
    QListWidget *decks_list;
    QLineEdit *searchBar;
    QPushButton *resetResearch_button;
    bool inSearchMode;

    QPushButton *newDeck_button;
    QPushButton *importDeck_button;
    QPushButton *removeDeck_button;
    QHBoxLayout *optionsList_layout;

    Container<Deck> decks;
    Container<Deck> searchedDecks;

    void keyReleaseEvent(QKeyEvent *event);
public:
    explicit ModifyDecksWidget(QWidget *parent = nullptr);

    int numberOdDecks() const;
    Deck getDeckAt(int i) const;
    int getCurrentRow() const;
    void setCurrentSelectedRowText(const QString& s);
signals:
    void deletedDeck();
    void changedSelectedDeck(int);
public slots:
    void handleCreationOfNewDeckButton();
    void handleDeckImport();
    void handleDeleteOfDeckButton();
    void handleResetReasearchButton();
    void handleInsertNewDeckAfterCreation(const Deck &newDeck);
    void handleDeckSelection(QListWidgetItem *item);
};


#endif //QARDGAME_MODIFYDECKSWIDGET_H
