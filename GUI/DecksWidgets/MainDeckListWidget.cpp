//
// Created by Marco Ferrati on 2019-05-09.
//

#include "MainDeckListWidget.h"

#include "../CardsWidgets/CreateNewCardWidget.h"
#include "SearchFilterWidget.h"

#include "ProjectException/ProjException.h"
#include "../QUtils.h"

#include <iostream>
using namespace std;

MainDeckListWidget::MainDeckListWidget(QWidget *parent): QWidget(parent) {
    main_layout = new QVBoxLayout(this);
    mainDeck_list = new QListWidget();
    connect(mainDeck_list, SIGNAL(currentItemChanged(QListWidgetItem * , QListWidgetItem *)), this,
            SLOT(handleSwitchItem(QListWidgetItem * , QListWidgetItem *)));

    try {
        mainDeck = Deck::load("decks/maindeck.xml");
        for (unsigned int i = 0; i < mainDeck.getNumberOfCards(); ++i) {
            mainDeck_list->addItem(mainDeck.getCard(i)->getTitle().c_str());
        }
    } catch (ProjException e){
        QUtils::showAlert("Impossibile caricare il maindeck", e.what());
    }

    mainDeck_list->setMinimumWidth(300);

    add_button = new QPushButton("+");
    connect(add_button, SIGNAL(clicked()), this, SLOT(handleAddButton()));

    remove_button = new QPushButton("-");
    connect(remove_button, SIGNAL(clicked()), this, SLOT(handleRemoveButton()));

    inSearchMode = false;
    resetReasearch_button = new QPushButton("Resetta ricerca");
    resetReasearch_button->setVisible(false);
    connect(resetReasearch_button, SIGNAL(clicked()), this, SLOT(handleReserchResearchButton()));

    listOptions_layout = new QHBoxLayout();
    listOptions_layout->addWidget(add_button, 0, Qt::AlignLeft);
    listOptions_layout->addWidget(remove_button, 0, Qt::AlignLeft);
    listOptions_layout->addWidget(resetReasearch_button, 0, Qt::AlignLeft);
    listOptions_layout->insertStretch(listOptions_layout->count() + 1, 20);

    searchBar_input = new QLineEdit();
    showFilters_button = new QPushButton("@");
    connect(showFilters_button, SIGNAL(clicked()), this, SLOT(handleShowFiltersButton()));

    search_layout = new QHBoxLayout();
    search_layout->addWidget(searchBar_input);
    search_layout->addWidget(showFilters_button);

    list_layout = new QVBoxLayout();
    list_layout->addLayout(search_layout);
    list_layout->addWidget(mainDeck_list);
    list_layout->addLayout(listOptions_layout);

    main_layout->addLayout(list_layout);
}

Deck& MainDeckListWidget::getSelectedDeck(){
    return inSearchMode ? searchedDeck : mainDeck;
}

void MainDeckListWidget::keyReleaseEvent(QKeyEvent *event) {
    if (event->key() == 16777220){
        if (!searchBar_input->text().isEmpty()){
            searchedDeck = mainDeck.find([this](DeepPtr<Card> card){
                return QString(card->getTitle().c_str()).contains(searchBar_input->text(), Qt::CaseInsensitive);
            });
            inSearchMode = true;
            mainDeck_list->blockSignals(true);
            mainDeck_list->clear();
            mainDeck_list->blockSignals(false);

            for (unsigned int i = 0; i < searchedDeck.getNumberOfCards(); ++i)
                mainDeck_list->addItem(searchedDeck.getCard(i)->getTitle().c_str());

            resetReasearch_button->setVisible(true);
        }
        else{
            inSearchMode = false;
            mainDeck_list->blockSignals(true);
            mainDeck_list->clear();
            mainDeck_list->blockSignals(false);

            for (unsigned int i = 0; i < mainDeck.getNumberOfCards(); ++i)
                mainDeck_list->addItem(mainDeck.getCard(i)->getTitle().c_str());

            resetReasearch_button->setVisible(false);
        }
    }
}

int MainDeckListWidget::selectedItemRow() const {
    return mainDeck_list->currentRow();
}

void MainDeckListWidget::handleSwitchItem(QListWidgetItem *currentItem, QListWidgetItem *) {
    unsigned int index = static_cast<unsigned int>(currentItem->listWidget()->currentRow());
    emit(changedCard(inSearchMode ? searchedDeck.getCard(index) : mainDeck.getCard(index)));
}

void MainDeckListWidget::handleAddButton() {
    CreateNewCardWidget* createNewCardWidget = new CreateNewCardWidget();
    createNewCardWidget->setWindowModality(Qt::WindowModal);
    connect(createNewCardWidget, SIGNAL(createdNewCard()), this, SLOT(handleMainDeckChanged()));
    createNewCardWidget->show();
}

void MainDeckListWidget::handleRemoveButton() {
    if (mainDeck_list->count() <= 0)
        return;

    unsigned int selectedIndex = static_cast<unsigned int>(mainDeck_list->currentRow());

    QString cardTitle;
    if (inSearchMode)
        cardTitle = searchedDeck.getCard(selectedIndex)->getTitle().c_str();
    else
        cardTitle = mainDeck.getCard(selectedIndex)->getTitle().c_str();

    QMessageBox* alertBox = new QMessageBox();
    alertBox->setText("Sei sicuro di voler eliminare " + cardTitle);
    alertBox->setInformativeText("Una volta eliminata non sarà più possibile recuperarla");
    alertBox->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    alertBox->setDefaultButton(QMessageBox::No);
    alertBox->setMinimumWidth(500);
    if (alertBox->exec() == QMessageBox::Yes){
        if (inSearchMode){
            mainDeck.removeCard(searchedDeck.getCard(selectedIndex));
            searchedDeck.removeCard(selectedIndex);
        }
        else
            mainDeck.removeCard(selectedIndex);

        mainDeck_list->blockSignals(true);
        delete mainDeck_list->takeItem(selectedIndex);
        mainDeck_list->blockSignals(false);

        emit(deletedCard());

        try{
            mainDeck.save("decks/maindeck.xml");
        }
        catch (ProjException e){
            QUtils::showAlert("Impossibile salvare le modifiche", e.what())->exec();
        }
    }
    delete alertBox;
}

void MainDeckListWidget::handleShowFiltersButton() {
    SearchFilterWidget *searchFilterWidget = new SearchFilterWidget(mainDeck);
    searchFilterWidget->setWindowModality(Qt::WindowModal);
    connect(searchFilterWidget, SIGNAL(clickedSearchButton(
                                               const Deck&)), this, SLOT(handleSearchFilters(
                                                                                 const Deck&)));
    searchFilterWidget->show();
}

void MainDeckListWidget::handleSearchFilters(const Deck &deck) {
    searchedDeck = deck;
    inSearchMode = true;

    mainDeck_list->blockSignals(true);
    mainDeck_list->clear();
    mainDeck_list->blockSignals(false);

    for (unsigned int i = 0; i < searchedDeck.getNumberOfCards(); ++i)
        mainDeck_list->addItem(searchedDeck.getCard(i)->getTitle().c_str());

    resetReasearch_button->setVisible(true);
}

void MainDeckListWidget::handleReserchResearchButton() {
    inSearchMode = false;

    mainDeck_list->blockSignals(true);
    mainDeck_list->clear();
    mainDeck_list->blockSignals(false);

    for (unsigned int i = 0; i < mainDeck.getNumberOfCards(); ++i)
        mainDeck_list->addItem(mainDeck.getCard(i)->getTitle().c_str());

    resetReasearch_button->setVisible(false);
}

void MainDeckListWidget::handleMainDeckChanged() {
    try {
        mainDeck = Deck::load("decks/maindeck.xml");
        mainDeck_list->addItem(mainDeck.getCard(mainDeck.getNumberOfCards() - 1)->getTitle().c_str());
    }
    catch (ProjException e){
        QUtils::showAlert("Impossibile caricare il maindeck", e.what())->exec();
    }

}

void MainDeckListWidget::handleCardChanged() {
    try {
        mainDeck = Deck::load("decks/maindeck.xml");
    }
    catch (ProjException e){
        QUtils::showAlert("Impossibile caricare il maindeck", e.what())->exec();
    }
}
