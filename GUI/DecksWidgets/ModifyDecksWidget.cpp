//
// Created by Marco Ferrati on 2019-06-22.
//

#include "ModifyDecksWidget.h"

#include <QDir>
#include <QMessageBox>
#include "../QUtils.h"
#include <QFileDialog>

#include "DeckCreatorWidget.h"

ModifyDecksWidget::ModifyDecksWidget(QWidget *parent): QWidget(parent) {
    setWindowTitle("Modifica un deck");

    main_layout = new QVBoxLayout(this);

    title_label = new QLabel("<font size=12>Seleziona un deck</font>");

    QDir decksDir("decks");
    QStringList decksFileName = decksDir.entryList(QStringList() << "*.xml", QDir::Files);
    foreach(QString fileName, decksFileName){
        try{
            if (fileName != "maindeck.xml")
                decks.append(Deck::load("decks/"+fileName.toStdString()));
        } catch (ProjException e){
            std::cerr << "[Warning]: Errore nell'apertura di un deck, deck skippato" << std::endl << "\t" << e.what() << std::endl;
        }
    }

    decks_list = new QListWidget();
    for (auto cit = decks.const_begin(); cit != decks.const_end(); ++cit)
        decks_list->addItem((*cit).getName().c_str());

    connect(decks_list, SIGNAL(currentRowChanged(int)), this, SIGNAL(changedSelectedDeck(int)));
    connect(decks_list, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(handleDeckSelection(QListWidgetItem *)));

    searchBar = new QLineEdit();
    inSearchMode = false;

    newDeck_button = new QPushButton("+");
    connect(newDeck_button, SIGNAL(clicked()), this, SLOT(handleCreationOfNewDeckButton()));

    importDeck_button =new QPushButton("Importa da file");
    connect(importDeck_button, SIGNAL(clicked()), this, SLOT(handleDeckImport()));

    removeDeck_button = new QPushButton("-");
    connect(removeDeck_button, SIGNAL(clicked()), this, SLOT(handleDeleteOfDeckButton()));

    resetResearch_button = new QPushButton("Annulla ricerca");
    resetResearch_button->setVisible(false);
    connect(resetResearch_button, SIGNAL(clicked()), this, SLOT(handleResetReasearchButton()));

    optionsList_layout = new QHBoxLayout();
    optionsList_layout->addWidget(newDeck_button, 0, Qt::AlignLeft);
    optionsList_layout->addWidget(importDeck_button, 0, Qt::AlignLeft);
    optionsList_layout->addWidget(removeDeck_button, 0, Qt::AlignLeft);
    optionsList_layout->addWidget(resetResearch_button, 0, Qt::AlignLeft);
    optionsList_layout->insertStretch(optionsList_layout->count()+1, 20);

    main_layout->addWidget(title_label);
    main_layout->addWidget(searchBar);
    main_layout->addWidget(decks_list);
    main_layout->addLayout(optionsList_layout);
}

void ModifyDecksWidget::keyReleaseEvent(QKeyEvent *event) {
    if (event->key() == 1666777220){
        if (!searchBar->text().isEmpty()){ //in caso la search field non sia vuota
            searchedDecks = decks.filter([this] (Deck deck){
               return deck.getName().find(searchBar->text().toStdString()) != std::string::npos;
            });
            inSearchMode = true;
            decks_list->blockSignals(true);
            decks_list->clear();
            decks_list->blockSignals(false);

            for (auto cit = searchedDecks.const_begin(); cit != searchedDecks.const_end(); ++cit)
                decks_list->addItem((*cit).getName().c_str());
            resetResearch_button->setVisible(true);
        }
        else{ //In caso di searcBar vuota annullo la ricerca
            handleResetReasearchButton();
        }
    }
}

int ModifyDecksWidget::numberOdDecks() const {
    return decks_list->count();
}

Deck ModifyDecksWidget::getDeckAt(int i) const {
    return inSearchMode ? searchedDecks[static_cast<unsigned int>(i)] : decks[static_cast<unsigned int>(i)];
}

int ModifyDecksWidget::getCurrentRow() const {
    return decks_list->currentRow();
}

void ModifyDecksWidget::setCurrentSelectedRowText(const QString &newDeckName) {
    decks_list->currentItem()->setText(newDeckName);
}

void ModifyDecksWidget::handleCreationOfNewDeckButton() {
    DeckCreatorWidget* newDeckWidget = new DeckCreatorWidget();
    newDeckWidget->setWindowModality(Qt::WindowModal);
    newDeckWidget->show();
    connect(newDeckWidget, SIGNAL(newDeckCreated(const Deck&)), this, SLOT(handleInsertNewDeckAfterCreation(const Deck&)));
}

void ModifyDecksWidget::handleDeleteOfDeckButton() {
    if (decks_list->count() <= 0)
        return;

    QMessageBox *alert_messageBox = new QMessageBox;
    unsigned int index = decks_list->currentRow();
    QString deckName = inSearchMode ? searchedDecks[index].getName().c_str() : decks[index].getName().c_str();

    alert_messageBox->setText("Sei sicuro di voler eliminare "+ deckName);
    alert_messageBox->setInformativeText("Una volta eliminato non sarà più possibile recuperarlo");
    alert_messageBox->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    alert_messageBox->setDefaultButton(QMessageBox::Yes);
    alert_messageBox->setMinimumWidth(500);

    if (alert_messageBox->exec() == QMessageBox::Yes){
        if (inSearchMode){
            decks.remove(searchedDecks[index]);
            searchedDecks.removeAt(index);
        }
        else
            decks.removeAt(index);

        std::string deckFileName = decks[index].getName() + ".xml";
        if (QDir("decks").remove(QString::fromStdString(deckFileName).replace(" ", ""))){
            decks_list->blockSignals(true);
            delete decks_list->takeItem(index);
            decks_list->blockSignals(false);
            emit(deletedDeck());
        }
        else {
            QUtils::showAlert("Impossibile eliminare il deck")->exec();
        }
    }
}

void ModifyDecksWidget::handleResetReasearchButton() {
    inSearchMode = false;
    decks_list->blockSignals(true);
    decks_list->clear();
    decks_list->blockSignals(false);
    for (auto cit = decks.const_begin(); cit != decks.const_end(); ++cit)
        decks_list->addItem((*cit).getName().c_str());
    resetResearch_button->setVisible(false);
}

void ModifyDecksWidget::handleInsertNewDeckAfterCreation(const Deck &newDeck) {
    decks.append(newDeck);
    decks_list->addItem(newDeck.getName().c_str());
}

void ModifyDecksWidget::handleDeckSelection(QListWidgetItem *) {
    Deck selectedDeck = inSearchMode ? searchedDecks[decks_list->currentRow()] : decks[decks_list->currentRow()];
    DeckCreatorWidget *modifyWidget = new DeckCreatorWidget(selectedDeck);
    modifyWidget->setWindowModality(Qt::WindowModal);
    modifyWidget->show();
    close();
}

void ModifyDecksWidget::handleDeckImport() {
    bool overwrite = false;
    QString fileName = QFileDialog::getOpenFileName(this, "Seleziona il deck da importate", QString(), "*.xml");
    if (fileName.isEmpty())
        return;

    try {
        Deck importedDeck = Deck::load(fileName.toStdString());

        if (importedDeck.getName().empty()){
            QUtils::showAlert("Errore durante l'importazione", "Deck senza nome o in un formato sconosciuto")->exec();
            return;
        }

        if (QFile::exists("decks/"+QString::fromStdString(importedDeck.getName()).replace(" ", "")+".xml")) {
            QMessageBox *alert_messageBox = new QMessageBox;

            alert_messageBox->setText("Deck già esistente");
            alert_messageBox->setInformativeText("Vuoi sovrascriver il deck?");
            alert_messageBox->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            alert_messageBox->setDefaultButton(QMessageBox::Yes);
            alert_messageBox->setMinimumWidth(500);
            if (alert_messageBox->exec() == QMessageBox::Yes) {
                overwrite = true;
                QFile::remove("decks" + QString::fromStdString(importedDeck.getName()).replace(" ", "") + ".xml");
            }
            else
                return;
        }

        if (!QFile::copy(fileName, "decks/"+QString::fromStdString(importedDeck.getName()).replace(" ","")+".xml"))
            QUtils::showAlert("Errore durante la copia del file");
        else{
            //Import avvenuto con successo
            if (!overwrite)
                decks_list->addItem(importedDeck.getName().c_str());
            decks.append(importedDeck);
        }
    } catch (ProjException e){
        QUtils::showAlert("Errore durante l'importazione", e.what());
    }

}
