#ifndef DECKCREATORWIDGET_H
#define DECKCREATORWIDGET_H

#include <QWidget>

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include "MainDeckListWidget.h"

#include "../../Model/Deck.h"

class DeckCreatorWidget : public QWidget{
    Q_OBJECT
private:
    QHBoxLayout *main_layout;

    QLineEdit *deckName_input;
    QListWidget *newDeckCards_list;
    MainDeckListWidget *mainDeck_list;

    QPushButton *moveToNewDeck_button;
    QPushButton *removeFromNewDeck_button;
    QVBoxLayout *buttonsForNewDeck_layout;

    QPushButton *createNewDeck_button;
    QVBoxLayout *newDeck_layout;

    Deck newDeck;

    bool failedToCheckData() const;
public:
    explicit DeckCreatorWidget(const Deck& deckToModify = Deck(), QWidget *parent = nullptr);
    QString getNewDeckName() const;
signals:
    void newDeckCreated(const Deck&);
    void deckWasModified(const Deck&);
public slots:
    void handleMoveToNewDeck();
    void handleDoubleClickOnMainDeckList(QListWidgetItem *item);
    void handleRemoveFromNewDeck();
    void handleDoubleClickOnNewDeckList(QListWidgetItem *item);
    void handleCreateNewDeck();
    void handleModifyDeck();
};

#endif // DECKCREATORWIDGET_H
