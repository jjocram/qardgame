//
// Created by Marco Ferrati on 2019-05-01.
//

#ifndef QARDGAME_MAINWIDGET_H
#define QARDGAME_MAINWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QSize>
#include <QVBoxLayout>
#include <QHBoxLayout>


class MainWidget: public QWidget {
    Q_OBJECT
private:
    QPushButton *play_button;
    QPushButton *createCard_button;
    QPushButton *createDeck_button;
    QPushButton *editCards_button;
    QPushButton *editDecks_button;

    QVBoxLayout *main_layout;
    QHBoxLayout *cardButtons_layout;
    QHBoxLayout *deckButtons_layout;
private slots:
    void openPlayWidget();
    void openCreateCardWidget();
    void openEditCardsWidget();
    void openCreateDeckWidget();
    void openEditDecksWidget();
public:
    explicit MainWidget(QWidget* parent=nullptr);
};

#endif //QARDGAME_MAINWIDGET_H
