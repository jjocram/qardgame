//
// Created by Marco Ferrati on 2019-06-23.
//

#include "GameViewWidget.h"

#include <QGraphicsBlurEffect>
#include <QMessageBox>

#include "../../Model/Hierachy/Creature.h"
#include "../../Model/Hierachy/Magic.h"

GameViewWidget::GameViewWidget(Player *P1_init, Player *P2_init, QWidget *parent): QWidget(parent), P1(P1_init), P2(P2_init) {
    setWindowTitle("Partita 1 vs 1");
    main_layout = new QVBoxLayout(this);

    //Costruziona parte relativa P1
    P1_deck_list = new QListWidget();
    P1_deck_list->setFlow(QListView::LeftToRight);
    P1_deck_list->setSpacing(2);
    buildDeckView(P1, P1_deck_list);

    P1_inGameCreature_list = new QListWidget();
    P1_inGameCreature_list->setFlow(QListView::LeftToRight);
    P1_inGameCreature_list->setSpacing(2);

    //Costruziona parte relativa P2
    P2_deck_list = new QListWidget();
    P2_deck_list->setFlow(QListView::LeftToRight);
    P2_deck_list->setSpacing(2);
    buildDeckView(P2, P2_deck_list);

    P2_inGameCreature_list = new QListWidget();
    P2_inGameCreature_list->setFlow(QListView::LeftToRight);
    P2_inGameCreature_list->setSpacing(2);

    //Impostazione della dimensione mininma come la dimensione di 2 carte
    setMinimumSize(300 ,250);

    //Costruzione layout con le informazini di P1
    P1_life_label = new QLabel("Vita: " + QString::fromStdString(std::to_string(P1->getLife())));
    P1_mana_label = new QLabel("Mana: " + QString::fromStdString(std::to_string(P1->getMana())));
    P1_endTurn_button = new QPushButton("Termina turno");
    connect(P1_endTurn_button, SIGNAL(clicked()), this, SLOT(endTurnP1()));

    P1_info_layout = new QHBoxLayout();
    P1_info_layout->addStretch(0);
    P1_info_layout->addWidget(P1_life_label);
    P1_info_layout->addWidget(P1_mana_label);
    P1_info_layout->addWidget(P1_endTurn_button);
    P1_info_layout->addStretch(0);

    //Costruzione layout con le informazini di P1
    P2_life_label = new QLabel("Vita: " + QString::fromStdString(std::to_string(P2->getLife())));
    P2_mana_label = new QLabel("Mana: " + QString::fromStdString(std::to_string(P2->getMana())));
    P2_endTurn_button = new QPushButton("Termina turno");
    connect(P2_endTurn_button, SIGNAL(clicked()), this, SLOT(endTurnP2()));

    P2_info_layout = new QHBoxLayout();
    P2_info_layout->addStretch(0);
    P2_info_layout->addWidget(P2_life_label);
    P2_info_layout->addWidget(P2_mana_label);
    P2_info_layout->addWidget(P2_endTurn_button);
    P2_info_layout->addStretch(0);

    //Disabilitazione del deck del P2
    P1->setIsMyTurn(true);
    P2->setIsMyTurn(false);
    disableWidget(P2_deck_list);

    //Costruzione del main_layout
    main_layout->addLayout(P2_info_layout);
    paintHorizontalLine(main_layout);
    main_layout->addWidget(P2_deck_list);
    paintHorizontalLine(main_layout);
    main_layout->addWidget(P2_inGameCreature_list);
    paintHorizontalLine(main_layout);
    main_layout->addWidget(P1_inGameCreature_list);
    paintHorizontalLine(main_layout);
    main_layout->addWidget(P1_deck_list);
    paintHorizontalLine(main_layout);
    main_layout->addLayout(P1_info_layout);
}

void GameViewWidget::buildDeckView(Player *player, QListWidget *listWidget) {
    //Gradient for special cards
    QLinearGradient gradient(QPointF(100, 100), QPointF(200, 200));
    Deck deck = player->getDeck();
    for (unsigned int i = 0; i < deck.getNumberOfCards(); ++i){
        QListWidgetItem *item =newCardShowerItem();

        //Set background color for cards
        switch (deck.getCard(i)->getType()){
            case Creature_Type:{
                item->setBackgroundColor(QColor(0,128,0));
                break;
            }
            case ManaGenerator_Type:{
                item->setBackgroundColor(QColor(128,0,128));
                break;
            }
            case Magic_Type:{
                item->setBackgroundColor(QColor(0,0,255));
                break;
            }
            case SpecialCreature_Type:{
                gradient.setColorAt(0, QColor(0,128,0));
                gradient.setColorAt(1, QColor(0,0,255));
                item->setBackground(QBrush(gradient));
                break;
            }
            case SpecialManaGenerator_Type:{
                gradient.setColorAt(0, QColor(128,0,128));
                gradient.setColorAt(1, QColor(0,0,255));
                item->setBackground(QBrush(gradient));
                break;
            }
        }

        listWidget->addItem(item);
        CardShowerWidget *cardShowerWidget = new CardShowerWidget(deck.getCard(i)->clone(), static_cast<int>(i), player, listWidget);//ZONA INCRIMINATA
        listWidget->setItemWidget(item, cardShowerWidget);

        connect(cardShowerWidget, SIGNAL(playCardSignal(DeepPtr<Card>&, Player *, int)), this, SLOT(aCardWasPlayed(DeepPtr<Card>&, Player *, int)));
    }
}

void GameViewWidget::paintHorizontalLine(QLayout *layout) {
    QFrame *separatorLine = new QFrame();
    separatorLine->setFrameShape(QFrame::HLine);
    separatorLine->setFrameShadow(QFrame::Sunken);
    layout->addWidget(separatorLine);
}

void GameViewWidget::connectInGameCreaturesForChoice() {
    for(unsigned int i = 0; i<P1_inGameCardShower.count(); ++i){
        connect(P1_inGameCardShower[i], SIGNAL(cardClickedSignal(DeepPtr<Card>&, Player*, int, QListWidget*)), this, SLOT(cardClicked(DeepPtr<Card>&, Player*, int, QListWidget*)));
    }

    for(unsigned int i = 0; i<P2_inGameCardShower.count(); ++i){
        connect(P2_inGameCardShower[i], SIGNAL(cardClickedSignal(DeepPtr<Card>&, Player*, int, QListWidget*)), this, SLOT(cardClicked(DeepPtr<Card>&, Player*, int, QListWidget*)));
    }

}

void GameViewWidget::disconnectInGameCreaturesFromChoice() {
    for(unsigned int i = 0; i<P1_inGameCardShower.count(); ++i){
        disconnect(P1_inGameCardShower[i], SIGNAL(cardClickedSignal(DeepPtr<Card>&, Player*, int, QListWidget*)), this, SLOT(cardClicked(DeepPtr<Card>&, Player*, int, QListWidget*)));
    }

    for(unsigned int i = 0; i<P2_inGameCardShower.count(); ++i){
        disconnect(P2_inGameCardShower[i], SIGNAL(cardClickedSignal(DeepPtr<Card>&, Player*, int, QListWidget*)), this, SLOT(cardClicked(DeepPtr<Card>&, Player*, int, QListWidget*)));
    }

}

void GameViewWidget::disableWidget(QWidget *widget) {
    widget->setGraphicsEffect(new QGraphicsBlurEffect());
    widget->setEnabled(false);
}

void GameViewWidget::enableWidget(QWidget *widget) {
    delete widget->graphicsEffect();
    widget->setEnabled(true);
}

void GameViewWidget::endGame() {
    QString victoryPhrase;
    if (P2->getLife() == 0)
        victoryPhrase = "Congratulazioni " + QString::fromStdString(P1->getName()) + " hai vinto la partita";
    else if (P1->getLife() == 0)
        victoryPhrase = "Congratulazioni " + QString::fromStdString(P2->getName()) + " hai vinto la partita";

    QMessageBox *winning_alert = new QMessageBox();
    winning_alert->setText(victoryPhrase);
    winning_alert->setStandardButtons(QMessageBox::Ok);
    winning_alert->setDefaultButton(QMessageBox::Ok);
    if (winning_alert->exec() == QMessageBox::Ok)
        close();
}

void GameViewWidget::aCardWasPlayed(DeepPtr<Card>&playedCard , Player *whoPlayedCard , int index) {
    whoPlayedCard->setCard(index, getNotPlayerTurn());
    if (whoPlayedCard == P1){
        P1_deck_list->item(index)->setBackground(Qt::gray); //Set color to gray

        //Update info
        P1_mana_label->setText("Mana: " + QString(std::to_string(whoPlayedCard->getMana()).c_str()));
        P1_mana_label->repaint();

        if (playedCard->getType() == Creature_Type || playedCard->getType() == SpecialCreature_Type){
            //Una creature è stata giocata; aggiornare la view
            creatureIsSetted(dynamic_cast<Creature *>(playedCard.operator->()), whoPlayedCard, P1_inGameCreature_list);//Controllare meglio il .operator->
        }

        if (playedCard->getType() == Magic_Type || playedCard->getType() == SpecialCreature_Type || playedCard->getType() == SpecialManaGenerator_Type){
            //Una magia è stata giocata, aggiornare la view per ricevere un input
            connectInGameCreaturesForChoice();
            disableWidget(P1_deck_list);
            disableWidget(P2_deck_list);
            P1->setCardPlayed(Magic_isPlayed);
            P1->setInGameMagic(dynamic_cast<Magic *>(playedCard.operator->()));
        }

        if (playedCard->getType() == ManaGenerator_Type || playedCard->getType() == SpecialManaGenerator_Type){
            //Un generatore di mana è stato giocato, aggiorno la view aggiornando le informazioni
            P1_mana_label->setText("Mana: " + QString(std::to_string(whoPlayedCard->getMana()).c_str()));
            P1_mana_label->repaint();
        }

    }
    else if (whoPlayedCard == P2){
        P2_deck_list->item(index)->setBackground(Qt::gray); //Set color to gray

        //Update info
        P2_mana_label->setText("Mana: " + QString(std::to_string(whoPlayedCard->getMana()).c_str()));
        P2_mana_label->repaint();

        if (playedCard->getType() == Creature_Type || playedCard->getType() == SpecialCreature_Type){
            //Una creature è stata giocata; aggiornare la view
            creatureIsSetted(dynamic_cast<Creature *>(playedCard.operator->()), whoPlayedCard, P2_inGameCreature_list);
        }

        if (playedCard->getType() == Magic_Type || playedCard->getType() == SpecialCreature_Type || playedCard->getType() == SpecialManaGenerator_Type){
            //Una magia è stata giocata, aggiornare la view per ricevere un input
            connectInGameCreaturesForChoice();
            disableWidget(P2_deck_list);
            disableWidget(P1_deck_list);
            P2->setCardPlayed(Magic_isPlayed);
            P2->setInGameMagic(dynamic_cast<Magic *>(playedCard.operator->()));
        }

        if (playedCard->getType() == ManaGenerator_Type || playedCard->getType() == SpecialManaGenerator_Type){
            //Un generatore di mana è stato giocato, aggiorno la view aggiornando le informazioni
            P2_mana_label->setText("Mana: " + QString(std::to_string(whoPlayedCard->getTotalMana()).c_str()));
            P2_mana_label->repaint();
        }
    }
    else{
        std::cerr << "Nessun giocatore ha giocato questa carta" << std::endl;
    }
}

void GameViewWidget::endTurnP1() {
    P1->setIsMyTurn(false);
    P2->setIsMyTurn(true);
    disableWidget(P1_deck_list);
    enableWidget(P2_deck_list);
    P1->endTurn();
    P1_mana_label->setText("Mana: " + QString(std::to_string(P1->getTotalMana()).c_str()));
    P1_mana_label->repaint();
}

void GameViewWidget::endTurnP2() {
    P1->setIsMyTurn(true);
    P2->setIsMyTurn(false);
    disableWidget(P2_deck_list);
    enableWidget(P1_deck_list);
    P2->endTurn();
    P2_mana_label->setText("Mana: " + QString(std::to_string(P2->getTotalMana()).c_str()));
    P2_mana_label->repaint();
}

void GameViewWidget::cardClicked(DeepPtr<Card>&, Player *player, int index, QListWidget *listWidget) {
    CardShowerWidget *selectedCardShower = player == P1 ? P1_inGameCardShower[index] : P2_inGameCardShower[index];
    Player *notPlayer = player == P1 ? P2 : P1;

    switch (getPlayerTurn()->getCardPlayed()){
        case Magic_isPlayed:{
            //è stata giocata una magia;
            switch(getPlayerTurn()->getInGameMagic()->getEffect()->getType()){
                //In baso al tipo di effetto possono cambiare i tipi di bersagli
                case DamageType:{
                    getPlayerTurn()->getInGameMagic()->playMagic(getPlayerTurn(), getNotPlayerTurn(), index, 0);
                    //Check creature life
                    Creature *attackedCreature = player->getInGameCreatureAt(index);
                    if (attackedCreature && attackedCreature->getLife() <= 0){
                        //La creatura è stata uccisa, rimozione della sua tile
                        removeCardShower(player, index);
                    }
                    break;
                }
                case ContinuousType:{
                    int selectedList;
                    if (getPlayerTurn() == P1){
                        if (listWidget == P1_inGameCreature_list)
                            selectedList = 0;
                        else {
                            selectedList = 1;
                        }
                    }
                    else{
                        if (listWidget == P2_inGameCreature_list)
                            selectedList = 0;
                        else
                            selectedList = 1;
                    }
                    getPlayerTurn()->getInGameMagic()->playMagic(getPlayerTurn(), player, index, selectedList);
                    break;
                }
            }
            break;
        }
        case Creature_isPlayed:{
            //Una creatura ha dichiarato un attacco
            AttackSituation fightResult = getPlayerTurn()->getAttackingCreature()->playCard(getPlayerTurn(), getNotPlayerTurn(), index);
            switch (fightResult){
                case victory:{
                    selectedCardShower->updateCardShower();
                    break;
                }
                case victoryAndDeath:{
                    removeCardShower(player, index);
                    break;
                }
                case parity:{
                    //Do nothing
                    break;
                }
                case defeat:{
                    unsigned int i = getPlayerTurn()->indexOfInGameCreature(getPlayerTurn()->getAttackingCreature());
                    if (player == P1) P2_inGameCardShower[i]->updateCardShower();
                    else P1_inGameCardShower[index]->updateCardShower();
                    break;
                }
                case defeatAndDeath:{
                    unsigned int i = getPlayerTurn()->indexOfInGameCreature(getPlayerTurn()->getAttackingCreature());
                    removeCardShower(notPlayer,  i);
                    break;
                }
                default:{
                    std::cerr << "Situazione d'attacco non riconosciuta" << std::endl;
                }
            }
            break;
        }
    }

    //Si riporta la situazione alla normalità
    disconnectInGameCreaturesFromChoice();

    if(getPlayerTurn() == P1) enableWidget(P1_deck_list);
    if(getPlayerTurn() == P2) enableWidget(P2_deck_list);
    if(!P1_inGameCreature_list->isEnabled()) enableWidget(P1_inGameCreature_list);
    if(!P2_inGameCreature_list->isEnabled()) enableWidget(P2_inGameCreature_list);

    selectedCardShower->updateCardShower();
}

void GameViewWidget::creatureIsAttacking(DeepPtr<Card>&card, Player *player, int index) {
    getPlayerTurn()->setCardPlayed(Creature_isPlayed);
    getPlayerTurn()->setAttackingCreature(dynamic_cast<Creature *>(card.operator->()));
    player->creaturePerformedAnAttack(index);
    if (player == P1 && P2_inGameCardShower.isEmpty()){
        //Il giocatore 2 non ha creature con cui difendersi
        getPlayerTurn()->getAttackingCreature()->playCard(P1, P2, -1);
        P2_life_label->setText("Vita: " + QString(std::to_string(P2->getLife()).c_str()));
        P2_life_label->repaint();
        if (P2->getLife() == 0)
            endGame();
        return;
    }

    if (player == P2 && P1_inGameCardShower.isEmpty()){
        //Il giocatore 1 non ha creature con le quali difendersi
        getPlayerTurn()->getAttackingCreature()->playCard(P2, P1, -1);
        P1_life_label->setText("Vita: " + QString(std::to_string(P1->getLife()).c_str()));
        P1_life_label->repaint();
        if (P1->getLife() == 0)
            endGame();
        return;
    }

    connectInGameCreaturesForChoice();
    disableWidget(P1_deck_list);
    disableWidget(P2_deck_list);
    if (player == P1) disableWidget(P1_inGameCreature_list);
    else disableWidget(P2_inGameCreature_list);
}

void GameViewWidget::creatureIsSetted(Creature *creature, Player *player, QListWidget *listWidget) {
    QListWidgetItem *item = newCardShowerItem();
    CardShowerWidget *cardShowerWidget = nullptr;

    if(creature->getType() == Creature_Type){
        item->setBackgroundColor(QColor(0,128,0)); //Set background color
        listWidget->addItem(item);
        cardShowerWidget = new CardShowerWidget(creature->clone(), listWidget->count()-1, player, listWidget, true);
        listWidget->setItemWidget(item, cardShowerWidget);
    }
    else if(creature->getType() == SpecialCreature_Type){
        QLinearGradient grad(QPointF(100,100), QPointF(200,200));
        grad.setColorAt(0, QColor(0,128,0));
        grad.setColorAt(1, QColor(0,0,255));
        item->setBackground(QBrush(grad)); //Set background gradient
        listWidget->addItem(item);
        cardShowerWidget = new CardShowerWidget(creature->clone(), listWidget->count()-1, player, listWidget, true);
        listWidget->setItemWidget(item, cardShowerWidget);
    }
    else {
        std::cerr << "La carta non è una creatura" << std::endl;
        return;
    }

    //Aggiungo la carta alla lista delle carte attualmente in gioco
    player == P1 ? P1_inGameCardShower.append(cardShowerWidget) : P2_inGameCardShower.append(cardShowerWidget);
    connect(cardShowerWidget, SIGNAL(creatureIsAttackingSignal(DeepPtr<Card>&, Player*, int)), this, SLOT(creatureIsAttacking(DeepPtr<Card>&, Player*, int)));
}

QListWidgetItem *GameViewWidget::newCardShowerItem() {
    QListWidgetItem *item = new QListWidgetItem();
    item->setSizeHint(QSize(150, 250));
    item->setFlags(item->flags() & ~Qt::ItemIsSelectable); //Removed the possibility to select the item
    return item;
}

Player *GameViewWidget::getPlayerTurn() {
    if (P1->isMyTurn())
        return P1;
    else if (P2->isMyTurn())
        return P2;
    else
        return nullptr;
}

Player *GameViewWidget::getNotPlayerTurn() {
    if (P1->isMyTurn())
        return P2;
    else if (P2->isMyTurn())
        return P1;
    else
        return nullptr;
}

void GameViewWidget::removeCardShower(Player *p, int index) {
    Container<CardShowerWidget *> &inGameCreatureShowers = p == P1 ? P1_inGameCardShower : P2_inGameCardShower;
    QListWidget *inGameCreature_list = p == P1 ? P1_inGameCreature_list : P2_inGameCreature_list;

    disconnect(inGameCreatureShowers[index], SIGNAL(cardClickedSignal(DeepPtr<Card>&, Player*, int, QListWidget*)), this, SLOT(cardClicked(DeepPtr<Card>&, Player*, int, QListWidget*)));
    inGameCreature_list->removeItemWidget(inGameCreature_list->item(index));
    delete inGameCreature_list->takeItem(index);
    inGameCreatureShowers.removeAt(index);
}
