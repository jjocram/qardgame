//
// Created by Marco Ferrati on 2019-06-23.
//

#ifndef QARDGAME_CARDSHOWERWIDGET_H
#define QARDGAME_CARDSHOWERWIDGET_H

#include <QWidget>

#include <QListWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QHBoxLayout>
#include <QImage>


#include "../../Model/Hierachy/Card.h"
#include "../../Model/Player.h"
#include "../../Container/DeepPtr.h"

class CardShowerWidget: public QWidget{
    Q_OBJECT
private:
    DeepPtr<Card> card;
    Player *owner;

    int index; //Index where is located in listWidget
    bool played;

    QListWidget *list; //ListWidget where this is located

    QVBoxLayout *main_layout;
    QVBoxLayout *card_layout;

    QImage manaCost_image;
    QLabel *name_label;
    QLabel *manaCost_label;
    QHBoxLayout *baseInfo_layout;
    QLabel *description_label;

    QHBoxLayout *icons_layout;

    //Layout creatura
    QImage life_image;
    QLabel *life_label;
    QImage atk_image;
    QLabel *atk_label;
    QImage def_image;
    QLabel *def_label;

    //Layout mana generator
    QImage mana_image;
    QLabel *mana_label;
    QLabel *manaIncrementValue_label;

    //Layout magic
    QImage magicType_image;
    QLabel *magicType_label;
    QLabel *damage_label;
    QImage arrow_image;
    QLabel *arrow_label;
    QImage stat_image;
    QLabel *stat_label;

    //Menu
    QMenu* options_menu;


    void buildImage(QImage &image, const QString &s, int fontSize, const QColor &color);

    void buildCardLayout();
    void paintCreature();
    void paintManaGenerator();
    void paintMagic();
    void paintHorizontalLine();
protected:
    void mousePressEvent(QMouseEvent *);

public:
    explicit CardShowerWidget(Card *card_init,int i, Player *player_init, QListWidget* l, bool played_init = false, QWidget *parent = nullptr);
    void updateCardShower();
signals:
    void playCardSignal(DeepPtr<Card> &Card , Player*, int);
    void cardClickedSignal(DeepPtr<Card> &, Player *, int, QListWidget *);
    void creatureIsAttackingSignal(DeepPtr<Card> &, Player *, int);

public slots:
    void showOptionsMenu(const QPoint &pos);
    void playCardFromDeck();
    void playCardFromInGameCreature();
};


#endif //QARDGAME_CARDSHOWERWIDGET_H
