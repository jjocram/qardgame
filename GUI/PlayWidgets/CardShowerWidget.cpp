//
// Created by Marco Ferrati on 2019-06-23.
//

#include "CardShowerWidget.h"

#include <QPixmap>
#include <QPainter>
#include <QMenu>
#include <QAction>

#include "../QUtils.h"

#include "../../Model/Hierachy/ManaGenerator.h"
#include "../../Model/Hierachy/Magic.h"
#include "../../Model/Hierachy/Effects/DamageEffect.h"
#include "../../Model/Hierachy/Effects/ContinuousEffect.h"

CardShowerWidget::CardShowerWidget(Card *card_init, int i, Player *player_init, QListWidget *l, bool played_init,
                                   QWidget *parent): QWidget(parent), card(card_init), owner(player_init), index(i), played(played_init), list(l){
    //Size della carta
    setFixedSize(150, 250);

    main_layout = new QVBoxLayout(this);

    //Right-click menù
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(showOptionsMenu(const QPoint &)));

    name_label = new QLabel(card->getTitle().c_str());
    name_label->setWordWrap(true);

    manaCost_image = QImage(":/Images/mana.png");
    buildImage(manaCost_image, std::to_string(card->getManaCost()).c_str(), 300, Qt::white);
    manaCost_label = new QLabel("");
    manaCost_label->setPixmap(QPixmap::fromImage(manaCost_image).scaled(30, 30, Qt::KeepAspectRatio));

    baseInfo_layout = new QHBoxLayout();
    baseInfo_layout->addWidget(name_label);
    baseInfo_layout->addWidget(manaCost_label);

    description_label = new QLabel(card->getDescription().c_str());
    description_label->setWordWrap(true);
    description_label->setStyleSheet("font-size: 9px");

    card_layout = new QVBoxLayout();
    buildCardLayout();

    main_layout->addLayout(baseInfo_layout);
    main_layout->addWidget(description_label);
    main_layout->addLayout(card_layout);
}

void CardShowerWidget::buildImage(QImage &image, const QString &s, int fontSize, const QColor &color) {
    image = image.convertToFormat(QImage::Format_ARGB32);
    QPainter p(&image);
    p.setPen(QPen(color));
    p.setFont(QFont("Times", fontSize, QFont::Bold));
    p.drawText(image.rect(), Qt::AlignCenter, s);
}

void CardShowerWidget::buildCardLayout() {
    switch (card->getType()){
        case Creature_Type:{
            paintCreature();
            break;
        }
        case Magic_Type:{
            paintMagic();
            break;
        }
        case ManaGenerator_Type:{
            paintManaGenerator();
            break;
        }
        case SpecialCreature_Type:{
            paintCreature();
            paintMagic();
            break;
        }
        case SpecialManaGenerator_Type:{
            paintManaGenerator();
            paintMagic();
            break;
        }
    }
}

void CardShowerWidget::paintCreature() {
    Creature *creature = dynamic_cast<Creature *>(card.operator->());

    icons_layout = new QHBoxLayout();

    life_image = QImage(":/Images/life.png");
    buildImage(life_image, std::to_string(creature->getLife()).c_str(), 25, Qt::white);
    life_label = new QLabel("");
    life_label->setPixmap(QPixmap::fromImage(life_image).scaled(30, 30, Qt::KeepAspectRatio));

    atk_image = QImage(":/Images/fight.png");
    buildImage(atk_image, std::to_string(creature->getAtk()).c_str(), 150, Qt::black);
    atk_label = new QLabel("");
    atk_label->setPixmap(QPixmap::fromImage(atk_image).scaled(30, 30, Qt::KeepAspectRatio));

    def_image = QImage(":/Images/shield.png");
    buildImage(def_image, std::to_string(creature->getDef()).c_str(), 300, Qt::black);
    def_label = new QLabel("");
    def_label->setPixmap(QPixmap::fromImage(def_image).scaled(30, 30, Qt::KeepAspectRatio));

    icons_layout->addWidget(life_label);
    icons_layout->addWidget(atk_label);
    icons_layout->addWidget(def_label);

    card_layout->addLayout(icons_layout);
}

void CardShowerWidget::paintManaGenerator() {
    ManaGenerator *manaGenerator = dynamic_cast<ManaGenerator *>(card.operator->());

    icons_layout = new QHBoxLayout();

    mana_image = QImage(":/Images/mana.png");
    mana_image = mana_image.convertToFormat(QImage::Format_ARGB32);
    mana_label = new QLabel("");
    mana_label->setPixmap(QPixmap::fromImage(mana_image).scaled(30, 30, Qt::KeepAspectRatio));
    manaIncrementValue_label = new QLabel("+" + QString::fromStdString(std::to_string(manaGenerator->getIncrementValue())));
    manaIncrementValue_label->setFont(QFont("Times", 18, QFont::Bold));

    icons_layout->addStretch(0);
    icons_layout->addWidget(mana_label);
    icons_layout->addWidget(manaIncrementValue_label);
    icons_layout->addStretch(0);

    card_layout->addLayout(icons_layout);
}

void CardShowerWidget::paintMagic() {
    Magic *magic = dynamic_cast<Magic *>(card.operator->());

    icons_layout = new QHBoxLayout();

    if (magic->getEffect()->getType() == DamageType){
        magicType_image = QImage(":/Images/lightning.png");

        life_image = QImage(":/Images/life.png");
        buildImage(life_image, "-"+QString(std::to_string(static_cast<DamageEffect *>(magic->getEffect())->getDamage()).c_str()), 25, Qt::white);
        damage_label = new QLabel("");
        damage_label->setPixmap(QPixmap::fromImage(life_image).scaled(30, 30, Qt::KeepAspectRatio));

        icons_layout->addWidget(damage_label, 0, Qt::AlignCenter);
    }
    else if (magic->getEffect()->getType() == ContinuousType) {
        ContinuousEffect *effect = dynamic_cast<ContinuousEffect *>(magic->getEffect());

        magicType_image = QImage(":/Images/infinite.png");

        if (effect->getValue() >= 0)
            arrow_image = QImage(":/Images/up-arrow.png");
        else
            arrow_image = QImage(":/Images/down-arrow.png");

        if (effect->getTypeModified() == atk) {
            stat_image = QImage(":/Images/fight.png");
            buildImage(stat_image, QString::fromStdString(std::to_string(effect->getValue())), 150, Qt::black);
        }
        else {
            stat_image = QImage(":/Images/shield.png");
            buildImage(stat_image, QString::fromStdString(std::to_string(effect->getValue())), 300, Qt::black);
        }

        arrow_label = new QLabel("");
        arrow_label->setPixmap(QPixmap::fromImage(arrow_image).scaled(30, 30, Qt::KeepAspectRatio));

        stat_label = new QLabel("");
        stat_label->setPixmap(QPixmap::fromImage(stat_image).scaled(30, 30, Qt::KeepAspectRatio));

        icons_layout->addStretch(0);
        icons_layout->addWidget(arrow_label);
        icons_layout->addWidget(stat_label);
        icons_layout->addStretch(0);
    }

    magicType_label = new QLabel();
    magicType_label->setPixmap(QPixmap::fromImage(magicType_image).scaled(30, 30, Qt::KeepAspectRatio));

    card_layout->addWidget(magicType_label, 0, Qt::AlignCenter);
    card_layout->addLayout(icons_layout);
}

void CardShowerWidget::paintHorizontalLine() {
    QFrame* separatorLine = new QFrame();
    separatorLine->setFrameShape(QFrame::HLine);
    separatorLine->setFrameShadow(QFrame::Sunken);

    card_layout->addWidget(separatorLine);

}

void CardShowerWidget::mousePressEvent(QMouseEvent *) {
    emit cardClickedSignal(card, owner, index, list);
}


void CardShowerWidget::showOptionsMenu(const QPoint &pos) {
    options_menu = new QMenu(card->getTitle().c_str(), this);
    QAction* playCard_action = nullptr;

    if (!played){
        playCard_action = new QAction("Gioca", this);
        connect(playCard_action, SIGNAL(triggered()), this, SLOT(playCardFromDeck()));
    }
    else if (played && (card->getType() == Creature_Type || card->getType() == SpecialCreature_Type)){
        playCard_action = new QAction("Attacca", this);
        connect(playCard_action, SIGNAL(triggered()), this, SLOT(playCardFromInGameCreature()));
    }

    options_menu->addAction(playCard_action);
    options_menu->exec(mapToGlobal(pos));
}

void CardShowerWidget::playCardFromDeck() {
    switch (owner->canPlay(card.operator->())) {
        case Playable:{
            played = true;
            emit(playCardSignal(card, owner, index));
            break;
        }
        case NotEnoughMana:{
            QUtils::showAlert("Mana non sufficiente per giocare questa carta")->exec();
            break;
        }
        case YouCanPlayOncePerTurn:{
            QUtils::showAlert("Non puoi giocare più di un generatore di mana per turno")->exec();
            break;
        }
    }
}

void CardShowerWidget::playCardFromInGameCreature() {
    if (owner->hasCreatureAttacked(index)){
        QUtils::showAlert("Una creatura può attaccare solo una volta per turno")->exec();
    }
    else
        emit(creatureIsAttackingSignal(card, owner, index));
}

void CardShowerWidget::updateCardShower() {
    QUtils::clearLayout(card_layout);
    card = DeepPtr<Card>(owner->getInGameCreatureAt(index)->clone());
    buildCardLayout();
}


