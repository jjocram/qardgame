//
// Created by Marco Ferrati on 2019-06-23.
//

#include "StartPlayWidget.h"

#include <QDir>

#include "GameViewWidget.h"

#include "../QUtils.h"

#include "../../Model/Player.h"

StartPlayWidget::StartPlayWidget(QWidget *parent): QWidget(parent) {
    setWindowTitle("Selezione deck");
    main_layout = new QVBoxLayout(this);

    deckChoice_layout = new QHBoxLayout();

    P1_DeckChoice_combobox = new QComboBox();
    connect(P1_DeckChoice_combobox, SIGNAL(currentIndexChanged(int)), this, SLOT(handleChangeInFirstDeck(int)));

    P1_Deck_list = new QListWidget();

    P2_DeckChoice_combobox = new QComboBox();
    connect(P2_DeckChoice_combobox, SIGNAL(currentIndexChanged(int)), this, SLOT(handleChangeInSecondDeck(int)));

    P2_Deck_list = new QListWidget();

    QDir decksDir("decks");
    QStringList decksFile = decksDir.entryList(QStringList() << "*.xml", QDir::Files);
    foreach(QString fileName, decksFile){
        try {
            if (fileName != "maindeck.xml") { //Il main deck non può esserre usato
                Deck deck = Deck::load("decks/" + fileName.toStdString());
                availableDecks.append(deck);
                P1_DeckChoice_combobox->addItem(deck.getName().c_str());
                P2_DeckChoice_combobox->addItem(deck.getName().c_str());
            }
        } catch (ProjException e) {
            std::cerr << "[Warning]: Errore nell'apertura di un deck, deck skippato" << std::endl << "\t" << e.what() << std::endl;
        }
    }

    //Caricamento deck di default;
    if (availableDecks.count() == 0){
        QUtils::showAlert("Nessun deck creato", "Per poter giocare è necessario creare o importare almeno un deck")->exec();
        close();
    }

    P1_DeckChoice_label = new QLabel("<font size=7>Deck giocatore 1</font>");
    P1_info_layout = new QVBoxLayout();
    P1_info_layout->addWidget(P1_DeckChoice_label);
    P1_info_layout->addWidget(P1_DeckChoice_combobox);
    P1_info_layout->addWidget(P1_Deck_list);
    fillList(P1_Deck_list, availableDecks[0]);
    P1_Deck = availableDecks[0];

    P2_DeckChoice_label = new QLabel("<font size=7>Deck giocatore 2</font>");
    P2_info_layout = new QVBoxLayout();
    P2_info_layout->addWidget(P2_DeckChoice_label);
    P2_info_layout->addWidget(P2_DeckChoice_combobox);
    P2_info_layout->addWidget(P2_Deck_list);
    fillList(P2_Deck_list, availableDecks[0]);
    P2_Deck = availableDecks[0];

    deckChoice_layout->addLayout(P1_info_layout);
    deckChoice_layout->addLayout(P2_info_layout);

    start_button = new QPushButton("Avvia partita");
    connect(start_button, SIGNAL(clicked()), this, SLOT(handleStartGameButton()));

    main_layout->addLayout(deckChoice_layout);
    main_layout->addWidget(start_button);
}

void StartPlayWidget::fillList(QListWidget *list, Deck deck) {
    list->blockSignals(true);
    list->clear();
    list->blockSignals(false);
    for (unsigned int i = 0; i < deck.getNumberOfCards(); ++i)
        list->addItem(deck.getCard(i)->getTitle().c_str());
}

void StartPlayWidget::handleChangeInFirstDeck(int i) {
    fillList(P1_Deck_list, availableDecks[static_cast<unsigned int>(i)]);
    P1_Deck = availableDecks[static_cast<unsigned int>(i)];
}

void StartPlayWidget::handleChangeInSecondDeck(int i) {
    fillList(P2_Deck_list, availableDecks[static_cast<unsigned int>(i)]);
    P2_Deck = availableDecks[static_cast<unsigned int>(i)];
}

void StartPlayWidget::handleStartGameButton() {
    Player *P1 = new Player("Giocatore 1", P1_Deck, true); //Inizia a giocare il giocatore 1
    Player *P2 = new Player("Giocatore 2", P2_Deck, false);

    GameViewWidget *gameView = new GameViewWidget(P1, P2);
    gameView->setWindowModality(Qt::WindowModal);
    gameView->show();
}
