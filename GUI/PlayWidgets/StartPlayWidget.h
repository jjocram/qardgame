//
// Created by Marco Ferrati on 2019-06-23.
//

#ifndef QARDGAME_STARTPLAYWIDGET_H
#define QARDGAME_STARTPLAYWIDGET_H

#include <QWidget>

#include <QComboBox>
#include <QListWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

#include "../../Container/Container.h"
#include "../../Model/Deck.h"

class StartPlayWidget: public QWidget {
    Q_OBJECT
private:
    Container<Deck> availableDecks;
    Deck P1_Deck;
    Deck P2_Deck;

    QVBoxLayout *main_layout;
    QHBoxLayout *deckChoice_layout;

    QComboBox *P1_DeckChoice_combobox;
    QComboBox *P2_DeckChoice_combobox;
    QListWidget *P1_Deck_list;
    QListWidget *P2_Deck_list;
    QLabel *P1_DeckChoice_label;
    QLabel *P2_DeckChoice_label;
    QVBoxLayout *P1_info_layout;
    QVBoxLayout *P2_info_layout;

    QPushButton *start_button;

    void fillList(QListWidget *list, Deck deck);
public:
    explicit StartPlayWidget(QWidget *parent = nullptr);

public slots:
    void handleChangeInFirstDeck(int i);
    void handleChangeInSecondDeck(int i);
    void handleStartGameButton();
};


#endif //QARDGAME_STARTPLAYWIDGET_H
