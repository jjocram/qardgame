//
// Created by Marco Ferrati on 2019-06-23.
//

#ifndef QARDGAME_GAMEVIEWWIDGET_H
#define QARDGAME_GAMEVIEWWIDGET_H

#include <QWidget>

#include <QListWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>

#include "CardShowerWidget.h"

#include "../../Container/Container.h"
#include "../../Model/Hierachy/Card.h"
#include "../../Model/Player.h"
#include "../../Model/Deck.h"


class GameViewWidget: public QWidget {
    Q_OBJECT
private:
    Player *P1;
    Player *P2;

    Container<CardShowerWidget *> P1_inGameCardShower;
    Container<CardShowerWidget *> P2_inGameCardShower;

    QVBoxLayout *main_layout;

    QListWidget *P1_deck_list;
    QListWidget *P2_deck_list;

    QListWidget *P1_inGameCreature_list;
    QListWidget *P2_inGameCreature_list;

    QLabel *P1_life_label;
    QLabel *P2_life_label;
    QLabel *P1_mana_label;
    QLabel *P2_mana_label;
    QPushButton *P1_endTurn_button;
    QPushButton *P2_endTurn_button;

    QHBoxLayout *P1_info_layout;
    QHBoxLayout *P2_info_layout;

    void buildDeckView(Player *player, QListWidget *listWidget);
    void paintHorizontalLine(QLayout *layout);
    void creatureIsSetted(Creature * creature, Player *player, QListWidget *listWidget);
    void connectInGameCreaturesForChoice();
    void disconnectInGameCreaturesFromChoice();
    void disableWidget(QWidget *widget);
    void enableWidget(QWidget *widget);
    void endGame();
    QListWidgetItem *newCardShowerItem();
    Player* getPlayerTurn();
    Player* getNotPlayerTurn();
    void removeCardShower(Player* p, int index);
public:
    explicit GameViewWidget(Player *P1_init, Player *P2_init, QWidget *parent = nullptr);
public slots:
    void aCardWasPlayed(DeepPtr<Card> &playedCard , Player *whoPlayedCard , int index);
    void endTurnP1();
    void endTurnP2();
    void cardClicked(DeepPtr<Card>&, Player*, int, QListWidget*);
    void creatureIsAttacking(DeepPtr<Card> &card, Player *player, int index);
};


#endif //QARDGAME_GAMEVIEWWIDGET_H
