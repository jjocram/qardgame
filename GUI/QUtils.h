//
// Created by Marco Ferrati on 2019-05-01.
//

#ifndef QARDGAME_QUTILS_H
#define QARDGAME_QUTILS_H

#include <QWidget>
#include <QString>
#include <QMessageBox>
#include <QLayout>

class QUtils {
public:
    static QMessageBox *showAlert(const QString &titleString, const QString &descriptionString = QString());

    static void clearLayout(QLayout *layout);
};


#endif //QARDGAME_QUTILS_H
