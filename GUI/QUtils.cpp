//
// Created by Marco Ferrati on 2019-05-01.
//

#include "QUtils.h"

#include <QMessageBox>
#include <QLayoutItem>

QMessageBox *QUtils::showAlert(const QString &titleString, const QString &descriptionString) {
    QMessageBox* alertBox = new QMessageBox();
    alertBox->setText(titleString);
    if (!descriptionString.isEmpty())
        alertBox->setInformativeText(descriptionString);
    alertBox->setStandardButtons(QMessageBox::Ok);
    alertBox->setDefaultButton(QMessageBox::Ok);
    return alertBox;
}

void QUtils::clearLayout(QLayout *layout) {
    if (!layout)
        return;

    //Strutture di layoyout possibili:
    //  1) layout( Widget, Widget, ... )
    //  2) layout( Layout( Widget, Widget, ... ), Layout( Widget, Widget, ... ), ... )
    QLayoutItem *item;
    while ((item = layout->takeAt(0)) != nullptr){
        QLayout *insideLayout = item->layout();
        if (insideLayout != nullptr){ //Item è un sottoLayout
            QUtils::clearLayout(insideLayout);
            layout->removeItem(item);
            delete item->layout();
        }
        else{
            layout->removeItem(item);
            delete item->widget();
        }
    }
}
