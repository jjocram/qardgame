//
// Created by Marco Ferrati on 2019-05-01.
//

#ifndef QARDGAME_MAINWINDOW_H
#define QARDGAME_MAINWINDOW_H

#include <QMainWindow>

#include <QMenuBar>
#include <QMenu>
#include <QAction>

#include "MainWidget.h"


class MainWindow: public QMainWindow {
    Q_OBJECT
private:
    MainWidget* main_widget;
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
};


#endif //QARDGAME_MAINWINDOW_H
