//
// Created by Marco Ferrati on 2019-04-28.
//

#ifndef QARDGAME_SPECIALMANAGENERATOR_H
#define QARDGAME_SPECIALMANAGENERATOR_H

#include "ManaGenerator.h"
#include "Magic.h"

class SpecialManaGenerator: public ManaGenerator, public Magic {
public:
    SpecialManaGenerator(const std::string &titleInit, const std::string &descriptionInit, unsigned int manaCostInit,
                         CardType typeInit, unsigned int incrementValue,Effect *effect);

    explicit SpecialManaGenerator(QXmlStreamReader &reader);

    Card *clone() const override;

    void setCard(Player *whoPlayed, Player *whoReceived) override;

    std::string getInfo() const override;

    void writeOnXml(QXmlStreamWriter &writer) const override;
};


#endif //QARDGAME_SPECIALMANAGENERATOR_H
