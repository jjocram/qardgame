//
// Created by Marco Ferrati on 2019-04-28.
//

#ifndef QARDGAME_MAGIC_H
#define QARDGAME_MAGIC_H


#include "Card.h"
#include <string>
#include "Effects/Effect.h"

class Effect;

class Magic: virtual public Card {
private:
    Effect* effect;
public:
    Magic(const std::string &titleInit, const std::string &descriptionInit, unsigned int manaCostInit,
          CardType typeInit, Effect *effect);
    Magic(const Magic& magic);

    explicit Magic(QXmlStreamReader &reader);

    ~Magic() override;
    Card *clone() const override;

    void setCard(Player *whoPlayed, Player *whoReceived) override;
    void playMagic(Player *whoPlayed, Player *whoReceived, int index, int playerSelected);

    Effect *getEffect() const;

    std::string getInfo() const override;

    void writeOnXml(QXmlStreamWriter &writer) const override;
};


#endif //QARDGAME_MAGIC_H
