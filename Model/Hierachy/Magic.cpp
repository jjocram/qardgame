//
// Created by Marco Ferrati on 2019-04-28.
//

#include "Magic.h"
#include "Effects/DamageEffect.h"
#include "Effects/ContinuousEffect.h"

#include <iostream>
using namespace std;

Magic::Magic(const std::string &titleInit, const std::string &descriptionInit, unsigned int manaCostInit,
             CardType typeInit, Effect *effect) : Card(titleInit, descriptionInit, manaCostInit, typeInit),
                                                  effect(effect) {}

Magic::Magic(const Magic &magic):Card(magic), effect(magic.effect->clone()) {}

Magic::~Magic() {
    if (effect) delete effect;
}

Card *Magic::clone() const {
    return new Magic(*this);
}

void Magic::setCard(Player *, Player *) {

}

void Magic::playMagic(Player *whoPlayed, Player *whoReceived, int index, int playerSelected) {
    effect->activate(whoPlayed, whoReceived, index, playerSelected);
}

Effect *Magic::getEffect() const {
    return effect;
}

std::string Magic::getInfo() const {
    return Card::getInfo().append("; Magic");
}

void Magic::writeOnXml(QXmlStreamWriter &writer) const {
    Card::writeOnXml(writer);

    writer.writeStartElement("effect");
    effect->writeOnXml(writer);
    writer.writeEndElement();
}

Magic::Magic(QXmlStreamReader &reader) : Card(reader) {
    reader.readNextStartElement();
    EffectType effectType = static_cast<EffectType >(reader.attributes().value("type").toInt());
    switch (effectType){
        case DamageType:
            effect = new DamageEffect(reader);
            break;
        case ContinuousType:
            effect = new ContinuousEffect(reader);
            break;
    }
}


