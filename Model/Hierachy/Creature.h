//
// Created by Marco Ferrati on 2019-04-26.
//

#ifndef QARDGAME_CREATURE_H
#define QARDGAME_CREATURE_H


#include "Card.h"

enum AttackSituation{victory, victoryAndDeath, parity, defeat, defeatAndDeath, directAttack, alreadyAttacked, error};

class Creature: virtual public Card {
private:
    int atk;
    int def;
    int life;
public:
    Creature(const std::string &titleInit, const std::string &descriptionInit, unsigned int manaCostInit, CardType typeInit, int atk, int def, int life);
    Creature(const Creature &creature);

    explicit Creature(QXmlStreamReader &reader);

    Creature *clone() const override;

    int getAtk() const;
    int getDef() const;
    int getLife() const;

    void modifiedAtk(int value);
    void modifiedDef(int value);

    //Metodo che viene chiamato quando la carta attacca
    virtual AttackSituation playCard(Player* whoAttacked, Player* whoReceivedTheAttack, int indexOfDefenseCard);

    //Metodo che viene chiamato quando la casrta è messa in gioco
    void setCard(Player *whoPlayed, Player *whoReceived) override;

    virtual void attack(Player* opponent);
    virtual AttackSituation attack(Creature *creature);
    virtual int takeDamage(int damage, Player* cardOwner);

    void writeOnXml(QXmlStreamWriter &writer) const override;

    std::string getInfo() const override;
};


#endif //QARDGAME_CREATURE_H
