//
// Created by Marco Ferrati on 2019-04-26.
//

#include "Card.h"
#include <QString>

#include <iostream>
using namespace std;

Card::Card(const std::string &title_init, const std::string &description_init, unsigned int manaCost_init, CardType type_init): title(title_init), description(description_init), manaCost(manaCost_init), type(type_init){}

Card::Card(const Card &card): title(card.title), description(card.description), manaCost(card.manaCost), type(card.type) {}

Card::Card(QXmlStreamReader &reader) {
    type = static_cast<CardType>(reader.attributes().value("CardType").toInt());

    reader.readNextStartElement();
    title = reader.readElementText().toStdString();

    reader.readNextStartElement();
    description = reader.readElementText().toStdString();

    reader.readNextStartElement();
    manaCost = static_cast<unsigned int>(reader.readElementText().toInt());
}

const std::string &Card::getTitle() const {
    return title;
}

const std::string &Card::getDescription() const {
    return description;
}

unsigned int Card::getManaCost() const {
    return manaCost;
}

const std::string Card::getTypeString() const {
    switch (type){
        case Creature_Type: return std::string("Creature");
        case Magic_Type: return std::string("Magic");
        case ManaGenerator_Type: return std::string("ManaGenerator");
        case SpecialCreature_Type: return std::string("SpecialCreature");
        case SpecialManaGenerator_Type: return std::string("SpecialManaGenerator");
    }
    return std::string("[ERRORE] TIPO NON RICONOSCIUTO");
}

CardType Card::getType() const {
    return type;
}

void Card::writeOnXml(QXmlStreamWriter &writer) const {
    writer.writeAttribute("CardType", std::to_string(type).c_str());

    writer.writeStartElement("title");
    writer.writeCharacters(title.c_str());
    writer.writeEndElement();

    writer.writeStartElement("description");
    writer.writeCharacters(description.c_str());
    writer.writeEndElement();

    writer.writeStartElement("manaCost");
    writer.writeCharacters(std::to_string(manaCost).c_str()); //Trick per convertire un numero in una stringa c-style convertita poi a QString
    writer.writeEndElement();
}

std::string Card::getInfo() const {
    std::string info = "Name: ";
    info.append(title);
    info.append(", ManaCost: ");
    info.append(std::to_string(manaCost));
    info.append(", Tipo: ");
    info.append(getTypeString());
    return info;
}

bool Card::operator==(const Card &cardToConfront) const {
    return cardToConfront.type == type && cardToConfront.title == title && cardToConfront.description == description && cardToConfront.manaCost == manaCost;
}


