//
// Created by Marco Ferrati on 2019-04-26.
//

#include "Creature.h"
#include "../Player.h"

#include <iostream>
using namespace std;

Creature::Creature(const std::string &titleInit, const std::string &descriptionInit, unsigned int manaCostInit, CardType typeInit, int atk, int def, int life) : Card(titleInit, descriptionInit, manaCostInit,typeInit), atk(atk), def(def), life(life) {}
Creature::Creature(const Creature &creature) : Card(creature), atk(creature.atk), def(creature.def), life(creature.life) {}

Creature::Creature(QXmlStreamReader &reader) : Card(reader) {
    reader.readNextStartElement();
    atk = reader.readElementText().toInt();

    reader.readNextStartElement();
    def = reader.readElementText().toInt();

    reader.readNextStartElement();
    life = reader.readElementText().toInt();
}

Creature *Creature::clone() const {
    return new Creature(*this);
}

int Creature::getAtk() const {
    return atk;
}

int Creature::getDef() const {
    return def;
}

int Creature::getLife() const {
    return life;
}

void Creature::modifiedAtk(int value) {
    atk += value;
    if (atk<0)
        atk = 0;
}

void Creature::modifiedDef(int value) {
    def += value;
    if (def < 0)
        def = 0;
}

AttackSituation Creature::playCard(Player *whoAttacked, Player *whoReceivedTheAttack, int indexOfDefenseCard) {
    if (!whoAttacked && !whoReceivedTheAttack) return error;

    if (indexOfDefenseCard == -1){
        //Attacco diretto
        attack(whoReceivedTheAttack);
        return directAttack;
    }
    else{
        Creature* cardChoose = whoReceivedTheAttack->getInGameCreatureAt(indexOfDefenseCard);
        switch (attack(cardChoose)){
            case victory:{
                int remainLife = cardChoose->takeDamage((atk - cardChoose->getDef()), whoReceivedTheAttack);
                if (remainLife > 0 )
                    return victory;
                else
                    return victoryAndDeath;
            }
            case parity:
                return parity;
            case defeat: {
                int remainLife = takeDamage(cardChoose->getDef() - atk, whoAttacked);
                if (remainLife > 0)
                    return defeat;
                else
                    return defeatAndDeath;
            }
            case victoryAndDeath:break;
            case defeatAndDeath:break;
            case directAttack:break;
            case alreadyAttacked:break;
            case error:break;
        }
    }
    return error;
}

void Creature::setCard(Player *whoPlayed, Player *) {
    whoPlayed->getInGameCreature().append(DeepPtr<Creature>(this->clone()));
    whoPlayed->newCreatureSetted();
}

void Creature::attack(Player *opponent) {
    opponent->getDamage();
}

AttackSituation Creature::attack(Creature *creature) {
    if (creature->def < atk)
        return victory;
    else if (creature->def == atk)
        return parity;
    else
        return defeat;
}

int Creature::takeDamage(int damage, Player *) {
    life = life - damage;
    return life;
}

void Creature::writeOnXml(QXmlStreamWriter &writer) const {
    Card::writeOnXml(writer);

    writer.writeStartElement("atk");
    writer.writeCharacters(std::to_string(atk).c_str());
    writer.writeEndElement();

    writer.writeStartElement("def");
    writer.writeCharacters(std::to_string(def).c_str());
    writer.writeEndElement();

    writer.writeStartElement("life");
    writer.writeCharacters(std::to_string(life).c_str());
    writer.writeEndElement();
}

std::string Creature::getInfo() const {
    return Card::getInfo().append(", Life: ").append(std::to_string(life)).append(", Atk: ").append(std::to_string(atk)).append(", Def: ").append(std::to_string(def));
}






