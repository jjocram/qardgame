//
// Created by Marco Ferrati on 2019-04-27.
//

#include "DamageEffect.h"
#include <iostream>
using namespace std;

DamageEffect::DamageEffect(EffectType type, int damage) : Effect(type), damage(damage) {}

DamageEffect *DamageEffect::clone() const {
    return new DamageEffect(*this);
}

int DamageEffect::getDamage() const {
    return damage;
}

void DamageEffect::makeDamage(Player *whoReceive) const {
    whoReceive->getDamage();
}

void DamageEffect::makeDamage(Creature *creature, Player *creatureOwner) const {
    creature->takeDamage(damage, creatureOwner);
}

void DamageEffect::activate(Player *, Player *whoReceived, int index, int) {
    if (index != -2){
        Creature* creature = whoReceived->getInGameCreatureAt(index);
        makeDamage(creature, whoReceived);
    }
    else{
        makeDamage(whoReceived);
    }
}

void DamageEffect::writeOnXml(QXmlStreamWriter &writer) {
    Effect::writeOnXml(writer);

    writer.writeStartElement("damage");
    writer.writeCharacters(std::to_string(damage).c_str());
    writer.writeEndElement();
}

DamageEffect::DamageEffect(QXmlStreamReader &reader) : Effect(reader) {
    reader.readNextStartElement();
    damage = reader.readElementText().toInt();
}
