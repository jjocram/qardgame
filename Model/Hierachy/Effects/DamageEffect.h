//
// Created by Marco Ferrati on 2019-04-27.
//

#ifndef QARDGAME_DAMAGEEFFECT_H
#define QARDGAME_DAMAGEEFFECT_H

#include "Effect.h"
#include "../../Player.h"


class DamageEffect: public Effect {
private:
    int damage;
public:
    DamageEffect(EffectType type, int damage);
    DamageEffect(const DamageEffect&) = default;

    explicit DamageEffect(QXmlStreamReader &reader);

    DamageEffect *clone() const override;

    int getDamage() const;

    void makeDamage(Player* whoReceive) const;
    void makeDamage(Creature* creature, Player* creatureOwner) const;

    void activate(Player *whoActivate, Player *whoReceived, int index, int player) override;

    void writeOnXml(QXmlStreamWriter &writer) override;
};


#endif //QARDGAME_DAMAGEEFFECT_H
