//
// Created by Marco Ferrati on 2019-04-27.
//

#include "ContinuousEffect.h"

#include <iostream>
using namespace std;

ContinuousEffect::ContinuousEffect(EffectType type, int value, TypeModified typeModified) : Effect(type), value(value),
                                                                                            typeModified(
                                                                                                    typeModified) {}
ContinuousEffect::ContinuousEffect(const ContinuousEffect &effect) : Effect(effect), value (effect.value), typeModified(effect.typeModified){}

Effect *ContinuousEffect::clone() const {
    return new ContinuousEffect(*this);
}

int ContinuousEffect::getValue() const {
    return value;
}

TypeModified ContinuousEffect::getTypeModified() const {
    return typeModified;
}

void ContinuousEffect::activate(Player *whoActivate, Player *whoReceived, int index, int playerSelected) {
    Player* player = playerSelected == 0 ? whoActivate : whoReceived;

    if (typeModified == atk)
        player->getInGameCreatureAt(index)->modifiedAtk(value);
    else
        player->getInGameCreatureAt(index)->modifiedDef(value);
}

void ContinuousEffect::writeOnXml(QXmlStreamWriter &writer) {
    Effect::writeOnXml(writer);

    writer.writeStartElement("value");
    writer.writeCharacters(std::to_string(value).c_str());
    writer.writeEndElement();

    writer.writeStartElement("typeModified");
    writer.writeCharacters(std::to_string(typeModified).c_str());
    writer.writeEndElement();
}

ContinuousEffect::ContinuousEffect(QXmlStreamReader &reader) : Effect(reader) {
    reader.readNextStartElement();
    value = reader.readElementText().toInt();
    reader.readNextStartElement();
    typeModified = static_cast<TypeModified>(reader.readElementText().toInt());
}
