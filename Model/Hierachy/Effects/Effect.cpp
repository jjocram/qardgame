//
// Created by Marco Ferrati on 2019-04-27.
//

#include "Effect.h"

Effect::Effect(EffectType type) : type(type) {}

EffectType Effect::getType() const {
    return type;
}

void Effect::writeOnXml(QXmlStreamWriter &writer) {
    writer.writeAttribute("type", std::to_string(type).c_str());
}

Effect::Effect(QXmlStreamReader &reader) {
    //reader.readNextStartElement();
    type = static_cast<EffectType>(reader.attributes().value("type").toInt());
}

