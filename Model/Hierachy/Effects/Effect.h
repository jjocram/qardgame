//
// Created by Marco Ferrati on 2019-04-27.
//

#ifndef QARDGAME_EFFECT_H
#define QARDGAME_EFFECT_H

#include <QXmlStreamWriter>
#include <QXmlStreamReader>

#include "../../Player.h"
#include "../Creature.h"

enum EffectType{DamageType, ContinuousType};

class Effect {
private:
    EffectType type;
public:
    explicit Effect(EffectType type);
    explicit Effect(QXmlStreamReader& reader);
    Effect(const Effect& effect) = default;
    virtual ~Effect() = default;
    virtual Effect* clone() const = 0;

    EffectType getType() const;
    virtual void activate(Player* whoActivate, Player* whoReceived, int index, int player) = 0;
    virtual void writeOnXml(QXmlStreamWriter& writer);
};


#endif //QARDGAME_EFFECT_H
