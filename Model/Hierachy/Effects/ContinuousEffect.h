//
// Created by Marco Ferrati on 2019-04-27.
//

#ifndef QARDGAME_CONTINUOUSEFFECT_H
#define QARDGAME_CONTINUOUSEFFECT_H

#include "Effect.h"

enum TypeModified{atk, def};

class ContinuousEffect: public Effect {
private:
    int value;
    TypeModified typeModified;
public:
    ContinuousEffect(EffectType type, int value, TypeModified typeModified);
    ContinuousEffect(const ContinuousEffect &effect);

    explicit ContinuousEffect(QXmlStreamReader &reader);

    Effect *clone() const override;

    int getValue() const;
    TypeModified getTypeModified() const;

    void activate(Player *whoActivate, Player *whoReceived, int index, int player) override;

    void writeOnXml(QXmlStreamWriter &writer) override;
};


#endif //QARDGAME_CONTINUOUSEFFECT_H
