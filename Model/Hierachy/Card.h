//
// Created by Marco Ferrati on 2019-04-26.
//

#ifndef QARDGAME_CARD_H
#define QARDGAME_CARD_H

#include <string>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

class Player; //Dichiarazione incompleta per usare il puntatore
enum CardType{Creature_Type, Magic_Type, ManaGenerator_Type, SpecialCreature_Type, SpecialManaGenerator_Type};

class Card {
private:
    std::string title;
    std::string description;
    unsigned int manaCost;

    CardType type;
public:
    Card(const std::string& title_init, const std::string& description_init, unsigned int manaCost_init, CardType type_init);
    Card(const Card& card);
    explicit Card(QXmlStreamReader& reader);
    virtual ~Card() = default;

    virtual Card* clone() const = 0;

    virtual void setCard(Player* whoPlayed, Player* whoReceived) = 0;

    const std::string &getTitle() const;
    const std::string &getDescription() const;
    unsigned int getManaCost() const;
    const std::string getTypeString() const;
    CardType getType() const;

    virtual void writeOnXml(QXmlStreamWriter& writer) const;

    virtual std::string getInfo() const;
    bool operator==(const Card& cardToConfront) const;
};


#endif //QARDGAME_CARD_H
