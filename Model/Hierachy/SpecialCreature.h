//
// Created by Marco Ferrati on 2019-04-28.
//

#ifndef QARDGAME_SPECIALCREATURE_H
#define QARDGAME_SPECIALCREATURE_H


#include "Creature.h"
#include "Magic.h"

class SpecialCreature: public Creature, public Magic {
public:
    SpecialCreature(const std::string &titleInit, const std::string &descriptionInit, unsigned int manaCostInit,
                    CardType typeInit, int atk, int def, int life,
                    Effect *effect);
    //SpecialCreature(const SpecialCreature&);
    explicit SpecialCreature(QXmlStreamReader &reader);
    //~SpecialCreature() override; //TDOD: remove

    Creature *clone() const override;

    AttackSituation
    playCard(Player *whoAttacked, Player *whoReceivedTheAttack, int indexOfDefenseCard) override;

    void setCard(Player *whoPlayed, Player *whoReceived) override;

    void writeOnXml(QXmlStreamWriter &writer) const override;

    std::string getInfo() const override;
};


#endif //QARDGAME_SPECIALCREATURE_H
