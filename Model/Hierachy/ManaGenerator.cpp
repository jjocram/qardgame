//
// Created by Marco Ferrati on 2019-04-26.
//

#include "ManaGenerator.h"
#include "../Player.h"
#include <iostream>
using namespace std;

ManaGenerator::ManaGenerator(const std::string &titleInit, const std::string &descriptionInit,
                             unsigned int manaCostInit, CardType typeInit, unsigned int incrementValue) : Card(
        titleInit, descriptionInit, manaCostInit, typeInit), incrementValue(incrementValue) {}

ManaGenerator::ManaGenerator(const ManaGenerator & manaGenerator): Card(manaGenerator), incrementValue(manaGenerator.incrementValue) {}

ManaGenerator::ManaGenerator(QXmlStreamReader &reader) : Card(reader) {
    reader.readNextStartElement();
    incrementValue = static_cast<unsigned int>(reader.readElementText().toInt());
}

Card *ManaGenerator::clone() const {
    return new ManaGenerator(*this);
}

void ManaGenerator::setCard(Player *whoPlayed, Player *) {
    whoPlayed->addMana(incrementValue);
}

unsigned int ManaGenerator::getIncrementValue() const {
    return incrementValue;
}

void ManaGenerator::writeOnXml(QXmlStreamWriter &writer) const {
    Card::writeOnXml(writer);

    writer.writeStartElement("incrementValue");
    writer.writeCharacters(std::to_string(incrementValue).c_str());
    writer.writeEndElement();
}

std::string ManaGenerator::getInfo() const {
    return Card::getInfo().append(" is a ManaGenerator");
}


