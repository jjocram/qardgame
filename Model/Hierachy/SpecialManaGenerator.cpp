//
// Created by Marco Ferrati on 2019-04-28.
//

#include "SpecialManaGenerator.h"

SpecialManaGenerator::SpecialManaGenerator(const std::string &titleInit, const std::string &descriptionInit,
                                           unsigned int manaCostInit, CardType typeInit, unsigned int incrementValue,
                                            Effect *effect)
        : Card(titleInit, descriptionInit, manaCostInit, typeInit), ManaGenerator(titleInit, descriptionInit, manaCostInit, typeInit, incrementValue),
          Magic(titleInit, descriptionInit, manaCostInit, typeInit, effect) {}

SpecialManaGenerator::SpecialManaGenerator(QXmlStreamReader &reader) : Card(reader), ManaGenerator(reader), Magic(reader) {}

Card *SpecialManaGenerator::clone() const {
    return new SpecialManaGenerator(*this);
}

void SpecialManaGenerator::setCard(Player *whoPlayed, Player *whoReceived) {
    ManaGenerator::setCard(whoPlayed, whoReceived);
}

std::string SpecialManaGenerator::getInfo() const {
    return ManaGenerator::getInfo().append("; it's a magic mana generator");
}

void SpecialManaGenerator::writeOnXml(QXmlStreamWriter &writer) const {
    ManaGenerator::writeOnXml(writer);

    writer.writeStartElement("effect");
    getEffect()->writeOnXml(writer);
    writer.writeEndElement();
}
