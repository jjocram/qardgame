//
// Created by Marco Ferrati on 2019-04-26.
//

#ifndef QARDGAME_MANAGENERATOR_H
#define QARDGAME_MANAGENERATOR_H


#include "Card.h"

class ManaGenerator: virtual public Card {
private:
    unsigned int incrementValue;
public:
    ManaGenerator(const std::string &titleInit, const std::string &descriptionInit, unsigned int manaCostInit,
                  CardType typeInit, unsigned int incrementValue);
    ManaGenerator(const ManaGenerator&);

    explicit ManaGenerator(QXmlStreamReader &reader);

    Card *clone() const override;
    void setCard(Player *whoPlayed, Player *whoReceived) override;

    unsigned int getIncrementValue() const;

    void writeOnXml(QXmlStreamWriter &writer) const override;

    std::string getInfo() const override;
};


#endif //QARDGAME_MANAGENERATOR_H
