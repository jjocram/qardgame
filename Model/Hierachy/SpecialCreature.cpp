//
// Created by Marco Ferrati on 2019-04-28.
//

#include "SpecialCreature.h"

#include <iostream>
using namespace std;

SpecialCreature::SpecialCreature(const std::string &titleInit, const std::string &descriptionInit,
                                 unsigned int manaCostInit, CardType typeInit, int atk, int def, int life,
                                  Effect *effect) : Card(titleInit, descriptionInit, manaCostInit, typeInit),
                                                                                                   Creature(titleInit,
                                                                                                            descriptionInit,
                                                                                                            manaCostInit,
                                                                                                            typeInit,
                                                                                                            atk, def,
                                                                                                            life),
                                                                                                   Magic(titleInit,
                                                                                                         descriptionInit,
                                                                                                         manaCostInit,
                                                                                                         typeInit,
                                                                                                         effect) {}

//SpecialCreature::SpecialCreature(const SpecialCreature &specialCreature): Card (specialCreature), Creature (specialCreature), Magic (specialCreature){}

SpecialCreature::SpecialCreature(QXmlStreamReader &reader) : Card(reader), Creature(reader), Magic(reader) {}

Creature *SpecialCreature::clone() const {
    return new SpecialCreature(*this);
}

AttackSituation
SpecialCreature::playCard(Player *whoAttacked, Player *whoReceivedTheAttack, int indexOfDefenseCard) {
    return Creature::playCard(whoAttacked, whoReceivedTheAttack, indexOfDefenseCard);
}

void SpecialCreature::setCard(Player *whoPlayed, Player *whoReceived) {
    Creature::setCard(whoPlayed, whoReceived);
}

void SpecialCreature::writeOnXml(QXmlStreamWriter &writer) const {
    Creature::writeOnXml(writer);

    writer.writeStartElement("effect");
    getEffect()->writeOnXml(writer);
    writer.writeEndElement();
}

std::string SpecialCreature::getInfo() const {
    return Creature::getInfo().append("; it's a magic creature");
}




