//
// Created by Marco Ferrati on 2019-04-26.
//

#include "ProjectException/ProjException.h"

#include "Deck.h"

#include <QSaveFile>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QString>
#include <QDir>

#include "Hierachy/Creature.h"
#include "Hierachy/Magic.h"
#include "Hierachy/ManaGenerator.h"
#include "Hierachy/SpecialCreature.h"
#include "Hierachy/SpecialManaGenerator.h"
#include "Hierachy/Effects/DamageEffect.h"
#include "Hierachy/Effects/ContinuousEffect.h"

Deck::Deck(std::string name, const Container<DeepPtr<Card>> &cards) : name(std::move(name)), cards(cards) {}

Deck::Deck(std::string name) : name(std::move(name)) {}

//Deck::Deck(const Deck &deck): name(deck.name), cards(deck.cards) {}

Deck::Deck(const Container<DeepPtr<Card>>& cards_init): name(""), cards(cards_init) {}

const std::string &Deck::getName() const {
    return name;
}

void Deck::setName(const std::string &name) {
    Deck::name = name;
}

void Deck::addCard(Card *card) {
    cards.append(DeepPtr<Card>(card->clone()));
}

void Deck::removeCard(unsigned int index) {
    cards.removeAt(index);
}

void Deck::removeCard(Card *cardToRemove) {
    try {
        removeCard(cards.indexOf(DeepPtr<Card>(cardToRemove)));
    } catch (ProjException e){
        std::cerr << e.what() << std::endl;
    }
}

Card* Deck::getCard(unsigned int index) {
    return (index < cards.count()) ? cards[index].operator->() : nullptr;
}

unsigned int Deck::getNumberOfCards() const {
    return cards.count();
}

bool Deck::isEmpty() const {
    return cards.isEmpty();
}

void Deck::replace(unsigned int index, const DeepPtr<Card> &replaceEl) {
    try {
        cards.replaceAt(index, replaceEl);
    } catch (ProjException e){
        std::cerr << e.what() << std::endl;
    }
}

void Deck::replace(const DeepPtr<Card> &elToReplace, const DeepPtr<Card> &replaceEl) {
    try {
        cards.replaceAt(cards.indexOf(DeepPtr<Card>(elToReplace)), replaceEl);
    } catch (ProjException e){
        std::cerr << e.what() << std::endl;
    }
}

bool Deck::operator==(const Deck &deck) {
    return name == deck.name  && cards == deck.cards;
}

bool Deck::operator!=(const Deck & deck) {
    return !(*this == deck);
}

void Deck::save(const std::string &path) {
    QDir::current().mkdir("decks"); //Crea la cartella decks in caso sia assente

    QSaveFile file(QString::fromStdString(path).replace(" ", ""));

    if (!file.open(QIODevice::WriteOnly))
        throw ProjException("Impossibile aprire il file per il salvataggio del Deck");

    QXmlStreamWriter writer(&file);

    writer.setAutoFormatting(true);
    writer.writeStartDocument();
    writer.writeComment("File di salvataggio di un deck");

    writer.writeStartElement("root");

    writer.writeStartElement("deck");

    writer.writeAttribute("name", name.c_str());

    for (unsigned int i = 0; i < cards.count(); ++i){
        writer.writeStartElement("card");
        if ((cards[i]).operator->() == nullptr ){
            std::cerr << "Errore durante la dereferenziazione di DeepPtr<Card>" << std::endl;
            std::cerr << "Carta skippata" << std::endl;
            continue;
        }
        cards[i]->writeOnXml(writer);
        writer.writeEndElement();
        if (writer.hasError())
            throw ProjException("Errore durante il salvataggio di una carta; impossibile continurare");
    }

    writer.writeEndElement(); //end of deck
    writer.writeEndElement(); //end of root
    writer.writeEndDocument();

    file.commit();
}

Deck Deck::load(const std::string &path) {
    Container<DeepPtr<Card>> redCards;
    std::string deckName;

    QFile file(path.c_str());

    if (!file.open(QIODevice::ReadOnly))
        throw ProjException("Impossibile aprire il file per il caricamento del Deck");

    QXmlStreamReader reader(&file);

    if (reader.readNextStartElement() && reader.name() == "root"){
        if (reader.readNextStartElement() && reader.name() == "deck"){
            const QXmlStreamAttributes deckAttributes = reader.attributes();

            deckName = deckAttributes.hasAttribute("name") ? deckAttributes.value("name").toString().toStdString() : "";

            while (reader.readNextStartElement()){
                const QXmlStreamAttributes cardAttributes = reader.attributes();

                CardType cardType = static_cast<CardType>(cardAttributes.value("CardType").toInt());

                switch (cardType){
                    case Creature_Type:
                        redCards.append(DeepPtr<Card>(new Creature(reader)));
                        //std::cout << "Carta creatura creata" << std::endl;
                        break;
                    case ManaGenerator_Type:
                        redCards.append(DeepPtr<Card>(new ManaGenerator(reader)));
                        //std::cout << "Carta ManaGenerator creata" << std::endl;
                        break;
                    case Magic_Type:
                        redCards.append(DeepPtr<Card>(new Magic(reader)));
                        //std::cout << "Carta magia creata" << std::endl;
                        reader.readNextStartElement();
                        break;
                    case SpecialCreature_Type:
                        redCards.append(DeepPtr<Card>(new SpecialCreature(reader)));
                        //std::cout << "Carta SpecialCreature creata" << std::endl;
                        reader.readNextStartElement();
                        break;
                    case SpecialManaGenerator_Type:
                        redCards.append(DeepPtr<Card>(new SpecialManaGenerator(reader)));
                        //std::cout << "Carta SpecialManaGenerator creata" << std::endl;
                        reader.readNextStartElement();
                        break;
                }
                reader.readNextStartElement();
            }
        }
        else
            reader.skipCurrentElement();
    }
    else
        reader.skipCurrentElement();

    file.close();
    return Deck(deckName, redCards);
}

std::ostream &operator<<(std::ostream& os, const Deck& deck){
    os << "Deck: " << std::endl;
    for (auto cit = deck.cards.const_begin(); cit != deck.cards.const_end(); ++cit)
        os << "\t [" << cit.getIndex() << "]" << (*cit)->getInfo() << std::endl;
    return os;
}
