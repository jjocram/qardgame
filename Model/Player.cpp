//
// Created by Marco Ferrati on 2019-04-26.
//

#include "Player.h"

#include "Hierachy/ManaGenerator.h"
#include "Hierachy/Creature.h"

Player::Player(const std::string &name_init, const Deck &deck_init, bool myT): name(name_init), life(10), mana(0), totalMana(0), manaGeneratorSetted(false), deck(deck_init), MyTurn(myT){}

/*Player::Player(const Player &p) : name(p.name), life(p.life), mana(p.mana), totalMana(p.totalMana),
                                  manaGeneratorSetted(p.manaGeneratorSetted), deck(p.deck),
                                  inGameCreature(p.inGameCreature), inGameCreature_attacked(p.inGameCreature_attacked),
                                  cardPlayed(p.cardPlayed), attackingCreature(p.attackingCreature),
                                  inGameMagic(p.inGameMagic), MyTurn(p.MyTurn) {}
*/
const std::string &Player::getName() const {
    return name;
}

int Player::getLife() const {
    return life;
}

unsigned int Player::getMana() const {
    return mana;
}

unsigned int Player::getTotalMana() const {
    return totalMana;
}

const Deck &Player::getDeck() const {
    return deck;
}

Container<DeepPtr<Creature>> &Player::getInGameCreature(){
    return inGameCreature;
}

Creature *Player::getInGameCreatureAt(int index) {
    if (index < 0 || index > static_cast<int>(inGameCreature.count() - 1))
        throw ProjException("Index out of range");
    return inGameCreature[static_cast<unsigned int>(index)].operator->();
}

void Player::newCreatureSetted() {
    inGameCreature_attacked.append(false);
}

void Player::creaturePerformedAnAttack(int index) {
    if (index < 0 || index > static_cast<int>(inGameCreature_attacked.count() - 1))
        throw ProjException("Index out of range");
    inGameCreature_attacked[static_cast<unsigned int>(index)] = true;
}

bool Player::hasCreatureAttacked(int index) const{
    if (index < 0 || index > static_cast<int>(inGameCreature_attacked.count() - 1))
        throw ProjException("Index out of range");
    return inGameCreature_attacked[static_cast<unsigned int>(index)];
}

CardStatus Player::setCard(unsigned int index, Player *opponent) {
    Card* choosenCard = deck.getCard(index);
    unsigned int manaCost = choosenCard->getManaCost();

    if(removeMana(manaCost)){
        choosenCard->setCard(this, opponent);
        return Playable;
    }
    else
        return NotEnoughMana;
}

AttackSituation Player::playCreature(unsigned int index, Player *opponent, unsigned int indexOfDefenseCard) {
    if (inGameCreature_attacked[index])
        return alreadyAttacked;
    else {
        inGameCreature_attacked[index] = true;
        AttackSituation attackResult = inGameCreature[index]->playCard(this, opponent, indexOfDefenseCard);
        if (attackResult == victoryAndDeath){
            //Bisogna eliminare la creatura dall'inGameCreature di Opponent
            opponent->getInGameCreature().removeAt(indexOfDefenseCard);
        }
        else if (attackResult == defeatAndDeath){
            //Bisogna eliminare la creature dall'inGameCreature di questo (*this) Player
            inGameCreature.removeAt(index);
        }
        return attackResult;
    }
}

bool Player::getDamage() {
    life = life - 1;
    return life <= 0;
}

unsigned int Player::addMana(unsigned int value) {
    mana = mana + value;
    totalMana = totalMana + value;
    manaGeneratorSetted = true;

    return mana;
}

bool Player::removeMana(unsigned int manaToRemove) {
    if (mana < manaToRemove)
        return false;
    else{
        mana = mana - manaToRemove;
        return true;
    }
}

void Player::endTurn() {
    MyTurn = false;
    mana = totalMana;
    manaGeneratorSetted = false;
    for (unsigned int i = 0; i < inGameCreature_attacked.count(); ++i)
        inGameCreature_attacked[i] = false;
}

CardStatus Player::canPlay(Card *card) const {
    if (card->getManaCost() > mana)
        return NotEnoughMana;
    if (manaGeneratorSetted && dynamic_cast<ManaGenerator*>(card))
        return YouCanPlayOncePerTurn;
    return Playable;
}

void Player::setIsMyTurn(bool isMyTurn) {
    Player::MyTurn = isMyTurn;
}

void Player::setCardPlayed(PlayedCard cardPlayed) {
    Player::cardPlayed = cardPlayed;
}

void Player::setAttackingCreature(Creature *attackingCreature) {
    Player::attackingCreature = attackingCreature;
}

void Player::setInGameMagic(Magic *inGameMagic) {
    Player::inGameMagic = inGameMagic;
}

bool Player::isMyTurn() const {
    return MyTurn;
}

PlayedCard Player::getCardPlayed() const {
    return cardPlayed;
}

Creature *Player::getAttackingCreature() const {
    return attackingCreature;
}

Magic *Player::getInGameMagic() const {
    return inGameMagic;
}

unsigned int Player::indexOfInGameCreature(Creature *creature) {
    for (unsigned int i = 0; i<inGameCreature.count(); ++i){
        if (*creature == *inGameCreature[i])
            return i;
    }
    throw ProjException("Creature not found");
}
