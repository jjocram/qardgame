//
// Created by Marco Ferrati on 2019-04-26.
//

#ifndef QARDGAME_DECK_H
#define QARDGAME_DECK_H

#include "Hierachy/Card.h"
#include "../Container/Container.h"
#include "../Container/DeepPtr.h"

#include <string>

#include <iostream>
#include <chrono>
#include <ctime>

class Deck {
    friend std::ostream& operator<<(std::ostream& os, const Deck&);

private:
    std::string name;
    Container<DeepPtr<Card>> cards;
public:
    Deck() = default;
    Deck(std::string name, const Container<DeepPtr<Card>> &cards);
    explicit Deck(std::string name);

    Deck(const Deck &deck) = default;
    explicit Deck(const Container<DeepPtr<Card>>& cards_init);

    const std::string &getName() const;
    void setName(const std::string &name);

    void addCard(Card* card);
    void removeCard(unsigned int index);
    void removeCard(Card* cardToRemove);
    Card * getCard(unsigned int index);
    unsigned int getNumberOfCards() const;
    bool isEmpty() const;
    void replace(unsigned int index, const DeepPtr<Card> &replaceEl);
    void replace(const DeepPtr<Card> &elToReplace, const DeepPtr<Card> &replaceEl);

    bool operator==(const Deck&);
    bool operator!=(const Deck&);

    template <typename Functor>
    Deck find(Functor test);

    void save(const std::string& path);
    static Deck load(const std::string& path);
};

template <typename Functor>
Deck Deck::find(Functor test) {
    return Deck(cards.filter(test));
}

#endif //QARDGAME_DECK_H
