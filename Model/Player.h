//
// Created by Marco Ferrati on 2019-04-26.
//

#ifndef QARDGAME_PLAYER_H
#define QARDGAME_PLAYER_H

#include <string>
#include "Deck.h"
#include "Hierachy/Creature.h"
#include "Hierachy/Magic.h"
#include "../Container/Container.h"
class Magic;
class Effect;
enum CardStatus{Playable, NotEnoughMana, YouCanPlayOncePerTurn};
enum PlayedCard{Creature_isPlayed, Magic_isPlayed};

class Player {
private:
    std::string name;
    int life;
    unsigned int mana;
    unsigned int totalMana;
    bool manaGeneratorSetted;
    Deck deck;
    Container<DeepPtr<Creature>> inGameCreature;
    Container<bool> inGameCreature_attacked;

    PlayedCard cardPlayed;
    Creature *attackingCreature;
    Magic *inGameMagic;
    bool MyTurn;

public:
    Player(const std::string& name_init, const Deck &deck_init, bool myT);
    Player(const Player&) = default;
    ~Player() = default;

    const std::string &getName() const;

    int getLife() const;
    unsigned int getMana() const;

    Creature *getAttackingCreature() const;

    Magic *getInGameMagic() const;

    PlayedCard getCardPlayed() const;

    unsigned int getTotalMana() const;
    const Deck &getDeck() const;
    Container<DeepPtr<Creature>> &getInGameCreature();
    Creature *getInGameCreatureAt(int index);

    void newCreatureSetted();
    void creaturePerformedAnAttack(int index);
    bool hasCreatureAttacked(int index) const;

    CardStatus setCard(unsigned int index, Player* opponent);
    AttackSituation playCreature(unsigned int index, Player* opponent, unsigned int indexOfDefenseCard);
    bool getDamage();
    unsigned int addMana(unsigned int value);
    bool removeMana(unsigned int manaToRemove);
    void endTurn();
    CardStatus canPlay(Card* card) const;
    void setIsMyTurn(bool isMyTurn);
    void setCardPlayed(PlayedCard cardPlayed);
    bool isMyTurn() const;
    void setAttackingCreature(Creature *attackingCreature);
    void setInGameMagic(Magic *inGameMagic);
    unsigned int indexOfInGameCreature(Creature *creature);
};


#endif //QARDGAME_PLAYER_H
