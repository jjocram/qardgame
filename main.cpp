//
// Created by Marco Ferrati on 2019-04-26.
//

#include <QApplication>
#include "GUI/MainWindow.h"

#include <iostream>
#include "Container/Container.h"
#include "Container/DeepPtr.h"
#include "Model/Hierachy/Card.h"
#include "Model/Deck.h"

using namespace std;

int main(int argc, char* argv[]){

    QApplication app(argc, argv);
    MainWindow mainWindow;
    mainWindow.show();

    return app.exec();
/*
    Deck d = Deck::load("decks/maindeck.xml");

    d.removeCard(d.getNumberOfCards()-1);
    d.save("decks/maindeck.xml");
    */
}
