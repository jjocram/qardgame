# QardGame
Progetto realizzato per il corso di Programmazione ad Oggetti (PAO)
Laurea triennale in Informatico A.A 2018/19

# Directory e File
* Container/ -> contiene i file relativi al container usato nel progetto
* Model/ -> contiene la gerarchia e i file che si interfacciano con la GUI
    * Hierachy/ -> gerarchia (vedi relazione.pdf per maggiori dettagli)
    * Player -> identifica un giocato
    * Deck -> indentifica un mazzo
* GUI/ -> contiene i file relativi all'interfaccia grafica suddivisi in sottocartelle
    * CardsWidget/ -> widgets relativi alle singole carte
    * DecksWidget/ -> widgets relaitivi ai deck
    * PlayWidget/ -> widgets usati per le partite
    * QUtils -> Classe con delle utility usate all'interno di alcuni widget
* ProjectException/
    * ProjException -> classe usata per sollevare le eccezioni all'interno del progetto

# Compilazione e avvio
É necessario avere il file QardGame.pro per una corretta compilazione

`qmake`

`make`

`./QardGame`

# Valutazione
Di seguito è riportata la valutazione del progetto rilasciata dal docente.

Vincoli obbligatori: soddisfatti

Orientamento ad oggetti
+ Principio di incapsulamento correttamente applicato
+ Codice del modello correttamente separato da quello della vista
+ Polimorfismo applicato in maniera ragionevole

Funzionalità
+ Molte funzionalità, anche articolate, legate alle meccaniche di gioco

GUI
+ Molto articolata, ma ben curata
+ Consente ridimensionamento
+ Usa colori e icone
+ I diversi tipi di dato sono mostrati in maniera opportuna

Relazione: adeguata, sebbene succinta

Suggerimenti non collegati alla valutazione: nessuno

# Note
Se volete prendere spunto da questo progetto fatelo consapevolmente; ho cercato di sistemarlo il più possibile ma non è privo di errori o incorrettezze