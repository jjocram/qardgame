QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QardGame
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11
CONFIG += debug
QMAKE_CXXFLAGS += -std=c++11

DESTDIR=. #Target file directory
OBJECTS_DIR=generated_files #Intermediate object files directory
MOC_DIR=generated_files #Intermediate moc files director

SOURCES +=  main.cpp \
            ProjectException/ProjException.cpp \
            Model/Deck.cpp \
            Model/Player.cpp \
            Model/Hierachy/Card.cpp \
            Model/Hierachy/Creature.cpp \
            Model/Hierachy/Magic.cpp \
            Model/Hierachy/ManaGenerator.cpp \
            Model/Hierachy/SpecialCreature.cpp \
            Model/Hierachy/SpecialManaGenerator.cpp \
            Model/Hierachy/Effects/Effect.cpp \
            Model/Hierachy/Effects/DamageEffect.cpp \
            Model/Hierachy/Effects/ContinuousEffect.cpp \
            GUI/MainWindow.cpp \
            GUI/MainWidget.cpp \
            GUI/QUtils.cpp \
            GUI/CardsWidgets/CreateNewCardWidget.cpp \
            GUI/CardsWidgets/EditCardsWidget.cpp \
            GUI/DecksWidgets/MainDeckListWidget.cpp \
            GUI/DecksWidgets/SearchFilterWidget.cpp \
            GUI/CardsWidgets/CardEditorWidget.cpp \
            GUI/DecksWidgets/DeckCreatorWidget.cpp \
            GUI/DecksWidgets/ModifyDecksWidget.cpp \
            GUI/PlayWidgets/StartPlayWidget.cpp \
            GUI/PlayWidgets/CardShowerWidget.cpp \
            GUI/PlayWidgets/GameViewWidget.cpp

HEADERS +=  Container/Container.h \
            Container/DeepPtr.h \
            ProjectException/ProjException.h \
            Model/Deck.h \
            Model/Player.h \
            Model/Hierachy/Card.h \
            Model/Hierachy/Creature.h \
            Model/Hierachy/Magic.h \
            Model/Hierachy/ManaGenerator.h \
            Model/Hierachy/SpecialCreature.h \
            Model/Hierachy/SpecialManaGenerator.h \
            Model/Hierachy/Effects/Effect.h \
            Model/Hierachy/Effects/DamageEffect.h \
            Model/Hierachy/Effects/ContinuousEffect.h \
            GUI/MainWindow.h \
            GUI/MainWidget.h \
            GUI/QUtils.h \
            GUI/CardsWidgets/CreateNewCardWidget.h \
            GUI/CardsWidgets/EditCardsWidget.h \
            GUI/DecksWidgets/MainDeckListWidget.h \
            GUI/DecksWidgets/SearchFilterWidget.h \
            GUI/CardsWidgets/CardEditorWidget.h \
            GUI/DecksWidgets/DeckCreatorWidget.h \
            GUI/DecksWidgets/ModifyDecksWidget.h \
            GUI/PlayWidgets/StartPlayWidget.h \
            GUI/PlayWidgets/CardShowerWidget.h \
            GUI/PlayWidgets/GameViewWidget.h

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += resources.qrc
